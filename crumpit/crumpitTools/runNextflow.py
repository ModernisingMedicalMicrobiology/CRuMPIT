import sys
from subprocess import Popen,PIPE
from threading import Thread

class nfWorkflow:
	__allowed = ("wf","img","scratch","barcode","base","cdb",
				"sample_name","ip","refbase","home","watch","flow","seq_kit","bar_kit")
	def __init__(self,**kwargs):
		for k, v in kwargs.items():
			#assert( k in self.__class__.__allowed )
			setattr(self, k, v)

		if self.barcode == True:
			self.bararg='barcode'
		else: self.bararg='nobarcode'
		if self.watch==True:
			self.nfwatch='true'
		else: self.nfwatch='false'
		if self.basecalling==True:
			self.nfbasecall='true'
		else: self.nfbasecall='false'
		if self.grid==True:
			self.nfgrid='true'
		else: self.nfgrid='false'
		self.runWorkflow()

	def runWorkflow(self):
		if type(self.map)==list:
			self.map=' '.join(self.map)
		if type(self.custom_refs)==list:
			self.custom_refs=' '.join(self.custom_refs)
		
		flowcell = self.flow
		if len(flowcell) > 10:
			flowcell = self.flow[:10]

		l=['nextflow',
			'run',
			"{0}".format(self.wf),
			'-profile',
			"{0}".format(self.profile),
			'-w',self.scratch,
			'-with-trace',
			'--barcode={0}'.format(self.bararg),
			'--bdir={0}'.format(self.bdir),
			'--base={0}'.format(self.base),
			'--seqfol={0}'.format(self.fast5s),
			'--cdb={0}'.format(self.cdb),
			'--sample={0}'.format(self.sample_name),
			'--ip={0}'.format(self.ip),
			'--sqlip={0}'.format(self.sqlip),
			'--refbase={0}'.format(self.refbase),
			'--custom_refs={0}'.format(self.custom_refs),
			'--flowcell={0}'.format(flowcell),
			'--seq_kit={0}'.format(self.seq_kit),
			'--bar_kit={0}'.format(self.bar_kit),
			'--watch={0}'.format(self.nfwatch),
			'--porechop={0}'.format(self.porechop),
			'--basecalling={0}'.format(self.basecalling),
			'--grid={0}'.format(self.nfgrid),
			'--insfx={0}'.format(self.insfx),
            '--map={0}'.format(self.map)
			]
		if self.resume == True: l.append('-resume')
		self.nfpipe=Popen(l,stdout=PIPE,stderr=None,universal_newlines=True)

	def _recvr(self):
		n,y,ny=1,29,1
		lines=[]
		for line in self.nfpipe.stdout:
			if self.win is not None:
				lines.append(line)
				if len(lines) < y:
					self.win.addstr(n+1, 1, line.replace('\n',''))
			#self.win.clrtoeol()
				else:
					del(lines[0])
					ny=1
					for l in lines[:-1]:
						self.win.addstr(ny+1, 1, l.replace('\n',''))
						ny+=1
						self.win.clrtoeol()
				n+=1
				self.win.refresh()
				if n==y: n=1
			else: print(line.replace('\n',''),file=sys.stderr)


	def run(self,crswin=None):
		self.win=crswin
		rcv=Thread(target=self._recvr)
		rcv.start()
		if self.watch!=True: rcv.join()
