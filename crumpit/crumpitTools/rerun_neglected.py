#!/usr/bin/env python3
import sys
import os
from argparse import ArgumentParser, SUPPRESS
import subprocess
import shutil

class rerun:
    def __init__(self,**kwargs):
        self.t='{0}/trace.txt'.format(kwargs['dirs'])
        self.a='{0}/all_batches'.format(kwargs['dirs'])
        self.tmp='{0}/tmp'.format(kwargs['dirs'])

    def getTrace(self,trace):
        batches=[]
        with open(trace,'rt') as tf:
            for line in tf:
                l=line.split('\t')
                if l[0]=='task_id': headers=l
                else:
                    name=l[3]
                    batch=name.split('_')[-1][:-1]
                    status=l[4]
                    if status == 'COMPLETED':
                        batches.append(batch)
        return batches

    def getAllbatches(self,fol):
        batches={}
        out = subprocess.check_output(["find", "{0}".format(fol),"-name","*.batch*"],universal_newlines=True)
        for bf in out.splitlines():
            b=bf.split('/')[-1].split('.')[0]
            with open(bf,'rt') as f:
                batch=str(f.readlines()[0].split(',')[1])
            batches[batch]=bf
        return batches

    def makeFol(self,fol):
        if not os.path.exists(fol):
            os.makedirs(fol)

    def rerun(self,files,tmp,fol):
        dsts=[]
        for b in files:
            fn=files[b].split('/')[-1]
            dst='{0}/{1}'.format(tmp,fn)
            dsts.append(dst)
            shutil.move(files[b], dst)
        for dst in dsts:
            shutil.copy(dst, fol)


    def run(self):
        self.makeFol(self.tmp)
        runBatches=set(self.getTrace(self.t))
        allBatches=self.getAllbatches(self.a)
        failedBatches={k:v for k,v in allBatches.items() if k not in runBatches}
        self.failedN=len(list(failedBatches.keys()))
        self.rerun(failedBatches,self.tmp,self.a)


if __name__ == "__main__":
    parser = ArgumentParser(description='rerun negelected CRuMPIT batches')
    parser.add_argument('-d', '--dirs', required=True,
                             help='Folder containing the run')
    opts, unknown_args = parser.parse_known_args()
    wfconfig=vars(opts)
    # run
    rr=rerun(**wfconfig)
    rr.run()
