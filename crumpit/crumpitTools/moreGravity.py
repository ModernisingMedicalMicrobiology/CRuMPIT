#!/usr/bin/python
from ete3 import NCBITaxa
import sys
import gzip
import functools
ncbi=NCBITaxa()

def returnLineages(taxid):
	return ncbi.get_lineage(int(taxid))

def lastCommon(tax1,tax2):
	return [t1 for t1,t2 in zip(tax1,tax2) if t1==t2]

def lca(reads):
	# get list of taxes from centrifuge reads
	taxes=[r.split('\t')[2] for r in reads if r.split('\t')[2] != '0']
	if len(taxes)<=1:
		try:
			return taxes[0]
		except: return 0

        # get lineages from taxids
	try:
		lins=map(returnLineages,taxes)
	except:
		return str(1)

	# get last common acestor of first two lineages
	pl=functools.reduce(lastCommon, lins)
	return str(pl[-1])

def processFile(f,o):
	try:
		reads=gzip.open(f,'r').read()
	except:
		reads=open(f,'r').read()
	reads=reads.split('\n')[1:-1]
	preName = None
	with gzip.open(o,'wt') as outf:
		for i in range(len(reads)):
			c=reads[i].split('\t')
			if int(c[-1]) > 1 and c[0] != preName:
				preName=c[0]
				c[2]=lca(reads[i:i+int(c[-1])])
				c[-1]="1"
				outf.write("\t".join(c) + "\n")
			elif c[0] != preName:
				outf.write(reads[i] + "\n")

if __name__ == '__main__':
	processFile(sys.argv[1],sys.argv[2])
