#!/usr/bin/env python
import sys
from pymongo import MongoClient
import gzip
from argparse import ArgumentParser
from Bio import SeqIO

class mongPusher:
    def __init__(self,sample,ip='127.0.0.1',port=27017):
            client = MongoClient(ip, port)
            self.name = sample
            self.db = client[self.name]
            self.collection = self.db.cent_stats

#    def _iterFq_(self,fq):
#        for SeqIO.parse(open(fq,'rt','fastq'))

    def getData(self,cf,fq):
        albf=['runid', 'read', 'ch', 'start_time', 'barcode']
        l=[]
        for line,seq in zip(gzip.open(cf,'rt'),SeqIO.parse(open(fq,'rt'),'fastq')):
            #for line in open(f,'r'):
            line=line.replace('\n','')
            li=line.split('\t')
            desc=seq.description.split(' ')
            ddesc={d.split('=')[0] : d.split('=')[-1] for d in desc[1:] if d.split('=')[0] in albf}
            p = {"read_id":str(li[0]),
                 "seqID": str(li[1]),
                 "taxID": int(li[2]),
                 "score": int(li[3]),
                 "s2ndBestScore": int(li[4]),
                 "hitLength": int(li[5]),
                 "queryLength": int(li[6]),
                 "numMatches": int(li[7]) }
            p.update(ddesc)
            l.append(p)
        return l

    def pushPosts(self,stats):
        posts=self.collection
        posts_id = posts.insert_many(stats)

def pushCentArgs(parser):
    parser.add_argument('-s', '--sample_name', required=True,
                         help='Specify sample name as used in mongoDB.')
    parser.add_argument('-ip', '--ip', required=False, default='127.0.0.1',
                         help='IP address for mongoDB database')
    parser.add_argument('-p', '--port', required=False, default=27017,type=int,
                         help='port address for mongoDB database')
    parser.add_argument('-cf', '--cent_file',required=True,
                         help='Centrifuge raw/reduce file in gzip compression.')
    parser.add_argument('-fq', '--fq_file',required=True,
                         help='fastq file')
    return parser

def pushCentRun(opts):
    m=mongPusher(opts.sample_name,ip=opts.ip,port=opts.port)
    s=m.getData(opts.cent_file,opts.fq_file)
    m.pushPosts(s)


if __name__ == "__main__":
    # args
    parser = ArgumentParser(description='centrifuge detail pusher')
    parser = pushCentArgs(parser)
    opts, unknown_args = parser.parse_known_args()
    # run

    pushCentRun(opts)
