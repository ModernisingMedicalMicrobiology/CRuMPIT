#!/usr/bin/env python3
import mysql.connector
import uuid
import json
from datetime import datetime
import logging
import bson.int64
import re

from crumpitTools.config_parser import cmptConf

class sqlConnect:
    '''Class for connecting to SQL database'''
    def __init__(self, sqlip='localhost', sqlport=3306, user='crumpit', password='CrumpitUserP455!', database='NanoporeMeta'):
        self.sqlip = sqlip
        self.sqlport = sqlport
        self.user = user
        self.password = password
        self.database = database
        self.activeConnection = 0

        fileStream = logging.FileHandler('crumpit_sqlConnect.log')
        fileStream.setLevel(logging.DEBUG)
        consoleStream = logging.StreamHandler()
        consoleStream.setLevel(logging.ERROR)
        logging.basicConfig(level=logging.DEBUG,
            format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
            datefmt='%Y-%m-%d %H:%M:%S',
            handlers=[
                fileStream,
                consoleStream]
            )

        c=cmptConf('/etc/crumpit.config')
        wfconfig=dict(c.config.items('crumpit'))
        mongosqlmap = wfconfig['mongosqlmap']

        if mongosqlmap:
            try:
                with open(mongosqlmap, 'r') as fp:
                    self.mongosqlmap = json.load(fp)
            except Exception as e:
                logging.exception('ERROR: Could not read mongo to SQL map')
                logging.exception(e)
        else:
            self.mongosqlmap = None

        self.resetSqlConnection()

    def resetSqlConnection(self):
        self.activeConnection = 0
        try:
            self.conn = mysql.connector.connect(user=self.user, database=self.database, host=self.sqlip, port=self.sqlport, password=self.password, connection_timeout=10, use_pure=True)
            self.cursor = self.conn.cursor(dictionary=True)
            self.activeConnection = 1
        except mysql.connector.Error as err:
            logging.exception("Something went wrong connecting to the SQL DB: {}".format(err))
        return self.activeConnection

    def __del__(self):
        try:
            self.cursor.close()
            self.conn.close()
        except Exception as e:
            print(e)

    def getTables(self):
        if not self.activeConnection:
            self.resetSqlConnection()

        try:
            query = ("SELECT DISTINCT TABLE_NAME, COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = %s")
            self.cursor.execute(query, (self.database,))

            tableColumns = {}
            for row in self.cursor:
                if row['TABLE_NAME'] in tableColumns:
                    tableColumns[row['TABLE_NAME']].append(row['COLUMN_NAME'])
                else:
                    tableColumns[row['TABLE_NAME']] = [row['COLUMN_NAME']]
            
            return tableColumns
        
        except mysql.connector.Error as err:
            logging.exception("Could not access DB: {}".format(err))
            return {}

    def getTableColumns(self, table):
        if not self.activeConnection:
            self.resetSqlConnection()

        try:
            query = ("SELECT DISTINCT COLUMN_NAME, IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = %s AND TABLE_NAME = %s")
            self.cursor.execute(query, (self.database, table))

            columns = {}
            for row in self.cursor:
                columns[row['COLUMN_NAME']] = row['IS_NULLABLE']
            
            return columns
        
        except mysql.connector.Error as err:
            logging.exception("Could not access DB: {}".format(err))
            return []

    def getNumRuns(self, name:str):
        if not self.activeConnection:
            self.resetSqlConnection()

        try:
            query = ("SELECT COUNT(ID) as RunNumber FROM Run WHERE sample_name = %s")
            self.cursor.execute(query, (name,))

            for row in self.cursor:
                return row['RunNumber']

        except mysql.connector.Error as err:
            logging.exception("Could not access runs DB: {}".format(err))
            return -1

    def getRun(self, name:str):
        if not self.activeConnection:
            self.resetSqlConnection()

        try:
            query = ("SELECT * FROM Run WHERE sample_name = %s")
            self.cursor.execute(query, (name,))

            for row in self.cursor:
                return row

        except mysql.connector.Error as err:
            logging.exception("Could not access runs DB: {}".format(err))
            return -1

    def getBarcodes(self, runID:bytes):
        if not self.activeConnection:
            self.resetSqlConnection()

        try:
            query = ("SELECT * FROM Barcode WHERE RunID = %s")
            self.cursor.execute(query, (runID,))

            barcodes = {}
            for row in self.cursor:
                barcodes[row['barcode']] = row
            return barcodes

        except mysql.connector.Error as err:
            logging.exception("Could not access barcode DB: {}".format(err))
            return None

    def getKingdoms(self, barcodeID:bytes):
        if not self.activeConnection:
            self.resetSqlConnection()

        try:
            query = ("SELECT * FROM Kingdom WHERE BarcodeID = %s")
            self.cursor.execute(query, (barcodeID,))

            kingdoms = {}
            for row in self.cursor:
                kingdoms[row['name']] = row
            return kingdoms

        except mysql.connector.Error as err:
            logging.exception("Could not access kingdom DB: {}".format(err))
            return None

    def getClassifiedSpecies(self, kingdomID:bytes):
        if not self.activeConnection:
            self.resetSqlConnection()

        try:
            query = ("SELECT * FROM `Classified Species` WHERE KingdomID = %s")
            self.cursor.execute(query, (kingdomID,))

            species = {}
            for row in self.cursor:
                species[row['TaxID']] = row
            return species

        except mysql.connector.Error as err:
            logging.exception("Could not access classified species DB: {}".format(err))
            return None

    def getPreRunInfo(self):
        if not self.activeConnection:
            self.resetSqlConnection()

        try:
            query = ("SELECT sample_name, run_date, porechop, watch_hours, map AS mapping, basecalling, flow, seq_kit, bar_kit, TaxID, reference_path FROM Run LEFT JOIN `Mapped Species` ON `Mapped Species`.RunID = Run.ID;")
            self.cursor.execute(query)
            mongoMap = self.mongosqlmap['Run']

            info = {}
            for row in self.cursor:
                if row['sample_name'] in info:
                    info[row['sample_name']]['mapping'] += ' {}'.format(row['TaxID'])
                    if row['reference_path'] != None:
                        info[row['sample_name']]['custom_refs'] += ' {}'.format(row['reference_path'])
                else:
                    info[row['sample_name']] = {'sample_name':row['sample_name'], 'porechop':row['porechop'], 'flow':row['flow'], 'seq_kit':row['seq_kit'], 'bar_kit':row['bar_kit']}
                    if row['basecalling'] == 1:
                        info[row['sample_name']]['basecalling'] = 'true'
                    else:
                        info[row['sample_name']]['basecalling'] = 'false'

                    if row['TaxID'] == None:
                        if row['mapping'] == '0':
                            info[row['sample_name']]['mapping'] = 'off'
                        else:
                            info[row['sample_name']]['mapping'] = 'on'
                    else:
                        info[row['sample_name']]['mapping'] = '{}'.format(row['TaxID'])
                        if row['reference_path'] != None:
                            info[row['sample_name']]['custom_refs'] = '{}'.format(row['reference_path'])
                    
                    if 'watch_hours' in mongoMap:
                        info[row['sample_name']][mongoMap['watch_hours']] = str(row['watch_hours'])
                    else:
                        info[row['sample_name']]['watchHours'] = str(row['watch_hours'])
                    
                    if 'run_date' in mongoMap:
                        info[row['sample_name']][mongoMap['run_date']] = row['run_date']
                    else:
                        info[row['sample_name']]['run_date'] = row['run_date']

            return info

        except mysql.connector.Error as err:
            logging.exception("Could not access runs DB: {}".format(err))
            return -1

    def getMappedSpecies(self, sample_name: str):
        if not self.activeConnection:
            self.resetSqlConnection()

        try:
            query = ("SELECT m.ID, m.RunID, m.TaxID, m.reference_path FROM `Mapped Species` AS m JOIN Run ON m.RunID = Run.ID WHERE sample_name = %s")
            print(query)
            self.cursor.execute(query, (sample_name,))

            species = {}
            for row in self.cursor:
                print("row to add {}".format(row))
                species[bytes(row['ID'],"utf-8")] = row
                print("new species {}".format(species))
            return species

        except mysql.connector.Error as err:
            logging.exception("Could not access mapped species DB: {}".format(err))
            return None

    def getDepthStats(self, sample_name: str, barcode: str=None, chrom: str=None):
        testBarcode = None
        if (barcode != None):
            testBarcodes = re.findall(r"[a-zA-Z]+\d{1,2}[a-zA-Z]*$", barcode)
            if len(testBarcodes) < 1:
                logging.info("Barcode {} not in expected format, using as raw values".format(barcode))
                testBarcode = barcode
            else:
                if re.search(r"[a-zA-Z]+0\d[a-zA-Z]*$", barcode):
                    testBarcode = re.findall(r"\d[a-zA-Z]*$", barcode)[0]
                else:
                    testBarcode = re.findall(r"\d{2}", barcode)[0]
        
        runID = self.getRun(sample_name)['ID']

        if not self.activeConnection:
            self.resetSqlConnection()

        try:
            if testBarcode != None and chrom != None:
                query = ("SELECT `Depth Stats`.ID as ID, `Depth Stats`.ID_text as ID_text, barcode, taxid, chrom, cov_avg, cov_stdv, len, x1, x5, x10, x20 \
                    FROM `Depth Stats` \
                    JOIN Barcode ON `Depth Stats`.BarcodeID = Barcode.ID \
                    WHERE Barcode.barcode = %s and Barcode.RunID = %s and `Depth Stats`.chrom = %s")
                self.cursor.execute(query, (testBarcode, runID, chrom))
            elif testBarcode != None:
                query = ("SELECT `Depth Stats`.ID as ID, `Depth Stats`.ID_text as ID_text, barcode, taxid, chrom, cov_avg, cov_stdv, len, x1, x5, x10, x20 \
                    FROM `Depth Stats` \
                    JOIN Barcode ON `Depth Stats`.BarcodeID = Barcode.ID \
                    WHERE Barcode.barcode = %s and Barcode.RunID = %s")
                self.cursor.execute(query, (testBarcode, runID))
            elif chrom != None:
                query = ("SELECT `Depth Stats`.ID as ID, `Depth Stats`.ID_text as ID_text, barcode, taxid, chrom, cov_avg, cov_stdv, len, x1, x5, x10, x20 \
                    FROM `Depth Stats` \
                    JOIN Barcode ON `Depth Stats`.BarcodeID = Barcode.ID \
                    WHERE Barcode.RunID = %s and `Depth Stats`.chrom = %s")
                self.cursor.execute(query, (runID, chrom))
            else:
                query = ("SELECT `Depth Stats`.ID as ID, `Depth Stats`.ID_text as ID_text, barcode, taxid, chrom, cov_avg, cov_stdv, len, x1, x5, x10, x20 \
                    FROM `Depth Stats` \
                    JOIN Barcode ON `Depth Stats`.BarcodeID = Barcode.ID \
                    WHERE Barcode.RunID = %s")
                self.cursor.execute(query, (runID,))

            results = {}
            for row in self.cursor:
                results[str(row['ID'])] = row
            return results

        except mysql.connector.Error as err:
            logging.exception("Could not access depth stats DB: {}".format(err))
            return None


    #def __createInsertQuery(self, values:dict, fkID:bytes=None, fkColumn:str=None):
    def __createInsertQuery(self, values:dict, fkDict:dict=None):
        columnsText = "(ID"
        valuesText = "VALUES (%(ID)s"
        valuesData = { 'ID': uuid.uuid4().bytes }

        # if fkID:
        #     columnsText += ", {}".format(fkColumn)
        #     valuesText += ", %(fkID)s"
        #     valuesData['fkID'] = fkID
        
        if fkDict:
            for (fkColumn, fkID) in fkDict.items():
                columnsText += ", {}".format(fkColumn)
                valuesText += ", %(" + fkColumn + ")s"
                valuesData[fkColumn] = fkID

        for (column, value) in values.items():
            columnsText += ", " + column
            valuesText += ", %(" + column + ")s"

            if isinstance(value, list) and len(value) == 0:
                valuesData[column] = "NULL"
            elif isinstance(value, list):
                valuesData[column] = value[0]
            elif isinstance(value, str) and (value.lower() == "false" or value.lower() == 'n'):
                valuesData[column] = 0
            elif isinstance(value, str) and (value.lower() == "true" or value.lower() == 'y'):
                valuesData[column] = 1
            elif isinstance(value, bson.int64.Int64):
                valuesData[column] = str(value)
            else:
                valuesData[column] = value
        
        columnsText += ") "
        valuesText += ")"
        return (columnsText + valuesText, valuesData)

    def __createUpdateQuery(self, values:dict, ID:bytes):
        queryText = ""
        valuesData = {  }        

        first = True
        for (column, value) in values.items():
            if not first:
                queryText += ", "
            else:
                first = False
            
            queryText += column + " = %(" + column + ")s"

            if isinstance(value, list) and len(value) == 0:
                valuesData[column] = None
            elif isinstance(value, list):
                valuesData[column] = value[0]
            elif isinstance(value, str) and (value.lower() == "false" or value.lower() == 'n'):
                valuesData[column] = 0
            elif isinstance(value, str) and (value.lower() == "true" or value.lower() == 'y'):
                valuesData[column] = 1
            elif isinstance(value, bson.int64.Int64):
                valuesData[column] = str(value)
            else:
                valuesData[column] = value
        
        queryText += " WHERE ID = %(ID)s"
        valuesData['ID'] = ID
        return (queryText, valuesData)

    def __getMongoValue(self, post:dict, path:str):
        split = path.find('/')
        if split != -1:
            return self.__getMongoValue(post[path[:split]], path[split+1:])
        else:
            return post[path]

    def __getMapInfo(self, post:dict):
        mapping = None
        splitMap = None
        if 'map' not in post: 
            logging.debug('mapping is switched off for run {}'.format(post['sample_name']))
            mapping = False
        else:
            if isinstance(post['map'], str):
                if (str(post['map']).lower() == 'off'):
                    logging.debug('mapping is switched off for run {}'.format(post['sample_name']))
                    mapping = False
                elif str(post['map']).lower() == 'on':
                    logging.debug('mapping is switched on for run {} with no specific TaxIDs'.format(post['sample_name']))
                    mapping = True
                elif str(post['map']).lower() == 'all':
                    logging.debug('mapping is switched on for run {} to map all taxIDs'.format(post['sample_name']))
                    mapping = True
                    splitMap = 'all'
                else:
                    mapping = True
                    splitMap = str(post['map'][0]).split(' ')
            else:
                if (str(post['map'][0]).lower().strip() == 'off'):
                    logging.debug('mapping is switched off for run {}'.format(post['sample_name']))
                    mapping = False
                elif str(post['map'][0]).lower().strip() == 'on':
                    logging.debug('mapping is switched on for run {} with no specific TaxIDs'.format(post['sample_name']))
                    mapping = True
                elif str(post['map']).lower().strip() == 'all':
                    logging.debug('mapping is switched on for run {} to map all taxIDs'.format(post['sample_name']))
                    mapping = True
                    splitMap = 'all'
                else:
                    mapping = True
                    splitMap = str(post['map'][0]).strip().split(' ')
        return (mapping, splitMap)

    def __insertIntoRun(self, post: dict):
        columns = self.getTableColumns('Run')
        mongoMap = self.mongosqlmap['Run']
        run = {}

        for (column, nullable) in columns.items():
            if column in post:
                if column == 'map':
                    (mapping, splitMap) = self.__getMapInfo(post)
                    if mapping != None:
                        run[column] = mapping
                else:
                    run[column] = post[column]
            elif column in mongoMap and mongoMap[column] in post:
                run[column] = post[mongoMap[column]]
            elif not nullable:
                raise Exception('Field {} not found and is required.'.format(column))
            else:
                logging.debug('Field {} not found but is not required'.format(column))

        (queryText, valuesData) = self.__createInsertQuery(values=run)
        query = ("INSERT INTO Run " + queryText)

        if not self.activeConnection:
            self.resetSqlConnection()

        self.cursor.execute(query, valuesData)
        if self.cursor.rowcount < 1:
            logging.exception("Couldn't insert Run {}".format(post['sample_name']))
            raise mysql.connector.errors.Error("No rows inserted")
        else:
            logging.info("Inserted Run {}".format(post['sample_name']))
            return valuesData['ID']

    def __updateRun(self, post: dict, sqlTable:dict):
        columns = self.getTableColumns('Run')
        mongoMap = self.mongosqlmap['Run']
        run = {}

        for (column, nullable) in columns.items():
            if column in post:
                if column == 'map':
                    (mapping, splitMap) = self.__getMapInfo(post)
                    if mapping != None:
                        run[column] = mapping
                else:
                    run[column] = post[column]
            elif column in mongoMap and mongoMap[column] in post:
                if column != 'run_date' or ('run_date' in sqlTable and sqlTable['run_date'] == None):
                    run[column] = post[mongoMap[column]]
                else:
                    logging.debug('Field {} is only updated if null'.format(column))
            elif not nullable:
                raise Exception('Field {} not found and is required.'.format(column))
            else:
                logging.debug('Field {} not found but is not required'.format(column))

        (queryText, valuesData) = self.__createUpdateQuery(values=run, ID=sqlTable['ID'])
        query = ("UPDATE `{}` SET ".format('Run') + queryText)

        if not self.activeConnection:
            self.resetSqlConnection()

        self.cursor.execute(query, valuesData)
        if self.cursor.rowcount < 1:
            #logging.debug("No update on Run: {}".format(sqlTable['ID_text']))
            return -1
        else:
            logging.info("Updating Run {}".format(post['sample_name']))
            return self.cursor.rowcount

    def __insertIntoMappedSpecies(self, post:dict, runID:bytes):
        (mapping, splitMap) = self.__getMapInfo(post)
        
        if splitMap:
            custom_refs = None
            if ('custom_refs' in post):
                custom_refs = post['custom_refs'].split()

            mapList = []
            for i, species in enumerate(splitMap):
                if custom_refs is None:
                    mapList.append({'taxID':species})
                else:
                    mapList.append({'taxID':species, 'reference_path':custom_refs[i]})
            
            for entry in mapList:
                fkDict = { 'RunID': runID }
                (queryText, valuesData) = self.__createInsertQuery(values=entry, fkDict=fkDict)
                query = ("INSERT INTO `Mapped Species` " + queryText)

                if not self.activeConnection:
                    self.resetSqlConnection()

                self.cursor.execute(query, valuesData)
                if self.cursor.rowcount < 1:
                    raise mysql.connector.errors.Error("No rows inserted")
                else:
                    return valuesData['ID']

    def __insertIntoDepthStats(self, post:dict, barcodeID:bytes):
        (queryText, valuesData) = self.__createInsertQuery(values=post, fkDict={ 'BarcodeID': barcodeID })
        query = ("INSERT INTO `Depth Stats` " + queryText)
        print("query:{} values:{}".format(query, valuesData))

        if not self.activeConnection:
            self.resetSqlConnection()

        self.cursor.execute(query, valuesData)
        if self.cursor.rowcount < 1:
            raise mysql.connector.errors.Error("No rows inserted into Depth Stats")

    def __insertIntoFKTable(self, table:str, idVal:str, idColumn:str, post:dict, fkID:bytes, fkColumn:str):
        columns = self.getTableColumns(table)
        if table in self.mongosqlmap:
            mongoMap = self.mongosqlmap[table]
        else:
            mongoMap = None

        valuesDict = { idColumn:idVal }
        for (column, nullable) in columns.items():
            if column in post:
                valuesDict[column] = post[column]
            elif mongoMap and column in mongoMap:
                valuesDict[column] = self.__getMongoValue(post=post, path=mongoMap[column])
            elif not nullable:
                raise Exception('Field {} not found and is required.'.format(column))
        
        (queryText, valuesData) = self.__createInsertQuery(values=valuesDict, fkDict={ fkColumn : fkID  })
        query = ("INSERT INTO `{}` ".format(table) + queryText)

        if not self.activeConnection:
            self.resetSqlConnection()
        logging.debug("query: {} values: {}".format(query, valuesData))
        self.cursor.execute(query, valuesData)
        if self.cursor.rowcount < 1:
            raise mysql.connector.errors.Error("No rows inserted")
        else:
            return valuesData['ID']

    def __updateTable(self, table:str, idVal:bytes, post:dict, sqlTable:dict):
        columns = self.getTableColumns(table)
        if table in self.mongosqlmap:
            mongoMap = self.mongosqlmap[table]
        else:
            mongoMap = None

        valuesDict = { }
        for (column, nullable) in columns.items():
            if column in post:
                valuesDict[column] = post[column]
            elif mongoMap and column in mongoMap:
                valuesDict[column] = self.__getMongoValue(post=post, path=mongoMap[column])
            elif not nullable:
                raise Exception('Field {} not found and is required.'.format(column))
        
        (queryText, valuesData) = self.__createUpdateQuery(values=valuesDict, ID=idVal)
        query = ("UPDATE `{}` SET ".format(table) + queryText)

        if not self.activeConnection:
            self.resetSqlConnection()


        self.cursor.execute(query, valuesData)
        if self.cursor.rowcount < 1:
            #logging.debug("No update on {}: idVal :{}".format(table, sqlTable['ID_text']))
            return -1
        else:
            logging.debug("Updated on {}: idVal :{}".format(table, sqlTable['ID_text']))
            return self.cursor.rowcount

    def insertRun(self, post: dict):
        logging.info("Inserting Run {}".format(post['sample_name']))
        try:
            runID = self.__insertIntoRun(post=post)
            self.__insertIntoMappedSpecies(post=post, runID=runID)
            self.conn.commit()
        except Exception as e:
            logging.exception("Exception {}".format(e))
            logging.exception("Sample {}".format(post['sample_name']))
            logging.exception(post)
            return -1

        return self.updateRun(post=post)

    def updateRun(self, post:dict):
        logging.info("Updating Run {}".format(post['sample_name']))
        sqlRun = self.getRun(name=post['sample_name'])

        try:
            self.__updateRun(post, sqlRun)

            if 'barcodes' in post:
                sqlBarcodes = self.getBarcodes(sqlRun['ID'])
                for (mongoBarcode, barcodeValues) in post['barcodes'].items():
                    barcodeID = None
                    testBarcodes = re.findall(r"[a-zA-Z]+\d{1,2}[a-zA-Z]*$", mongoBarcode)
                    testBarcode = ''
                    if len(testBarcodes) < 1:
                        logging.info("Barcode {} not in expected format, saving as raw values".format(mongoBarcode))
                        testBarcode = mongoBarcode
                    else:
                        if re.search(r"[a-zA-Z]+0\d[a-zA-Z]*$", mongoBarcode):
                            testBarcode = re.findall(r"\d[a-zA-Z]*$", mongoBarcode)[0]
                        else:
                            testBarcode = re.findall(r"\d{2}", mongoBarcode)[0]

                    if testBarcode not in sqlBarcodes and testBarcode != 'nobarcode':
                        barcodeID = self.__insertIntoFKTable(table='Barcode',idVal=testBarcode, idColumn='barcode', post=barcodeValues, fkID=sqlRun['ID'], fkColumn='RunID')
                    elif testBarcode != 'nobarcode':
                        barcodeID = sqlBarcodes[testBarcode]['ID']
                        self.__updateTable(table='Barcode', idVal=barcodeID, post=barcodeValues, sqlTable=sqlBarcodes[testBarcode])
                    else:
                        logging.debug('Not saving {} entry: Run {}'.format(testBarcode, post['sample_name']))

                    if barcodeID:
                        sqlKingdoms = self.getKingdoms(barcodeID)
                        for (mongoKingdom, kingdomValues) in barcodeValues.items():
                            kingdomID = None
                            if mongoKingdom not in sqlKingdoms and mongoKingdom != 'total':
                                kingdomID = self.__insertIntoFKTable(table='Kingdom',idVal=mongoKingdom, idColumn='name', post=kingdomValues, fkID=barcodeID, fkColumn='BarcodeID')
                            elif mongoKingdom != 'total':
                                kingdomID = sqlKingdoms[mongoKingdom]['ID']
                                self.__updateTable(table='Kingdom', idVal=kingdomID, post=kingdomValues, sqlTable=sqlKingdoms[mongoKingdom])
                            else:
                                logging.debug('Not saving {} entry: Run {}, Barcode {}'.format(mongoKingdom, post['sample_name'], mongoBarcode))

                            if kingdomID:
                                sqlSpecies = self.getClassifiedSpecies(kingdomID)
                                if mongoKingdom != 'unclassified':
                                    try:
                                        for (mongoSpecies, speciesValues) in kingdomValues['spBases'].items():
                                            try:
                                                if mongoSpecies in kingdomValues['filtspecies']:
                                                    speciesValues['filtered'] = True
                                                else:
                                                    speciesValues['filtered'] = False

                                                if mongoSpecies not in sqlSpecies:
                                                    speciesID = self.__insertIntoFKTable(table='Classified Species',idVal=mongoSpecies, idColumn='TaxID', post=speciesValues, fkID=kingdomID, fkColumn='KingdomID')
                                                else:
                                                    speciesID = sqlSpecies[mongoSpecies]['ID']
                                                    self.__updateTable(table='Classified Species', idVal=speciesID, post=speciesValues, sqlTable=sqlSpecies[mongoSpecies])
                                            except Exception as e:
                                                logging.exception("Run {}, Barcode {}, Kingdom {}, Species {}".format(post['sample_name'], mongoBarcode, mongoKingdom, mongoSpecies))
                                                logging.exception("Exception: {}".format(e))
                                    except Exception as e:
                                        logging.exception("Run {}, Barcode {}, Kingdom {}".format(post['sample_name'], mongoBarcode, mongoKingdom))
                                        logging.exception("Exception: {}".format(e))

            self.conn.commit()
        except Exception as e:
            logging.exception("Could not update Run: {}".format(e))
            logging.exception("Run {}".format(post['sample_name']))
            return -1

        return 1

    def saveDepthStats(self, post:list):
        logging.info("saveDepthStats {}".format(post))
        print("saveDepthStats {}".format(post))
        if len(post) > 0:
            firstRecord = post[0]
            sample_name = firstRecord['sample']
        else:
            logging.exception("Could not save Depth Stats: No records to insert")
            logging.exception("Depth Stats {}".format(post))

            print("Could not save Depth Stats: No records to insert")
            print("Depth Stats {}".format(post))
            return -1

        print("post len")
        try:
            # Find exisiting records
            current_stats = self.getDepthStats(sample_name=sample_name)
            print("current_stats {}".format(current_stats))
            test = self.getRun(sample_name)['ID']
            print("test {} {}".format(test, type(test)))
            barcodes = self.getBarcodes(bytes(self.getRun(sample_name)['ID']))
            print("barcodes {} {}".format(barcodes, type(barcodes)))

            for entry in post:
                inserted = False

                testBarcodes = re.findall(r"[a-zA-Z]+\d{1,2}[a-zA-Z]*$", entry['barcode'])
                testBarcode = ''
                if len(testBarcodes) < 1:
                    logging.info("Barcode {} not in expected format, using raw values".format(entry['barcode']))
                    testBarcode = entry['barcode']
                else:
                    if re.search(r"[a-zA-Z]+0\d[a-zA-Z]*$", entry['barcode']):
                        testBarcode = re.findall(r"\d[a-zA-Z]*$", entry['barcode'])[0]
                    else:
                        testBarcode = re.findall(r"\d{2}", entry['barcode'])[0]

                for statsID, record in current_stats.items():
                    if (testBarcode == record['barcode'] and 
                    entry['taxid'] == record['taxid'] and
                    entry['chrom'] == record['chrom']):
                        # If exists update
                        del entry['barcode']
                        del entry['sample']
                        print("updating Depth Stats entry {}".format(entry))
                        logging.info("updating Depth Stats entry {}".format(entry))
                        logging.info(type(statsID))
                        self.__updateTable(table='Depth Stats', idVal=bytes(statsID), post=entry, sqlTable=record)
                        inserted = True
                        break
                # Else insert
                if not inserted:
                    print("Couldn't find Depth Stats, inserting")
                    del entry['barcode']
                    del entry['sample']
                    self.__insertIntoDepthStats(post=entry, barcodeID=bytes(barcodes[testBarcode]['ID']))

            print('commit Depth Stats')
            self.conn.commit()
        except Exception as e:
            logging.exception("Could not save Depth Stats: {}".format(e))
            logging.exception("Depth Stats {}".format(post))

            print("Could not save Depth Stats: {}".format(e))
            print("Depth Stats {}".format(post))
            return -1