#!/usr/bin/env python3
import sys
from Bio import SeqIO
from tqdm import tqdm
from glob import glob
import gzip
import os
import subprocess

class splitFQ:
	def __init__(self,fq):
		self.fq=fq
		self.barcodes={}
		self.run()

	def processSeq(self,seq):
		bar=str(seq.description).split(' ')[-1].split('=')[-1]
		self.barcodes.setdefault(bar,[]).append(seq)
		return bar, seq

	def readFq(self,fq):
		record_iter = SeqIO.parse(open(fq,'rt'),'fastq')
		self.codes=dict(map(self.processSeq,record_iter))

	def writeFqs(self,outf='basecalled_fastq'):
		if not os.path.exists(outf):
			os.makedirs(outf)
		fileDic={}
		for b in self.barcodes:
			fileN='{0}/{1}.fastq'.format(outf,b)
			with open(fileN, "a") as handle:
		        	count=SeqIO.write(self.barcodes[b], handle, "fastq")
			#print(b,count)

	def gzipFqs(self,outf='basecalled_fastq'):
		for b in self.barcodes:
			fileN='{0}/{1}.fastq'.format(outf,b)
			subprocess.call(['gzip','-f',fileN])

	def run(self):
		self.readFq(self.fq)
		self.writeFqs()

def getSums(pubfq='fastqs'):
	batches=glob("{0}/*/".format(pubfq))
	with gzip.open('sequencing_summary.txt.gz','wt') as outf:
		for b in tqdm(range(len(batches))):
			with open(batches[b] + 'sequencing_summary.txt', 'rt') as inf:
				for line in inf: outf.write(line)

def processFiles(pubfq='fastqs'):
	batches=glob("{0}/*/*.fastq".format(pubfq))
	for b in tqdm(range(len(batches))):
		splitFQ(batches[b])

def splitBarcodeRunAll(*args):
	processFiles()
	subprocess.call('gzip -f basecalled_fastq/*.fastq',shell=True)
	getSums()

if __name__=='__main__':
	#splitFQ(sys.argv[1])
	processFiles()
	getSums()
