#!/usr/bin/env python3
import sys
import json
import os
import subprocess
from Bio import SeqIO
import gzip
import pymongo
import pysam
from cigar import Cigar
from argparse import ArgumentParser, SUPPRESS
import os.path
from tqdm import tqdm
import json
import numpy as np
from bokeh.plotting import figure, output_file, save
from bokeh.layouts import column
from bokeh.models import HoverTool
import pandas as pd
import json

from crumpitTools.config_parser import cmptConf
from crumpitTools.sqlConnect import sqlConnect

def rolling_window(a,window):
    df = pd.DataFrame(a)
    df['avg'] = df.rolling(window).mean()
    y = df['avg'].values.tolist()[0::window]
    x = [i*window for i in range(len(y))]
    return x,y

class pullPile:
    def __init__(self,sample,barcode,base,sqlip='127.0.0.1',sqlport=3306,batchNum=0,pf=None,rf='metagenomic',outHtml=None, mo='on'):
        self.refDict=self.load_obj(base + '/refs.json')
        self.pileFile=pf
        self.rf=rf
        self.outHtml=outHtml
        self.basedir=base + '/'
        #client = MongoClient(ip, port)
        self.sqlConnection = sqlConnect(sqlip=sqlip, sqlport=sqlport, database='NanoporeMeta')
        self.sample_name = sample
        self.barcode=barcode
        self.plots = []
        self.mo=mo

    def getMpileup(self):
        '''return dictionary of each mpileup file'''
        posts=[]
        with open(self.pileFile,'rt') as pileUp:
            for line in pileUp:
                l=line.split('\t')
                p={ 'pos':int(l[1]),
                    'cov':int(l[2]),
                    'chrom':str(l[0]),
                    'barcode':self.barcode }
                yield p

        #return posts

    def whichRef(self,taxid):
        '''return reference file from json (dictionary) given taxid'''
        try:
            return self.refDict[taxid]
        except:
            # hack for tb
            try:
                if str(taxid)=='1763':
                    return self.refDict["83332"]
            except: return "no reference"
            return "no reference"

    def load_obj(self, name ):
        with open( name , 'r') as f:
            return json.load(f)

    def getAllPos(self):
        return self.sqlConnection.getDepthStats(sample_name=self.sample_name)

    def getSomePos(self,chrom):
        return self.sqlConnection.getDepthStats(sample_name=self.sample_name, barcode=self.barcode, chrom=chrom)

    def taxToRef(self,tax):
        if self.rf=='metagenomic' or self.rf[0]=='metagenomic':
            self.ref=self.whichRef(tax)
        else:
            self.ref=self.rf
        self.taxid = tax

    def refInfo(self):
        self.refStats,self.refDescript={},{}
        if  self.rf[0]=='metagenomic' or self.rf=='metagenomic':
            refFiles=['{0}/{1}'.format(self.basedir,self.ref)]
        else:
            refFiles=self.rf
        for refFile in refFiles:
            with gzip.open(refFile,'rt') as inF:
                for seq in SeqIO.parse(inF,'fasta'):
                    chrom=str(seq.id).replace('.','_')
                    self.refStats[chrom] = { 'len' : len(seq.seq) , 'covpos' : np.zeros(len(seq.seq)+1 ) }
                    self.refDescript[chrom] = seq.description

    def calcCover(self,hce,chrom):
        for h in hce:
            chrom=h['chrom'].replace('.','_')
            self.refStats[chrom]['covpos'][h['pos']]=h['cov'] 

    def prepPlots(self):
        outD = 'plots/{0}/{1}'.format(self.barcode,self.taxid)
        if not os.path.exists(outD):
            os.makedirs(outD)
        if self.outHtml!=None:
            output_file(self.outHtml)
        else:
            output_file('{0}/{1}.html'.format(outD,self.taxid))

    def plotCov(self,covpos,chrom):
        # data
        pos,coverage = rolling_window(covpos, 100)
        data = dict(pos=pos,
                coverage=coverage)

        # figure
        p = figure(title="Chrom: {0}:{3},Barcode {1}, Run: {2}".format(chrom,
            self.barcode,
            self.sample_name,
            self.refDescript[chrom]),
                x_axis_label='position on chromosome', 
                y_axis_label='Read coverage',
                plot_width=1200, plot_height=800)

        # draw figure
        p.line('pos', 'coverage', legend=chrom, line_width=2, source=data)

        # add hovertool 
        p.add_tools( HoverTool(tooltips = [
                ( 'pos',   '@pos{0,0}' ),
                ( 'coverage', '@coverage{0,0.0}' ),
            ],
            mode='vline' ))

        self.plots.append( p )

    def getCoverage(self):
        if self.pileFile!=None:
            hce=self.getMpileup()
            chrom=None
            self.calcCover(hce, chrom)
        for chrom in self.refStats:
            if self.pileFile==None:
                hce=self.getSomePos(self.barcode,chrom)
                self.calcCover(hce,chrom)
            self.refStats[chrom]['x1']=len(np.where( self.refStats[chrom]['covpos'] > 0 )[0])
            self.refStats[chrom]['x5']=len(np.where( self.refStats[chrom]['covpos'] > 4 )[0])
            self.refStats[chrom]['x10']=len(np.where( self.refStats[chrom]['covpos'] > 9 )[0])
            self.refStats[chrom]['x20']=len(np.where( self.refStats[chrom]['covpos'] > 19 )[0])
            if self.refStats[chrom]['x1'] > 0:
                self.refStats[chrom]['cov_avg']=np.sum(self.refStats[chrom]['covpos'])/self.refStats[chrom]['x1']
                self.refStats[chrom]['cov_stdv']=np.std(self.refStats[chrom]['covpos'])
            else:
                self.refStats[chrom]['cov_avg']=0
                self.refStats[chrom]['cov_stdv']=0
            self.plotCov(self.refStats[chrom]['covpos'],chrom)
            #self.refStats[chrom]['covpos']=list(self.refStats[chrom]['covpos'])
            del self.refStats[chrom]['covpos']

    def pushTable(self,df):
        records=json.loads(df.to_json(orient='records'))
        self.sqlConnection.saveDepthStats(records)


def runPullPile(opts):
    # tax,sample,barcode,base,ip='127.0.0.1',port=27017,pf=None,rf=None,outHtml=None
    pullP=pullPile(opts.sample_name,
            opts.barcode,
            opts.refbase,
            sqlip=opts.sqlip,
            pf=opts.depth_file,
            rf=opts.ref,
            outHtml=opts.outHtml,
            mo=opts.map_options)
    pullP.taxToRef(str(opts.tax))
    pullP.refInfo()
    pullP.prepPlots()
    pullP.getCoverage()

    save(column(pullP.plots))
    df=pd.DataFrame(pullP.refStats)
    df=df.T
    df['taxid']=opts.tax
    df['sample']=opts.sample_name
    df['barcode']=opts.barcode
    df=df.reset_index()
    df=df.rename({'index':'chrom'},axis=1)

    outD='mapCsvs/{0}'.format(opts.barcode)
    if not os.path.exists(outD):
        os.makedirs(outD)
    df.to_csv('{0}/{1}.csv'.format(outD,opts.tax),index=False)
    pullP.pushTable(df)

    return pullP.refStats

def pullPileArgs(parser):
    parser.add_argument('-s', '--sample_name', required=True,
                             help='Specify sample name as used in mongoDB.')
    parser.add_argument('-pf', '--depth_file',required=True,
                             help='pileup file')
    parser.add_argument('-b', '--barcode',required=False,default='none',
                             help='barcode name for multiplexed sample, default = none')
    parser.add_argument('-t', '--tax',required=True,
                             help='taxid')
    parser.add_argument('-r', '--refbase',required=True,
                             help='refbase folder')

    #optionals
    parser.add_argument('-sqlip', '--sqlip', required=False, default='127.0.0.1',
                              help='IP address for SQL database')
    parser.add_argument('-sqlp', '--sqlport', required=False, default=3306,type=int,
                              help='port address for SQL database')
    parser.add_argument('-rf', '--ref',required=False,default='metagenomic',nargs='+',
                             help='specific reference')
    parser.add_argument('-o', '--out_stats',required=False,default='out.json',
                             help='output json')
    parser.add_argument('-oh', '--outHtml',required=False,default=None,
                             help='output html')
    parser.add_argument('-mo','--map_options',required=False,nargs='+',default='on',
                             help='mapping options, on (metagenomic) or list of taxids')

    return parser


if __name__=="__main__":
    # args
    parser = ArgumentParser(description='get info from depth file')
    parser = pullPileArgs(parser)


    opts, unknown_args = parser.parse_known_args()
    # run script
    stats=runPullPile(opts)
    with open(opts.out_stats, 'w') as outfile:
        json.dump(stats, outfile)
