#!/usr/bin/env python
from Bio import  SeqIO
import sys

def splitFq(fq):
    runs={}
    for seq in SeqIO.parse(open(fq,'r'),'fastq'):
        runid=seq.description.split(' ')[1].split('=')[-1]
        runs.setdefault(runid,[]).append(seq)
    return runs

def mergeFq(dic):
    for r in dic:
        with open(outdir + '/' + r +'.fastq','a') as output_handle:
            SeqIO.write(dic[r],output_handle, "fastq")

if __name__ == '__main__':
    outdir=sys.argv[-1]
    for i in sys.argv[1:-1]:
        r=splitFq(i)
        mergeFq(r)
