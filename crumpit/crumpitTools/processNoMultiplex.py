#!/usr/bin/env python3
import sys
from argparse import ArgumentParser, SUPPRESS
import glob
from Bio import SeqIO


class processNoMultiplex:
    def __init__(self,**kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)
        self.seq=[]
        self.processFiles()

    def processFile(self,fq):
        for seq in SeqIO.parse(open(fq,'rt'),'fastq'):
            if 'barcode=' not in seq.description:
                seq.description = seq.description + ' barcode=nomultiplex'
            self.seq.append(seq)

        fn=fq.split('/')[-1]
        outf='{0}_{1}'.format('processed',fn)
        with open(outf,'wt') as outhandle:
            SeqIO.write(self.seq,outhandle,"fastq")

    def processFiles(self):
        fqs = glob.iglob('{0}/*.fastq'.format(self.fol), recursive=True)
        for fq in fqs:
            self.processFile(fq)

def prNoMult_args(parser):
    parser.add_argument('-fol', '--fol', required=True,
                        help='fastq folder')
    return parser


def prNoMult_run(opts):
    kditc=vars(opts)
    processNoMultiplex(**kditc)

if __name__ == '__main__':
    parser = ArgumentParser(description='process fastqs with no multiplexing')
    parser = prNoMult_args(parser)
    opts, unknown_args = parser.parse_known_args()
    prNoMult_run(opts)
