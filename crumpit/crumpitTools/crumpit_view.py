#!/usr/bin/env python
import sys
import json
from ete3 import NCBITaxa
ncbi = NCBITaxa()

def txname(taxid):
	taxid2name = ncbi.get_taxid_translator([taxid])
	return taxid2name[taxid]

def visJson(b):
	'''parse dictionary/json and return formatted strings in table format'''

	c=("{:<15} {:>10} {:>15} {:>10} {:>15} {:>10}\n".format("Barcode",
															 "Human",
															 "Bacteria",
															 "Viruses",
															 "Total(b)",
															 "Total(r)"))
	for barcode in b:
		if barcode == 'null': continue
		c+="{:<15} {:>10,} {:>15,} {:>10,} {:>15,} {:>10,}\n".format(str(barcode),
											int(b[barcode]['Human']['bases']),
											int(b[barcode]['Bacteria']['bases']),
											int(b[barcode]['Viruses']['bases']),
											int(b[barcode]['total']['bases']),
											int(b[barcode]['total']['reads']))


	crum=("{:<15} {:<30} {:>10} {:>15}\n".format("Barcode","Species","Cent bases","mm bases"))
	for barcode in b:
		for fl in b[barcode]['Bacteria']['filtspecies'].keys():
			spec=txname(int(fl))
			if b[barcode]['Bacteria']['minimap2'] is None: mm=0
			elif fl in b[barcode]['Bacteria']['minimap2']:
				spec="{:<30}".format(spec)
				mm="{:>15,}".format(b[barcode]['Bacteria']['minimap2'][fl]['bases'])
			else: mm=0
			crum+=("{:<15} {:<30} {:>10,} {:>15}\n".format(barcode,
												spec,
												b[barcode]['Bacteria']['filtspecies'][fl]['bases'],
												mm))
	for barcode in b:
		for fl in b[barcode]['Viruses']['filtspecies'].keys():
			spec=txname(int(fl))
			if b[barcode]['Viruses']['minimap2'] is None: mm=0
			elif fl in b[barcode]['Viruses']['minimap2']:
				spec="{:<30}".format(spec)
				mm="{:>15,}".format(b[barcode]['Viruses']['minimap2'][fl]['bases'])
			else: mm=0
			crum+=("{:<15} {:<30} {:>10,} {:>15}\n".format(barcode,
												spec,
												b[barcode]['Viruses']['filtspecies'][fl]['bases'],
												mm))


	return c,crum

if __name__=='__main__':
	b=json.load(open('barcodes.json','rb'))
	centrifuge,crumpit=visJson(b)
	print(centrifuge)
	print(crumpit)
