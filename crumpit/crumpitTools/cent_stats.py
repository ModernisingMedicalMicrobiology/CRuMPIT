#!/usr/bin/env python3
from pymongo import MongoClient
from bson.objectid import ObjectId
import sys
from ete3 import NCBITaxa

ncbi = NCBITaxa()

def taxIDstats(taxid):
    if taxid == 0: return [0],'Unclassified'
    descendants = ncbi.get_descendant_taxa(taxid, intermediate_nodes=True)
    descendants.append(int(taxid))
    taxid2name = ncbi.get_taxid_translator([taxid])
    return descendants,taxid2name[taxid]

def getSpecies(taxes):
    l=ncbi.get_lineage(taxes)
    r=ncbi.get_rank(l)
    return {'species':i for i in l if r[i] == 'species'}

class mStats:
    def __init__(self,dbname,thresh=150,mdbip='127.0.0.1',mdbport=27017,taxes=None,last=False):
        client = MongoClient(mdbip, mdbport)
        self.dbname=str(dbname)
        self.db = client[dbname]
        self.thresh=float(thresh)
        self.getRunMeta()
        self.barcodes={}
        self.tax=taxes
        self.prepBarDicts()
        self.last = last
        self.tbtaxes,tbname = taxIDstats(1763)

    def barcodeDicts(self,b):
        b=str(b)
        if b not in self.barcodes: self.barcodes[str(b)]={}
        for i in ['unclassified','total']:
            if i not in self.barcodes[b]:
                self.barcodes[b]['unclassified']={'bases':0,'reads':0}
                self.barcodes[b]['total']={'bases':0,'reads':0}
        #self.barcodes[b]['spBases']={}
        #self.barcodes[b]['spReads']={}
        for k in self.tax:
            if k not in self.barcodes[b]: self.barcodes[b][k]={'bases':0,'reads':0,'spBases':{}}

    def prepBarDicts(self):
        for b in self.bars:
            self.barcodeDicts(b)

    def getRunMeta(self):
        collection = self.db.cent_stats
        self.bars= collection.distinct('barcode')
        if len(self.bars)==0:
            self.bars.append('nobarcode')
        self.runs = collection.distinct('run_id')


    def taxStats(self,tax):
        collection = self.db.cent_stats
        #if self.barcode is None: hce = collection.find({"score": {"$gt": self.thresh-1},"taxID": {"$in": tax},"_id": {"$gte": ObjectId('59fd85319714e753428c11e8')}})
        if self.barcode is None: hce = collection.find({"score": {"$gt": self.thresh-1},"taxID": {"$in": tax}})
        else: hce = collection.find({"score": {"$gt": self.thresh-1},"taxID": {"$in": tax},"barcode": self.barcode})
        return hce

    def allStats(self):
        collection = self.db.cent_stats
        if self.last != False:
            hce = collection.find({"_id": {"$gt": self.last}})
        else:
            hce = collection.find()
        return hce

    def countKindom(self,i,b,kingdon):
        if self.tax2kingdom[int(i['taxID'])]==kingdon:
            sd=getSpecies(int(i['taxID']))
            if 'species' in sd:
                if str(sd['species']) not in self.barcodes[b][kingdon]['spBases']:
                    self.barcodes[b][kingdon]['spBases'][str(sd['species'])]={}
                    self.barcodes[b][kingdon]['spBases'][str(sd['species'])]['bases']=0
                    self.barcodes[b][kingdon]['spBases'][str(sd['species'])]['reads']=0
                self.barcodes[b][kingdon]['spBases'][str(sd['species'])]['bases']+=i['queryLength']
                self.barcodes[b][kingdon]['spBases'][str(sd['species'])]['reads']+=1


    def countBases(self):
        self.kingdoms={}
        for k in self.tax:
             self.kingdoms[k]=taxIDstats(self.tax[k])[0]
             #for b in self.barcodes:
             #    self.barcodes[b][k]={'bases':0,'reads':0,'spBases':{}}
        self.tax2kingdom = dict((v, k) for k in self.kingdoms for v in self.kingdoms[k])
        #cents=self.taxStats(kingdom[0])
        cents=self.allStats()
        for i in cents:
            self.last=ObjectId(i['_id'])
            if 'barcode' in i:
                b=i['barcode']
                if b not in self.barcodes: self.barcodeDicts(str(b)) # incase its a new barcode not from the start
            else: continue #b='nobarcode'
            self.barcodes[b]['total']['bases']+=(i['queryLength'])
            self.barcodes[b]['total']['reads']+=1
            if i['taxID'] == 0:
                self.barcodes[b]['unclassified']['bases']+=i['queryLength']
                self.barcodes[b]['unclassified']['reads']+=1
            if i['score'] < self.thresh-1: continue
            if int(i['taxID']) in self.tax2kingdom:
                self.barcodes[b][self.tax2kingdom[int(i['taxID'])]]['bases']+=i['queryLength']
                self.barcodes[b][self.tax2kingdom[int(i['taxID'])]]['reads']+=1
                for kingdom in ['Bacteria','Viruses']: self.countKindom(i,b,kingdom)

            # for Mycobacterium, specifically look for genus and above reads
            if int(i['taxID']) in self.tbtaxes:
                if '1763' not in self.barcodes[b]['Bacteria']['spBases']:
                    self.barcodes[b]['Bacteria']['spBases']['1763']={}
                    self.barcodes[b]['Bacteria']['spBases']['1763']['bases']=0
                    self.barcodes[b]['Bacteria']['spBases']['1763']['reads']=0
                self.barcodes[b]['Bacteria']['spBases']['1763']['bases']+=i['queryLength']
                self.barcodes[b]['Bacteria']['spBases']['1763']['reads']+=1



def basic_stats():
    m=mStats(sys.argv[1])
    taxes={"Bacteria":2,"Human":9606,"Viruses":10239}
    for i in taxes:
        m.basicBases[taxes[i]]=m.countBases(taxes[i])
    baseStats=m.countUnclass()
    print(m.basicBases)

def bacteria_compo():
    m=mStats(sys.argv[1])
    m.kingdomSpecies(2)
    for i in m.spBases:
        print("{0}\t{1}\t{2}".format(ncbi.translate_to_names([i])[0],i,sum(m.spBases[i])))

if __name__=="__main__":
    basic_stats()
    bacteria_compo()
