#!/usr/bin/env python3
import sys
import json
import os
import subprocess
from Bio import SeqIO
import gzip
from pymongo import MongoClient
import pymongo
import pysam
import pandas as pd
from argparse import ArgumentParser, SUPPRESS
import os.path
from tqdm import tqdm 
from ete3 import NCBITaxa
ncbi = NCBITaxa()
import warnings
warnings.filterwarnings("ignore")


def getDescendants(t):
    taxids=ncbi.get_descendant_taxa(t,intermediate_nodes=True)
    #taxids.append(int(t))
    taxids.append(t)
    return taxids

centFields=['readID','seqID','TAXID','score','2ndBestScore','hitLength','queryLength','numMatches']
albf=['runid', 'read', 'ch', 'start_time', 'barcode']

#mapper object
class mapObj:
    def __init__(self,taxid,ref):
        self.taxid=taxid
        self.ref=ref
        self.seqs=[]

# pre map
class preMapper:
    def __init__(self,base,batchNum=0,mo='on',ref='metagenomic'):
        self.refDict=self.load_obj(base + 'refs.json')
        self.basedir=base
        self.batchNum=batchNum
        self.nomap=[None]
        self.mo=mo
        self.ref=ref
        self.mapType=ref

        if 'on' not in mo:
            self.moTaxids={}
            for t in mo:
                self.moTaxids[t]=getDescendants(t)
            self.moRevTaxids={}
            for k, v in self.moTaxids.items():
                for x in v:
                    self.moRevTaxids.setdefault(str(x),[]).append(str(k))

        if self.ref[0]!='metagenomic':
            self.refDict={}
            self.mapType='custom'
            assert len(self.ref)==len(self.mo), "If using taxID list and custom references, lists should be same lenth"
            for i,j in zip(self.mo,self.ref):
                for t in self.moTaxids[i]:
                    self.refDict[str(t)]=j


    def Spcs(self,taxes):
        '''return species if above species taxon'''
        if self.mo!='on':
            try:
                t=self.moRevTaxids[taxes]
                return t[0]
            except:
                pass
        try:
            l=ncbi.get_lineage(taxes)
            r=ncbi.get_rank(l)
            species=str({'species':i for i in l if r[i] == 'species'}['species'])
        except:
            species=str(taxes)
        return species

    def getTax(self,cent):
        '''take centrifuge input file (gzipped) and make dictionary of ids:ref,taxid'''
        cent=gzip.open(cent,'rt').readlines()
        cents=[c.split('\t') for c in cent]
        ids,taxids=zip(*[(i[0], self.Spcs(str(i[2]))) for i in cents])
        refs=map(self.whichRef,taxids)
        
        self.idToRef={}
        for bar in self.barcodes:
            self.idToRef[bar]={}
            
        for i,t,r in zip(ids,taxids,refs):
            if i == 'readID': continue
            bar=self.fqs[i]['barcode']
            if 'seqObj' in self.fqs[i]:
                self.idToRef[bar][i]={'ref':r,'taxid':t,'seqObj':self.fqs[i]['seqObj']}
            elif 'seqObj1' in self.fqs[i]:
                self.idToRef[bar][i]={'ref':r,
                        'taxid':t,
                        'seqObj1':self.fqs[i]['seqObj1'],
                        'seqObj2':self.fqs[i]['seqObj2']
                        }

    
    def whichRef(self,taxid):
        '''return reference file from json (dictionary) given taxid'''
        try:
            return self.refDict[taxid]
        except:
            return "no reference"
    
    def load_obj(self, name ):
        with open( name , 'r') as f:
            return json.load(f)

    def makeFol(self,fol):
        if not os.path.exists(fol):
            os.makedirs(fol)

    
    def getSeq(self,fq):
        '''create dictionary (self.fqs) of id:seqobject from fastq file'''
        self.fqs={}
        self.barcodes=set()
        with open(fq,'rt') as seqs:
            for seq in SeqIO.parse(seqs,'fastq'):
                desc=seq.description.split(' ')
                d={d.split('=')[0] : d.split('=')[-1] for d in desc[1:] if d.split('=')[0] in albf}
                if 'barcode' not in d:
                    d['barcode']='nobarcode'
                self.fqs[seq.id] = {'seqObj':seq,'barcode':d['barcode']}
                self.barcodes.add(d['barcode'])
    
    def getSeqs(self,fq1,fq2):
        '''create dictionary (self.fqs) of id:seqobject from fastq file'''
        self.fqs={}
        self.barcodes=['nobarcode']
        with open(fq1,'rt') as seqs1, open(fq2,'rt') as seqs2:
            for seq1,seq2 in zip(SeqIO.parse(seqs1,'fastq'),SeqIO.parse(seqs2,'fastq')):
                try:
                    assert seq1.id[:-2] == seq2.id[:-2]
                except AssertionError:
                    print(seq1.id[:-2], seq2.id[:-2])
                self.fqs[seq1.id[:-2]] = {'seqObj1':seq1,'seqObj2':seq2,'barcode':'nobarcode'}


    def makeindex(self, r):
        if self.mapType!='custom':
            l=['minimap2', '-d', "{0}{1}.mmi".format(self.basedir,r),"{0}{1}".format(self.basedir,r)] 
        else:
            l=['minimap2', '-d', "{}.mmi".format(r),"{}".format(r)] 
        subprocess.check_output(l)
    
    def run_minimap(self,f,r,opt='map-ont'):
        if self.mapType!='custom':
            ref=self.basedir +  r + '.mmi'
        else:
            ref= r + '.mmi'
        if not  os.path.isfile(ref): self.makeindex(r)
        l=['minimap2','-L','-ax', opt, ref, '/dev/stdin' ]
        fastqs='\n'.join([i.format('fastq') for i in f])
        outsam = subprocess.check_output(l,universal_newlines=True,input=fastqs,stderr=subprocess.DEVNULL)
        #outsam=fakeBam
        return outsam

    def makeBins(self):
        '''bin on barcode then reference'''
        self.bins={}
        for bar in self.barcodes:
            self.bins[bar]={}

        for b in self.idToRef:
            for i in self.idToRef[b]:
                r=self.idToRef[b][i]['ref']
                t=self.idToRef[b][i]['taxid']

                if self.mapType=='custom' and 'all' not in self.mo and 'on' not in self.mo:
                    if str(t) not in self.moRevTaxids:  continue

                if self.mapType=='custom' and self.mo[0]=='all':
                    assert len(self.ref)==1,"If using all for mo, should only be single reference genome file"
                    t='all'
                    r=self.ref[0]

                if 'seqObj' in self.idToRef[b][i]:
                    s=self.idToRef[b][i]['seqObj']
                    s2=False
                elif 'seqObj1' in self.idToRef[b][i]:
                    s=self.idToRef[b][i]['seqObj1']
                    s2=self.idToRef[b][i]['seqObj2']

                if r not in self.bins[b]:
                    self.bins[b][r]=mapObj(t,r)
                self.bins[b][r].seqs.append(s)
                if s2:
                    self.bins[b][r].seqs.append(s2)


    def writeSam(self,t):
        # to sam file
        sam,bar,taxid=t[0],t[1],t[2]
        outfol='sams/{0}'.format(bar)
        self.makeFol(outfol)
        outFileName='{0}/{1}_{2}.sam'.format(outfol,taxid,self.batchNum)
        with open(outFileName,'wt') as outf:
            outf.write(sam)

        # to bam file
        outfol='bams/{0}'.format(bar)
        self.makeFol(outfol)
        bamFileName='{0}/{1}_{2}.bam'.format(outfol,taxid,self.batchNum)
        bam=pysam.view('-F','4','-b',outFileName)
        with open(bamFileName,'wb') as outf:
            outf.write(bam)

        # stop for empty bams
        checkSam=pysam.view('-F','4','-S',bamFileName)
        if len(checkSam.split('\n')) == 1:
            self.nomap.append(sam)
            return None
        # to sorted bam file and index
        outfol='sorted/{0}'.format(bar)
        self.makeFol(outfol)
        sortFileName='{0}/{1}_{2}.sorted.bam'.format(outfol,taxid,self.batchNum)
        pysam.sort('-o',sortFileName,bamFileName)
        pysam.index(sortFileName)

        return sortFileName

    
    def map_each(self,cf,sf,r2=False):
        '''map each sequence from centrifuge and fastq files'''
        if r2==False:
            self.getSeq(sf)
        else:
            self.getSeqs(sf,r2)
        self.getTax(cf)
        self.makeBins()
        sams=[]

        for bar in self.bins:
            print('mapping barcode: {0}'.format(bar))
            print('Maptype: {0}'.format(self.mapType))
            for ref in tqdm(self.bins[bar]):
                if ref == "no reference": continue
                fqs=self.bins[bar][ref].seqs
                if r2==False:
                    sam=self.run_minimap(fqs,ref)
                else:
                    sam=self.run_minimap(fqs,ref,opt='sr')
                taxid=self.bins[bar][ref].taxid
                sams.append((sam,bar,taxid))

        samFiles=map(self.writeSam,sams)
        self.bamFiles=list(samFiles)
        self.bamFiles=set(self.bamFiles)-set(self.nomap)

########### SEND BAM TO DB #####################


class mongPusher:
    def __init__(self,sample,ip='127.0.0.1',port=27017):
        client = MongoClient(ip, port)
        self.name = sample
        self.db = client[self.name]
        self.collection = self.db.map_stats
        self.collection.create_index([("chrom", pymongo.DESCENDING),
            ("pos", pymongo.ASCENDING),
            ("barcode", pymongo.ASCENDING),
            ("TAXID", pymongo.ASCENDING)])
        self.posts=[]

    def getData(self,sam,taxid):
        '''return dictionary of bam info per read'''

        samfile = pysam.AlignmentFile(sam, "rb")
        for read in samfile.fetch():
            d={}
            readID = str(read.query_name)

            d['pos']    = int(read.reference_start)
            d['chrom']  = str(read.reference_name)
            d['tlen']   = int(read.infer_read_length())
            d['mapq']   = int(read.mapping_quality)
            d['readID'] = readID
            d['flag']   = int(read.flag)
            d['CIGAR']  = str(read.cigarstring)
            d['TAXID']  = taxid

            if 'seqObj' in self.fqs[readID]:
                desc=self.fqs[readID]['seqObj'].description.split(' ')
                descdict={d.split('=')[0] : d.split('=')[-1] for d in desc[1:] if d.split('=')[0] in albf}
              
                d.update(descdict)
            self.posts.append(d)

    def pushPosts(self):
        posts=self.collection
        posts_id = posts.insert_many(self.posts)

def preMapArgs(parser):
    parser.add_argument('-s', '--sample_name', required=True, 
                             help='Specify sample name as used in mongoDB.')
    parser.add_argument('-cf', '--cent_file',required=True,
                             help='Centrifuge raw/reduce file in gzip compression.')
    parser.add_argument('-fq', '--fq_file',required=True,
                             help='fastq file')
    parser.add_argument('-fq2', '--fq_file2',required=False,default=False,
                             help='second fastq file if mate paired Illumina')
    parser.add_argument('-outf', '--outf',required=False,default='bams',
                             help='out folder for bam files, default=bams/')
    parser.add_argument('-n', '--batchNum',required=False,default=0,
                             help='batch number for bam files, default=0')
    parser.add_argument('-base', '--basedir',required=True,
                             help='base directory for the reference genomes, includuedes refs.json')
    parser.add_argument('-k', '--kingdoms',required=False,default='Bacteria',nargs='+',
                             help='Kingdoms to map to default=Bacteria')

    #optionals
    parser.add_argument('-ip', '--ip', required=False, default='127.0.0.1',
                             help='IP address for mongoDB database')
    parser.add_argument('-p', '--port', required=False, default=27017,type=int,
                             help='port address for mongoDB database')
    parser.add_argument('-mo','--map_options',required=False,nargs='+',default=['on'],
                             help='mapping options, on (metagenomic) or list of taxids')
    parser.add_argument('-ref','--ref_options',required=False,default=['metagenomic'],nargs='+',
                             help='options for mapping, provide a reference file, default=metagenomic')
    return parser
                            

def runPreMap(opts):
    print(opts)
    basedir=opts.basedir + '/'
    p=preMapper(basedir,opts.batchNum,mo=opts.map_options,ref=opts.ref_options)
    p.map_each(opts.cent_file,opts.fq_file,r2=opts.fq_file2)

#    m=mongPusher(opts.sample_name,ip=opts.ip,port=opts.port)
#    m.fqs=p.fqs
#
#    for sam in p.bamFiles:
#        if sam==None:continue 
#        i=sam.split('/')
#        barcode = i[1]
#        taxid = i[2].split('_')[0]
#        m.getData(sam,taxid)
#
#    if len(m.posts) > 0: m.pushPosts()

if __name__=="__main__":
    # args
    parser = ArgumentParser(description='preMap component for Classflow clinical metagenomics classification workflow')
    parser = preMapArgs(parser)
    opts, unknown_args = parser.parse_known_args()

    # run script
    
    runPreMap(opts)

