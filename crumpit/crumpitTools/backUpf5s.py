#!/usr/bin/env python3
import sys
from argparse import ArgumentParser, SUPPRESS
import gzip
import tarfile
import os
import h5py

class backUpF5s:
    def __init__(self,**kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)
        self.parseNonHuman()
        if self.flist != None:
            self.parseF5list()
            self.batchNum=str(self.flist.split('/')[-1].replace('.batch',''))
            self.makeOut()
            self.processFiles()
        elif self.batch != None:
            self.processBatch()
        else:
            print('Need either flist (-l) or batch (-b) files')
            sys.exit()

    def makeOut(self):
        if not os.path.exists(self.out):
            os.makedirs(self.out)

    def processBatch(self):
        dr,fr=0,0
        with h5py.File(self.batch,'a') as f:
            for read in self.badList:
                r='read_{0}'.format(read)
                if r in f:
                    del f[r]
                    dr+=1
                else:
                    fr+=1
        print("deleted reads: {0}\nunfound reads: {1}\n".format(dr,fr))

    def processFiles(self):
        print(self.batchNum)
        tar=tarfile.open("{0}/{1}_f5s.tar.gz".format(self.out,self.batchNum),"w:gz")
        for i in self.fileNameLocation:
            tar.add(self.fileNameLocation[i],arcname=i)
        tar.close()

    def parseNonHuman(self):
        self.OKList=[]
        self.badList=[]
        for line in gzip.open(self.cent,'rt'):
            l=line.split('\t')
            if str(l[2]) != '9606':
                self.OKList.append(str(l[0]))
            else:
                self.badList.append(str(l[0]))

    def parseF5list(self):
        ''' take two files: sequencing_summary for id to read name, and list for file location.
        combine and return dictionary of read id to read file location'''
        self.idFileDict={}
        self.fileNameLocation={}
        for line in open(self.sequencing_summary,'rt'):
            l=line.split('\t')
            if l[1] not in self.OKList:
                self.idFileDict[l[0]]=l[1]

        for line in open(self.flist):
            l=line.split('/')
            f=l[-1].replace('\n','')
            if f not in self.idFileDict:
                self.fileNameLocation[f]=line.replace("\n","")

def bf5s_args(parser):

    parser.add_argument('-sum', '--sequencing_summary', required=True,
                        help='sequencing_summary from albacore/guppy')
    parser.add_argument('-l', '--flist', required=False,
                        help='list of fast5 files')
    parser.add_argument('-c', '--cent', required=True,
                        help='centrifuge file')
    parser.add_argument('-o', '--out', required=False,
                        help='output folder')
    parser.add_argument('-b', '--batch', required=False,
                        help='Multi fast5 file')
    return parser

def bf5s_run(opts):
    kditc=vars(opts)
    backUpF5s(**kditc)


if __name__ == '__main__':
    parser = ArgumentParser(description='back up non-human fast5 files to location')
    parser = bf5s_args(parser)
    opts, unknown_args = parser.parse_known_args()
    bf5s_run(opts)
