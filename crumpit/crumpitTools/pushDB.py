#!/usr/bin/env python
import sys
import sqlite3

def createTable(c):
    # Create table
    c.execute('''CREATE TABLE if not exists centrifuge
                         (readID primary key, seqID, taxID, score, s2ndBestScore, hitLength, queryLength, numMatches)''')

def insertData(many,c):
    # Insert a row of data
    c.executemany("INSERT OR REPLACE INTO centrifuge VALUES (?,?,?,?,?,?,?,?)",many)

def getData(f):
    #l=[i.split(',')[0:1] for i in open(f[0],'r').read().split('\n')]
    l=[]
    for line in open(f,'r'):
        line=line.replace('\n','')
        li=line.split('\t')
        l.append([str(li[0]),str(li[1]),int(li[2]),int(li[3]),int(li[4]),int(li[5]),int(li[6]),int(li[7])])
    return tuple(l)

def dropTab(c):
    c.execute('''drop table if exists centrifuge''')

def run():
    conn = sqlite3.connect(sys.argv[2])
    c = conn.cursor()
    #dropTab(c)
    createTable(c)
    t=getData(sys.argv[1])
    insertData(t,c)
    # Save (commit) the changes
    conn.commit()
    # We can also close the connection if we are done with it.
    # Just be sure any changes have been committed or they will be lost.
    conn.close()
if __name__ == '__main__':
    run()
