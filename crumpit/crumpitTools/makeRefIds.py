#!/usr/bin/env python
import sys
import ete3
import pickle

ncbi = ete3.NCBITaxa()

def getDescendants(taxid,refid):
    dnts = ncbi.get_descendant_taxa(taxid, intermediate_nodes=True)
    dnts.append(taxid)
    return {key: refid for key in dnts}

def save_obj(obj, name ):
    with open( name , 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name ):
    try:
        with open( name , 'rb') as f:
            return pickle.load(f)
    except:
        return {}


if __name__ == '__main__':
    #rp='/home/nick/albacore_tests/refs/refs.pkl'
    rp='refs.pkl'

    d=load_obj(rp)
    taxes=sys.argv[1].split(',')
    refs=sys.argv[2].split(',')

    for t,r in zip(taxes,refs):
        d.update(getDescendants(int(t),str(r)))

    save_obj(d,rp)
