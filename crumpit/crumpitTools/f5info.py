#!/usr/bin/env python3
import sys
import h5py
from argparse import ArgumentParser
from tqdm import tqdm
from pymongo import MongoClient
from bson.objectid import ObjectId
import logging

logging.basicConfig(level=logging.DEBUG, filename='crumpit.log')

class runDetails:
    """get run details and push to mongoDB runDetails"""
    def __init__(self, **kwargs):
        import h5py
        for k, v in kwargs.items():
            setattr(self, k, v)
        self.makeSets()
        self.processFiles()
        self.push()

    def processFiles(self):
        if self.batch_file != None:
            lines=open(self.batch_file).readlines()
            for line in tqdm(lines[0:100]):
                try:
                    self.addFileInfo(line.replace('\n',''))
                except:
                    logging.exception("addFileInfo:")
        elif self.f5_file != None:
            try:
                self.addFileInfo(self.f5_file)
            except:
                logging.exception("addFileInfo:")
        else:
            print("Need either batch file or bulk file")
            sys.exit()


    def makeSets(self):
        self.fields=[
        'experiment_type',
        'flowcell_type',
        'local_bc_temp_model',
        'sequencing_kit',
        'device_type',
        'flow_cell_id',
        'device_id',
        'protocols_version',
        'sample_id',
        'hostname']
        self.sets={}
        for field in self.fields:
            self.sets[field]=set()
        self.sets['run_name']=[bytes(self.run_name,"utf-8")]
        if self.batch_file != None:
            self.sets['batch']=[bytes(self.batch_file,"utf-8")]
        elif self.f5_file != None:
            self.sets['batch']=[bytes(self.f5_file,"utf-8")]


    def addFileInfo(self,f):
        hf5 = h5py.File(f, 'r')
        if self.batch_file != None:
            r1='UniqueGlobalKey'
        elif self.f5_file != None:
            r1=r1=list(hf5.keys())[0]
        vers=hf5.attrs['file_version']
        self.sets['experiment_type'].add(hf5[r1]['context_tags'].attrs['experiment_type'])
        self.sets['flowcell_type'].add(hf5[r1]['context_tags'].attrs['flowcell_type'])
        try:
            self.sets['local_bc_temp_model'].add(hf5[r1]['context_tags'].attrs['local_bc_temp_model'])
        except:
            self.sets['local_bc_temp_model'].add('mux')

        self.sets['sequencing_kit'].add(hf5[r1]['context_tags'].attrs['sequencing_kit'])
        self.sets['device_type'].add(hf5[r1]['tracking_id'].attrs['device_type'])
        self.sets['flow_cell_id'].add(hf5[r1]['tracking_id'].attrs['flow_cell_id'])
        self.sets['device_id'].add(hf5[r1]['tracking_id'].attrs['device_id'])
        self.sets['protocols_version'].add(hf5[r1]['tracking_id'].attrs['protocols_version'])
        self.sets['sample_id'].add(hf5[r1]['tracking_id'].attrs['sample_id'])
        self.sets['hostname'].add(hf5[r1]['tracking_id'].attrs['hostname'])

    def push(self):
        ins={k:cleanUp(v) for k,v in self.sets.items()}
        client = MongoClient(self.ip, self.port)
        self.db = client[self.run_name]
        self.collection = self.db.run_summary
        posts=self.collection
        posts_id = posts.insert_many([ins])

def cleanUp(s):
    r=[]
    try:
        for i in s:
            if type(i) !=  str:
                r.append(str(i.decode()))
    except:
        logging.exception("addFileInfo:")
    return r


class runInfoSum:
    def __init__(self,last=False,**kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)
        self.fields=[
        'experiment_type',
        'flowcell_type',
        'local_bc_temp_model',
        'sequencing_kit',
        'device_type',
        'flow_cell_id',
        'device_id',
        'protocols_version',
        'sample_id',
        'hostname']
        self.sets={}
        for field in self.fields:
            self.sets[field]=[]
        self.last = last
        client = MongoClient(self.ip, self.port)
        self.db = client[self.run_name]
        self.collection = self.db.run_summary
        self.getSummary()

    def docStats(self,h):
        for f in self.fields:
            for i in h[f]:
                # can't add set later on, so using set like list here
                if i not in self.sets[f]:
                    self.sets[f].append(i)

    def getSummary(self):
        if self.last==False: hce = self.collection.find()
        else: hce = self.collection.find({"_id": {"$gt": self.last}})
        for h in hce:
            self.last=ObjectId(h['_id'])
            self.docStats(h)

def f5info_args(parser):
    parser.add_argument('-r', '--run_name', required=True,
                         help='Specify sample name as used in mongoDB.')
    parser.add_argument('-ip', '--ip', required=False, default='127.0.0.1',
                         help='IP address for mongoDB database')
    parser.add_argument('-p', '--port', required=False, default=27017,type=int,
                         help='port address for mongoDB database')
    parser.add_argument('-b', '--batch_file',required=False,
                         help='batch file, list of fast5 files')
    parser.add_argument('-f5', '--f5_file',required=False,
                         help='fast5 file')
    return parser

def f5info_run(opts):
    kdict=vars(opts)
    m=runDetails(**kdict)

if __name__ == "__main__":
    # args
    parser = ArgumentParser(description='colect run info from fast5 files')
    parser = f5info_args(parser)
    
    opts, unknown_args = parser.parse_known_args()
    # run
    f5info_run(opts)
