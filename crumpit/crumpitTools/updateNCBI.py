from ete3 import NCBITaxa
if __name__ == '__main__':
    ncbi = NCBITaxa()
    ncbi.update_taxonomy_database()
