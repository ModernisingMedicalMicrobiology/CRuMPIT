#!/usr/bin/env python3
import sys
import os
from argparse import ArgumentParser
import time
from threading import Thread
import tarfile
import subprocess

class batchUpload:
    def __init__(self,**kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)
        self.completeBatches=[]
        self.stop=False
        self.makeDir()
        self.getPrevious()

    def getPrevious(self):
        fols=os.listdir(self.outfold)
        self.completeBatches.extend([os.path.splitext(f)[0] for f in fols])

    def makeDir(self):
        if not os.path.exists(self.outfold):
            os.makedirs(self.outfold)

    def stopWatch(self):
        self.stop=True

    def getBatches(self):
        fols=os.listdir(self.path)
        newFols=set(fols)-set(self.completeBatches)
        remove=[]
        for fol in newFols:
             if self.checkCompleted(fol) == True:
                 self.completeBatches.append(fol)
             else:
                 remove.append(fol)
        newFols=newFols-set(remove)
        return newFols

    def rsync(self,tar):
        l=['rsync',
            '-avP',
            '-e',
            'ssh -p {0}'.format(self.port),
            tar,
            '{0}@{1}:{2}/{3}/'.format(self.user,
                                    self.servip,
                                    self.servpath,
                                    self.runName)]
        rsync=subprocess.check_output(l,universal_newlines=True)

    def uploadBatch(self,fol):
        print(fol)
        of="{0}/{1}.tar".format(self.outfold,fol)
        sd="{0}/{1}".format(self.path,fol)
        with tarfile.open(of, "w:gz") as tar:
            tar.add(sd, arcname=os.path.basename(sd))
        self.rsync(of)
        os.remove(of)
        with open(of, 'a'):
            os.utime(of, None)

    def checkCompleted(self,fol):
        f5s=os.listdir("{0}/{1}".format(self.path,fol))
        filReads = filter(lambda x: x.endswith('.fast5'), f5s)
        if len(list(filReads)) >= self.batchsize: return True
        else: return False

    def run(self):
        newFols=self.getBatches()
        for b in newFols: self.uploadBatch(b)

    def keepWatch(self):
        while self.stop is False:
            nfols=self.getBatches()
            for b in nfols: self.uploadBatch(b)
            time.sleep(6)

    def runWatch(self):
        rcv=Thread(target=self.keepWatch)
        rcv.start()
        rcv.join()

def buArgs(parser):
    parser.add_argument('-p', '--path', required=True,
                                 help='Path to point at')
    parser.add_argument('-o', '--outfold', required=False,default='backUpFol',
                                 help='output path for batch files')
    parser.add_argument('-bs', '--batchsize', required=False,default=4000,type=int,
                                 help='batch size for basecalling (number of fast5 files)')
    parser.add_argument('-w', '--watch', required=False,action='store_true',
                                 help='Option to watch output')
    parser.add_argument('-u', '--user', required=True,type=str,
                                 help='user name on remote server')
    parser.add_argument('-ip', '--servip', required=True,type=str,
                                 help='remote server ip address')
    parser.add_argument('-po', '--port', required=False,type=int,default=22,
                                 help='remote server address port (default 22)')
    parser.add_argument('-rp', '--servpath', required=True,type=str,
                                 help='remote server path for fileUpload')
    parser.add_argument('-run', '--runName', required=True,type=str,
                                 help='Name of the run')

    return parser

def buRun(opts):
    kditc=vars(opts)
    bt=batchUpload(**kditc)
    bt.run()
    if opts.watch==True:
        try:
            bt.runWatch()
        except KeyboardInterrupt:
            bt.stopWatch()

if __name__ == '__main__':
    #args
    parser = ArgumentParser(description='CRuMPIT batch uploader: batch fast5 files for upload to CRuMPIT workflow')
    parser=buArgs(parser)
    opts, unknown_args = parser.parse_known_args()
    #run
    buRun(opts)
