#!/usr/bin/env python3
import pymongo

class mdbConnect:
    '''Class for connecting to MongoDB database, can be ingerited '''
    def __init__(self,**kwargs):
        allowed=['table_name','ip','port']
        for k, v in kwargs.items():
            if k in allowed: setattr(self, k, v)
        
        self.client = None
        self.resetMongoConnection()

    def resetMongoConnection(self):
        try:
            client = pymongo.MongoClient(self.ip, self.port)
            self.db = client[self.table_name]
            self.collection = self.db.nanopore_runs
        except Exception as e:
            print("Could not connect to mongo DB: {}".format(e))
            self.db = None
            self.collection = None