#!/usr/bin/env python3
import sys
import os
from argparse import ArgumentParser, SUPPRESS
import json
import time
import datetime
from pathlib import Path
from subprocess import Popen,PIPE
import logging
from threading import Thread
import pandas as pd
from pymongo import MongoClient

from crumpitTools.config_parser import cmptConf
from crumpitTools.sqlConnect import sqlConnect

logging.basicConfig(level=logging.DEBUG, filename='checkGrid.log')


def save_obj(obj, name ):
    with open( name , 'w') as f:
        json.dump(obj,f)

class run:
    def __init__(self,**kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)
        client = MongoClient(self.ip, self.port)
        self.db = client['gridRuns']
        self.collection = self.db.gridRuns
        self.kwargs=dict(kwargs)
        self.readLog()

        c=cmptConf('/etc/crumpit.config')
        wfconfig=dict(c.config.items('crumpit'))
        self.wfconfig=wfconfig
        self.presets={}
        if self.run_options != None:
            self.getPreSets()
        else:
            self.getPreSetsSQL()

        self.checkPlates()
        self.runPlates()
        #self.updateLog()

    def getPreSets(self):
        try:
            df=pd.read_excel(self.run_options,index='run_name')
            df=df.set_index('run_name')
            df=df.loc[~df.index.duplicated(keep='last')]
            self.presets=df.to_dict('index')
        except:
            logging.exception("getPreSets:")
            self.presets={}
    
    def getPreSetsSQL(self):
        try:
            sqlConnection = sqlConnect(database='NanoporeMeta',sqlip=self.wfconfig['sqlip'],sqlport=self.wfconfig['sqlport'])
            self.presets = sqlConnection.getPreRunInfo()
        except Exception as e:
            logging.exception("getPreSets: {}".format(e))
            print("getPreSets: {}".format(e))
            self.presets={}

    def readLog(self):
        '''read log file for previously run plates'''
        try:
            #self.log=json.load(open(self.logFile,'rt'))
            hce=self.collection.find()
            self.log={}
            for h in hce:
                self.log[h['run_name']]=h
            if len(self.log) < 1:
                raise ValueError('no previous gridion runs? Possible bad connection to mongdb')
                raise Exception('Fewer than 1 log in the gridion run logs')

        except:
            logging.exception("NO logs connection")
            if self.dry_run == False:
                sys.exit()
            #self.log={}

    def checkPlates(self):
        '''check if plates have already been run'''
        dirs=os.listdir(self.dirs)
        dirs=[d for d in dirs if os.path.isdir('{0}/{1}'.format(self.dirs,d))]
        # remove other directories
        od=['GA10000',
                'GA20000',
                'GA30000',
                'GA40000',
                'GA50000',
                'intermediate',
                'queued_reads',
                'reads',
                'reports',
                '.Trash-1000',
                'basecalled']
        dirs=[d for d in dirs if d not in od]

        newPlates=[plate.replace(' ','_') for plate in dirs if plate.replace(' ','_') not in self.log]
        actualList = list()
        for plate in newPlates:
            if plate not in self.presets:
                logging.info("Found new run {}, but no preset info is available. Skipping.".format(plate))
            else:
                actualList.append(plate)

        self.newPlates = actualList

    def runPlates(self):
        '''run plates through workflow'''
        self.threads={}
        self.crumpit={}
        for plate in self.newPlates:
            self.threads[plate]=Thread(target=self.runPlate,name=plate,args=(plate,))
        for plate in self.newPlates:
            self.threads[plate].start()
        for plate in self.newPlates:
            self.threads[plate].join()

    def runPlate(self,plate):
        '''run plate through workflow'''
        self.log[plate]={'status':'filtering','starttime':datetime.datetime.now(),'run_name':plate}
        self.updateLog(plate)
        cwd='{0}/{1}'.format(self.workfol,plate)
        if not os.path.exists(cwd):
            os.makedirs(cwd)

        ### command to run ###
        # get porechop preset from xls file
        if plate in self.presets:
            logging.info("Plate {} presets - {}".format(plate, self.presets[plate]))
            porechop=self.presets[plate]['porechop']
            mapping=self.presets[plate]['mapping']
            self.basecalling=str(self.presets[plate]['basecalling'])
            flow=self.presets[plate]['flow']
            seq_kit=self.presets[plate]['seq_kit']
            bar_kit=self.presets[plate]['bar_kit']

            custom_refs = None
            if 'custom_refs' in self.presets[plate]:
                custom_refs = ""
                for ref in self.presets[plate]['custom_refs'].split():
                    custom_refs += " {}".format(os.path.join(self.wfconfig['customrefbase'], ref))

            try:
                if 'watchHours' in self.presets[plate] and self.presets[plate]['watchHours'] is not None:
                    self.watchHours = int(self.presets[plate]['watchHours'])
                    logging.info("watchHours in presets, using value {}".format(self.watchHours))
                else:
                    logging.info("watchHours not in presets or Null, using defaults {}".format(self.watchHours))
            except Exception as e:
                logging.info("watchHours not in presets or Null, using defaults {}".format(self.watchHours))
                logging.info("Exception {}".format(e))
        else:
            porechop='off'
            mapping='off'
            flow=self.wfconfig['flow']
            seq_kit=self.wfconfig['kit']
            bar_kit=None
            custom_refs=None

        crumpitcmd=['crumpit', 'call',
            '-s', plate,
            '-f5s', '{0}/{1}'.format(self.dirs,plate),
            '--barcode',
            '--watch',
            '-bc', self.basecalling,
            '-wh', str(self.watchHours),
            '--wf', self.wf,
            '--porechop',porechop,
            '--map',mapping,
            '--flow',flow,
            '--seq_kit',seq_kit,
            '--grid']
        
        if custom_refs is not None:
            crumpitcmd.append('--custom_refs')
            crumpitcmd.append(custom_refs)

        if bar_kit and bar_kit != 'None':
            crumpitcmd.append('--bar_kit')
            crumpitcmd.append('{0}'.format(bar_kit))
        
        # Add batch size for basecalling
        if self.basecalling.lower() == "true":
            crumpitcmd.append('-bs')
            crumpitcmd.append('1')

        # do dry run or not
        if self.dry_run==False:
            print(crumpitcmd)
            self.crumpit[plate]=Popen(crumpitcmd,stdout=PIPE,stderr=None,universal_newlines=True,cwd=cwd)
        else:
            self.crumpit[plate]=type('test', (object,), {})()
            self.crumpit[plate].pid='fake'
            #setattr(self.crumpit, 'wait', )

        ## Logging ###
        self.log[plate].update({'status':'Running',
                            'Submittedtime':datetime.datetime.now(),
                            'PID':self.crumpit[plate].pid,
                            'cwd':cwd})
        self.updateLog(plate)
        ## wait for crumpit - will be self.watchHours default=48 hours
        if self.dry_run==False:
            self.crumpit[plate].wait()

        ### Logging ###
        self.log[plate].update({'status':'Finishing',
                                'Finishingtime':datetime.datetime.now(),
                                'PID':self.crumpit[plate].pid})
        self.updateLog(plate)
        ### compile fastqs and generate sequencing_summary
        #self.finishRun(plate,cwd)

        ### Logging ###
        self.log[plate].update({'status':'Finished',
                                'Finishtime':datetime.datetime.now(),
                                'PID':self.crumpit[plate].pid})
        self.updateLog(plate)

    def updateLog(self,plate):
        '''update log file'''
        #save_obj(self.log,self.logFile)
        posts=self.collection
        #posts_id = posts.insert_many(self.log)

        old=self.collection.find({"run_name":plate})
        if old.count() == 1:
            print('replace_one')
            posts=self.collection
            post = posts.replace_one({"run_name":plate},self.log[plate])
            #print(post.matched_count)
        elif old.count() > 1:
            logging.exception("More than one plate with same name!:")
            #print("{0} With sample id {1}".format(old.count(),post['sample_ID']))
            #sys.exit()
        elif old.count() == 0:
            self.pushIndividualPosts(self.log[plate])
            #print("Pushed for first time")
            #sys.exit()
        else:
            pass
            #print("Nope")
            #sys.exit()

    def pushIndividualPosts(self,post):
        print('pushing one')
        posts=self.collection
        post_id = posts.insert_one(post).inserted_id


    def finishRun(self,plate,cwd):
        self.compileFQ=Popen(['crumpit', 'splitBarcode'],stdout=PIPE,stderr=None,universal_newlines=True,cwd=cwd)
        self.compileFQ.wait()

def checkGridRun(opts):
    wfconfig=vars(opts)
    # run
    r=run(**wfconfig)

def checkGridArgs(parser):
    parser.add_argument('-d', '--dirs', required=True,
                             help='Folder containing basecalled GridION directories')
    parser.add_argument('-workfol', '--workfol', required=False, default='/work/basecalling',
                            help='Folder for processing run')
    parser.add_argument('-wh','--watchHours',required=False,default=48,
                             help='Number of hours to keep watching GridION run')
    parser.add_argument('-wf', '--wf', required=True,
                             help='workflow to use')
    parser.add_argument('-bc','--basecalling',required=False, default='False',
                             help='basecalling True or False')
    parser.add_argument('-dr', '--dry_run', required=False, action='store_true',
                             help='check for plates and update log but do not run, \
                             useful for adding previous runs to log')
    parser.add_argument('-ro','--run_options',required=False,
			     help='excel spreadsheet of pre run options')
    parser.add_argument('-ip', '--ip', required=False, default='127.0.0.1',
                         help='IP address for mongoDB database')
    parser.add_argument('-p', '--port', required=False, default=27017,type=int,
                         help='port address for mongoDB database')
    return parser

if __name__ == "__main__":
    parser = ArgumentParser(description='automatically run CRuMPIT')
    parser = checkGridArgs(parser)
    opts, unknown_args = parser.parse_known_args()
    checkGridRun(opts)

