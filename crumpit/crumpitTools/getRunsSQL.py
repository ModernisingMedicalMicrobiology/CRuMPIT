import sys
import mysql.connector
import logging
import uuid
from dateutil.parser import parse
import bson.int64
from argparse import ArgumentParser
import pandas as pd
from ete3 import NCBITaxa
ncbi = NCBITaxa()

def txname(taxid):
        taxid2name = ncbi.get_taxid_translator(taxid)
        return taxid2name

class connectSQL():
    def __init__(self, ip='localhost', port=3306, user='crumpit', password='CrumpitUserP455!', database='NanoporeMeta', use_pure=True):
        self.ip = ip
        self.port = port
        self.user = user
        self.password = password
        self.database = database
        self.activeConnection = 0

        fileStream = logging.FileHandler('metaDataConnection.log')
        fileStream.setLevel(logging.DEBUG)
        consoleStream = logging.StreamHandler()
        consoleStream.setLevel(logging.ERROR)
        logging.basicConfig(level=logging.DEBUG,
            format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
            handlers=[
                fileStream,
                consoleStream]
            )

        self.resetSqlConnection()

    def resetSqlConnection(self):
        self.activeConnection = 0
        try:
            self.conn = mysql.connector.connect(user=self.user, database=self.database, host=self.ip, port=self.port, password=self.password, connection_timeout=10, use_pure=True)
            self.cursor = self.conn.cursor(dictionary=True)
            self.activeConnection = 1
        except mysql.connector.Error as err:
            logging.exception("Something went wrong connecting to the SQL DB: {}".format(err))
        return self.activeConnection

    def getRuns(self):
        if not self.activeConnection:
            self.resetSqlConnection()

        try:
            query = ("SELECT sample_name,ID, ID_text, basecalling, porechop, map, flow FROM Run")
            self.cursor.execute(query)

            runs = {}
            for row in self.cursor:
                runs[row['sample_name']] = row
            return runs

        except mysql.connector.Error as err:
            logging.exception("Could not access runs DB: {}".format(err))
            return -1

    def getBarcodeMeta(self, run):
        if not self.activeConnection:
            self.resetSqlConnection()
        
        try:
            if run != None:
                query = ("SELECT Barcode.ID_text,name,barcode FROM Barcode JOIN Run ON Run.ID = Barcode.RunID WHERE Run.sample_name =  %s;")
                self.cursor.execute(query, (run,))
            else:
                logging.exception("Need to request a specific sample RUN")
                return -1

            resultDict = {}
            for row in self.cursor:
                resultDict[row['barcode']] = row
            return resultDict

        except mysql.connector.Error as err:
            logging.exception("Could not access runs DB: {}".format(err))
            return -1

    def getBarcodeInfo(self, guid:str = None):
        if not self.activeConnection:
            self.resetSqlConnection()

        try:
            if guid != None:
                query = ("SELECT `Barcode`.ID_text AS 'sampleID', name, sample_name as 'run_name', barcode, total_bases, total_reads, unclassified_bases, unclassified_reads FROM Run JOIN `Barcode` ON `Barcode`.RunID = Run.ID WHERE `Barcode`.ID_text = %s;")
                self.cursor.execute(query, (guid,))
            else:
                logging.exception("Need to request a specific sample GUID")
                return -1

            resultDict = {}
            for row in self.cursor:
                resultDict = row
                break

            query = ("SELECT b.ID_text as BarcodeID, TaxID as taxID, k.name AS kingdom_name, cs.bases, cs.sequenceReads as sequence_reads, cs.filtered  \
                FROM `Classified Species` AS cs \
                JOIN `Kingdom` AS k  ON cs.KingdomID = k.ID \
                JOIN `Barcode` AS b ON k.BarcodeID = b.ID \
                WHERE b.ID_text = %s \
                ORDER BY cs.bases DESC;")
            self.cursor.execute(query, (guid,))

            mapped_species = []
            for row in self.cursor:
                mapped_species.append({"taxID": row['taxID'], "kingdom_name": row['kingdom_name'], "bases": row['bases'], "sequence_reads":row['sequence_reads'], "filtered":row['filtered']})
            resultDict['mapped_species'] = mapped_species
            return resultDict

        except mysql.connector.Error as err:
            logging.exception("Could not access runs DB: {}".format(err))
            return -1

    def getKingdomInfo(self, guid:str = None):
        if not self.activeConnection:
            self.resetSqlConnection()
        try:
#            if guid != None:
#                query = ("SELECT ID_text,BarcodeID_text,name,bases,sequenceReads from Kingdom where BarcodeID_text =  %s;")
#                self.cursor.execute(query, (guid,))
#            else:
#                logging.exception("Need to request a specific sample GUID")
#                return -1
#
            resultDict = {}
#            for row in self.cursor:
#                resultDict = row
#                break

            query = ("SELECT b.ID_text as BarcodeID, k.name AS kingdom_name, k.bases, k.sequenceReads  \
                from `Kingdom` AS k JOIN `Barcode` AS b ON k.BarcodeID = b.ID \
                WHERE b.ID_text = %s;")
            self.cursor.execute(query, (guid,))

            mapped_species = []
            for row in self.cursor:
                mapped_species.append({"ID_text": row['BarcodeID'], "kingdom_name": row['kingdom_name'], "bases": row['bases'], "sequence_reads":row['sequenceReads']})
            resultDict['domain'] = mapped_species
            return resultDict

        except mysql.connector.Error as err:
            logging.exception("Could not access runs DB: {}".format(err))
            return -1


def setSpecies(df):
    nameTranslater=txname(list(df['taxID'].unique()))
    df['taxID']=df['taxID'].map(int)
    df['Species name']=df['taxID'].map(nameTranslater)
    df['taxID']=df['taxID'].map(str)
    #df['Barcode number']#=df['Barcode number'].map(int)
    df['Reads']=df['sequence_reads'].map(int)
    df['Bases']=df['bases'].map(int)
    df2=df[['Run_name','Barcode number','Species name','taxID','Bases','Reads','kingdom_name']]
    df2=df2.sort_values(by=['Run_name','Barcode number','Bases'],ascending=[True, True, False])
    return df2

def setKingdom(df):
    df['Reads']=df['sequence_reads']
    df['Bases']=df['bases']
    #df['Barcode number']#=df['Barcode number'].map(int)
    df=df.sort_values(by=['Run_name','Barcode number'])
    df2=df.pivot(index=['Run_name','Barcode number','Sample name'],
            columns=['kingdom_name'],
            values=['Bases','Reads'])#.reset_index().rename_axis(None, axis=1)
    df2.reset_index(inplace=True)
    df2.columns=df2.columns.to_flat_index()
    mapper={('Run_name', ''): 'Run name',
       ('Barcode number', ''): 'Barcode number',
       ('Sample name', ''): 'Sample name',
       ('Bases', 'Bacteria'): 'Bacteria bases',
       ('Bases', 'Human'): 'Human bases', 
       ('Bases', 'Viruses'): 'Viral bases',
       ('Bases', 'unclassified'): 'Unclassified bases',
       ('Reads', 'Bacteria'):'Bacteria reads',
       ('Reads', 'Human'):'Human reads',
       ('Reads', 'Viruses'):'Viral reads',
       ('Reads', 'unclassified'): 'Unclassified reads'}
    df2.rename(columns=mapper, inplace=True)
    return df2

def run(kdict):
    g=connectSQL(ip=kdict['ip'], 
	port=kdict['port'], 
	user=kdict['user'],
	password=kdict['password'],
	database='NanoporeMeta')

    if kdict['domains']==True:
        dfs=[] 
        for sample in kdict['samples']:
           barcodes=g.getBarcodeMeta(sample)

           for barcode in barcodes:
               barcodeInfo=g.getKingdomInfo(barcodes[barcode]['ID_text'])
               df=pd.DataFrame(barcodeInfo['domain'])
               df['Run_name'] = sample
               df['Barcode number'] = barcode
               df['Sample name'] = barcodes[barcode]['name']
               dfs.append(df)
        df=pd.concat(dfs)
        df=setKingdom(df)
        df.to_csv('{}_domains.csv'.format(kdict['outPrefix']),index=False)


    if kdict['all_species']==True:
        dfs=[] 
        for sample in kdict['samples']:
           barcodes=g.getBarcodeMeta(sample)
          
           for barcode in barcodes:
               barcodeInfo=g.getBarcodeInfo(barcodes[barcode]['ID_text'])
               df=pd.DataFrame(barcodeInfo['mapped_species'])
               df['Run_name'] = sample
               df['Barcode number'] = barcode
               dfs.append(df)
    
        df=pd.concat(dfs)
        df=setSpecies(df)
        df.to_csv('{}_all_species.csv'.format(kdict['outPrefix']),index=False)


def getRunsArgs(parser):
    parser.add_argument('-ip', '--ip', required=False, default='127.0.0.1',
                         help='IP address for mysql database')
    parser.add_argument('-p', '--port', required=False, default=3306,type=int,
                         help='port address for mysql database')
    parser.add_argument('-u', '--user', required=False, default='crumpit',type=str,
                         help='user for mysql database')
    parser.add_argument('-o', '--outPrefix', required=True, type=str,
                         help='output prefix')
    parser.add_argument('-ps', '--password', required=False, default='CrumpitUserP455',type=str,
                         help='password for mysql database')
    parser.add_argument('-s', '--samples', required=False,nargs='+',
                         help='sample or list of samples')
    parser.add_argument('-d', '--domains', required=False,action='store_true',
                         help='print all runs')
    parser.add_argument('-fp', '--species', required=False,action='store_true',
                         help='view filtered species breakdown')
    parser.add_argument('-sp', '--all_species', required=False,action='store_true',
                         help='view all species breakdown')
    parser.add_argument('-m', '--map', required=False,action='store_true',
                         help='view all mapped chromosomes breakdown')

    return parser

def getRunsRun(opts):
    kdict=vars(opts)
    run(kdict)


if __name__ == "__main__":
    # args
    parser = ArgumentParser(description='get run details from crumpitdb')
    parser = getRunsArgs(parser)
    opts, unknown_args = parser.parse_known_args()
    # run
    getRunsRun(opts)


