#!/usr/bin/env python
import sys
from pymongo import MongoClient
###import gzip
from argparse import ArgumentParser
from Bio import SeqIO
import pandas as pd

class mongPusher:
    def __init__(self,sample,fastq,ip='127.0.0.1',port=27017):
            client = MongoClient(ip, port)
            self.name = sample
            self.db = client[self.name]
            self.collection = self.db.alba_summary
            self.fastq = fastq
            self.getIds()

    def getIds(self):
        self.oldIDs=[]
        with open(self.fastq,'rt') as i:
            for seq in SeqIO.parse(i,'fastq'):
                self.oldIDs.append(seq.id)

    def getData(self,sf):
        n,li,ssums=0,[],[]
        with open(sf,'rt') as f:
            for line in f:
                l=line.split('\t')
                if n==0: 
                    headers=l
                    ssums.append(line)
                else: 
                    d = {headers[i]:l[i] for i in range(len(l))}
                    try:
                        if d['read_id'] in self.oldIDs:
                            li.append(d)
                            ssums.append(line)
                    except:
                        pass
                n+=1
        return li,ssums

    def getDataPD(self,sf):
        chunksize = 10000
        reader = pd.read_csv(sf,sep='\t',chunksize=chunksize)
        dfs=[]
        for chunk in reader:
            dfN=chunk[chunk['read_id'].isin(self.oldIDs)]
            dfs.append(dfN)
        df2=pd.concat(dfs)
        li=df2.to_dict('records')
        return li,df2

    def pushPosts(self,stats,ssums):
        posts=self.collection
        posts_id = posts.insert_many(stats)
        ssums.to_csv('sequencing_summary.txt',sep='\t',index=False)
        #with open('sequencing_summary.txt', 'wt') as outf:
        #    outf.write(''.join(ssums))

def pushAlbaSum_args(parser):
    parser.add_argument('-s', '--sample_name', required=True,
                         help='Specify sample name as used in mongoDB.')
    parser.add_argument('-ip', '--ip', required=False, default='127.0.0.1',
                         help='IP address for mongoDB database')
    parser.add_argument('-p', '--port', required=False, default=27017,type=int,
                         help='port address for mongoDB database')
    parser.add_argument('-sum', '--sum_file',required=True,
                             help='albacore summary file')
    parser.add_argument('-fastq', '--fastq',required=True,
                             help='fastq file')
    return parser


def pushAlbaSum_run(opts):
    m=mongPusher(opts.sample_name,opts.fastq,ip=opts.ip,port=opts.port)
    s,l=m.getDataPD(opts.sum_file)
    m.pushPosts(s,l)

if __name__ == "__main__":
    # args
    parser = ArgumentParser(description='Sequencing summary detail pusher')
    parser = pushAlbaSum_args(parser)
    opts, unknown_args = parser.parse_known_args()
    # run
    pushAlbaSum_run(opts)
