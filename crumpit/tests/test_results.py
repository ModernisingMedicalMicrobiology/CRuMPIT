import unittest
from crumpitTools.crumpit_results import crumpitResult
import os,shutil
from pymongo import MongoClient
import subprocess

modules_dir = os.path.dirname(os.path.realpath(__file__))
data_dir = os.path.join(modules_dir, 'data')
mongodata = os.path.join(data_dir,'mongodb/dump')
db1 = os.path.join(mongodata,'354a_reduced_tar_sp')
db2 = os.path.join(mongodata,'directGC_8x_reduced_tar_sp')
basedir = os.path.join(data_dir, '/mnt/Data2/dbs/refseq/')


results1={'1351': {'bases': 26685, 'Reads': 32}, '28264': {'bases': 1819, 'Reads': 3}, '851': {'bases': 2771, 'Reads': 4}}
results2={'BC07': {'485': {'bases': 4385, 'Reads': 2}},
'BC01': {'485': {'bases': 930434, 'Reads': 552}},
'BC02': {'485': {'bases': 199082, 'Reads': 145}}}

#restore1=['mongorestore', '--gzip', '--db', 'r354a_reduced_tar_sp', db1]
restore2=['mongorestore', '--gzip', '--db', 'directGC_8x_reduced_tar_sp', db2]


class testResults(unittest.TestCase):

    def setUp(self):
        self.db1='r354a_reduced_tar_sp'
        self.sample_name1=self.db1
        self.db2='directGC_8x_reduced_tar_sp'
        self.sample_name2=self.db2

        self.mapq=50
        self.ip='127.0.0.1'
        #self.ip='mongo'
        self.port=27017
        self.cent_score=150

        client = MongoClient(self.ip, self.port)
 #       client.drop_database(self.db1)
        client.drop_database(self.db2)

 #      l=subprocess.check_output(restore1)
        l2=subprocess.check_output(restore2)

    def testResults(self):
       '''test full crumpit results script'''
       dir_path = os.path.dirname(os.path.realpath(__file__))
       cwd = os.getcwd()

       d={'sample_name':'directGC_8x_reduced_tar_sp','ip':self.ip,'port':self.port,'cent_score':150,
		'cent_bases':10,'minimum_map_bases':1,'mapq':50,'watch':False,'test':True,
                'home':dir_path,'base':cwd,'chart':'n','until':60, 'refbase':basedir}
       cr=crumpitResult(**d)
       rp=cr.run()
       print(rp['BC01']['Bacteria']['minimap2']['485'])
       assert rp['BC01']['total']=={'bases': 4154664, 'reads': 3119}
       assert rp['BC01']['Bacteria']['minimap2']['485']['bases'] == 930434
       assert rp['BC01']['Bacteria']['minimap2']['485']['Reads'] == 552
       #assert rp['BC01']['Bacteria']['minimap2']['485']['chroms']['NC_002946.2']['len'] == 2153922
       

 
    def tearDown(self):
        client = MongoClient(self.ip, self.port)
        client.drop_database(self.db1)
        client.drop_database(self.db2)




