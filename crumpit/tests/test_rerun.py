import unittest
from crumpitTools.rerun_neglected import rerun
import os,shutil


modules_dir = os.path.dirname(os.path.realpath(__file__))
data_dir = os.path.join(modules_dir, 'data')

trace='''task_id	hash	native_id	name	status	exit	submit	duration	realtime	%cpurss	vmem	rchar	wchar
17	19/976d45	199558	processFiles (porton-fliter-v-spin_GA30000_1)	COMPLETED	0	2018-06-12 13:41:10.621	3m 4s	2m 10s	12.9%	39.8 MB	258.5 MB	66.5 MB	14.2 MB
6	a3/bda2f1	199552	processFiles (porton-fliter-v-spin_GA30000_2)	COMPLETED	0	2018-06-12 13:41:10.440	3m 15s	2m 15s	16.2%	39.9 MB	258.6 MB	66.4 MB	14.2 MB
3	45/7ccd88	199555	processFiles (porton-fliter-v-spin_GA30000_3)	COMPLETED	0	2018-06-12 13:41:10.511	3m 15s	2m 16s	16.7%	39.9 MB	258.6 MB	66.6 MB	33.8 MB
19	5b/d42ab3	199568	processFiles (porton-fliter-v-spin_GA30000_4)	COMPLETED	0	2018-06-12 13:42:04.897	2m 20s	2m 2s	24.0%	39.2 MB	258 MB	65 MB	14.3 MB
11	a6/b0152e	199557	processFiles (porton-fliter-v-spin_GA30000_5)	COMPLETED	0	2018-06-12 13:41:10.584	3m 24s	2m 16s	16.5%	39.8 MB	258.6 MB	66.4 MB	14.3 MB
1	6c/dc7dc6	199550	processFiles (porton-fliter-v-spin_GA30000_6)	COMPLETED	0	2018-06-12 13:41:10.397	3m 30s	2m 16s	16.2%	39.3 MB	258.1 MB	66.3 MB	14.2 MB
8	a8/3b35a0	199551	processFiles (porton-fliter-v-spin_GA30000_7)	COMPLETED	0	2018-06-12 13:41:10.429	3m 30s	2m 18s	21.3%	39.3 MB	258.1 MB	55.7 MB	19.7 MB
14	c4/0e94d2	199553	processFiles (porton-fliter-v-spin_GA30000_8)	COMPLETED	0	2018-06-12 13:41:10.456	3m 30s	2m 18s	16.2%	40 MB	258.5 MB	66.3 MB	33.6 MB'''

def deletFiles(folder):
    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
                #elif os.path.isdir(file_path): shutil.rmtree(file_path)
        except Exception as e:
            print(e)

def makeBatches(folder,nfiles):
    for n in range(1,nfiles,1):
        outfile="{0}/{1}.batch".format(folder,n)
        with open(outfile,'wt') as outf:
            outf.write("GA10000,{0},fastqfilelocation,reads/{0},sequencing_summary_{0}.txt\n".format(n))

def makeTrace(folder):
    with open(folder,'wt') as outf:
        outf.write(trace)

class testRerun(unittest.TestCase):

    def setUp(self):
        self.bdir =  os.path.join(data_dir, 'all_batches')
        self.bdirMatch  =  os.path.join(data_dir, 'all_batchesMatch')
        self.trace = os.path.join(data_dir, 'trace.txt')
        if not os.path.exists(self.bdir):
            os.makedirs(self.bdir)
        if not os.path.exists(self.bdirMatch):
            os.makedirs(self.bdirMatch)

        deletFiles(self.bdir)
        deletFiles(self.bdirMatch)
        makeBatches(self.bdir,11)
        makeBatches(self.bdirMatch,9)
        makeTrace(self.trace)
        d={'dirs':data_dir}
        self.rr=rerun(**d)

    def testGetTrace(self):
        runBatches=set(self.rr.getTrace(self.trace))
        assert runBatches == set(['1','2','3','4','5','6','7','8'])

    def testGetAllBatches(self):
        allBatches=self.rr.getAllbatches(self.bdir)
        print(set(allBatches.keys()))
        assert set(allBatches.keys()) == set(['1','2','3','4','5','6','7','8','9','10'])
        
    def testRerun(self):
        '''when two batches are missing from the trace file'''
        runBatches=set(self.rr.getTrace(self.trace))
        allBatches=self.rr.getAllbatches(self.bdir)
        failedBatches={k:v for k,v in allBatches.items() if k not in runBatches}
        assert failedBatches == {'9': '{0}/9.batch'.format(self.bdir),'10': '{0}/10.batch'.format(self.bdir)}
        print(runBatches,allBatches,failedBatches)

    def testRerunMatch(self):
        '''when everything works and no more batches need running'''
        runBatches=set(self.rr.getTrace(self.trace))
        allBatches=self.rr.getAllbatches(self.bdirMatch)
        failedBatches={k:v for k,v in allBatches.items() if k not in runBatches}
        print(runBatches,allBatches,failedBatches)
        assert failedBatches == {}

 
    def tearDown(self):
        deletFiles(self.bdir)
        deletFiles(self.bdirMatch)




