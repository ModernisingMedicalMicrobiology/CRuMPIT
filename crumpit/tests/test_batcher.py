import unittest
from crumpitTools.batcher import batcher
import os,shutil

modules_dir = os.path.dirname(os.path.realpath(__file__))
data_dir = os.path.join(modules_dir, 'data')

def deletFiles(folder):
    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
                #elif os.path.isdir(file_path): shutil.rmtree(file_path)
        except Exception as e:
            print(e)

class testBatcher(unittest.TestCase):

    def setUp(self):
        self.bdir =  os.path.join(data_dir, 'all_batches')
        self.gridBdir = os.path.join(data_dir, 'all_grid_batches')
        if not os.path.exists(self.bdir):
            os.makedirs(self.bdir)
        if not os.path.exists(self.gridBdir):
            os.makedirs(self.gridBdir)
        deletFiles(self.bdir)
        deletFiles(self.gridBdir)

    def testBatcher_f5s(self):
        '''test batcher with fast5 files'''

        f5_fols = os.path.join(data_dir, 'f5_fols')


        self.bt=batcher(f5_fols,\
                        self.bdir,\
                        batches=0,\
                        batchsize=1000)
        self.bt.run()

        assert self.bt.batches==3

    def testBatcher_fastqs(self):
        '''test batcher with fast5 files'''

        grid = os.path.join(data_dir, 'grid')
        bdir =  self.gridBdir

        bt=batcher(grid,\
                    bdir,\
                    batches=0,\
                    batchsize=1,\
                    fileType='fastq',\
                    gridFiles=8000)
        bt.runGrid()
        print (bt.batches)
        assert bt.batches==4

    def tearDown(self):
        deletFiles(self.bdir)
        deletFiles(self.gridBdir)
