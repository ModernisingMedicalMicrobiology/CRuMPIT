import unittest
from crumpitTools.map_stats import mpStats
import os,shutil
from pymongo import MongoClient
import subprocess

modules_dir = os.path.dirname(os.path.realpath(__file__))
data_dir = os.path.join(modules_dir, 'data')
mongodata = os.path.join(data_dir,'mongodb/dump')
db1 = os.path.join(mongodata,'354a_reduced_tar_sp')
db2 = os.path.join(mongodata,'directGC_8x_reduced_tar_sp')

results1={'1351': {'bases': 26685, 'Reads': 32}, '28264': {'bases': 1819, 'Reads': 3}, '851': {'bases': 2771, 'Reads': 4}}
results2={'BC07': {'485': {'bases': 4385, 'Reads': 2}},
'BC01': {'485': {'bases': 930434, 'Reads': 552}},
'BC02': {'485': {'bases': 199082, 'Reads': 145}}}


restore1=['mongorestore', '--gzip', '--db', 'r354a_reduced_tar_sp', db1]
restore2=['mongorestore', '--gzip', '--db', 'directGC_8x_reduced_tar_sp', db2]


class testMaps(unittest.TestCase):

    def setUp(self):
        self.db1='r354a_reduced_tar_sp'
        self.sample_name1=self.db1
        self.db2='directGC_8x_reduced_tar_sp'
        self.sample_name2=self.db2

        self.mapq=50
        self.ip='127.0.0.1'
        #self.ip='mongo'
        self.port=27017
        self.cent_score=150

        client = MongoClient(self.ip, self.port)
        client.drop_database(self.db1)
        client.drop_database(self.db2)

        l=subprocess.check_output(restore1)
        l2=subprocess.check_output(restore2)

    def testMapStats(self):
        '''get mapping stats from pyMongo for run with no barcodes'''
        mp=mpStats(self.sample_name1,score=self.mapq,mdbip=self.ip,mdbport=self.port,thresh=self.cent_score)
        rds_needed=000.1
        bar='none'
        mp.bases=float(rds_needed)
        mp.barcodes=str(bar)
        mps=mp.visTimings(["1351","28264","851"],showChart=False,ut=60*60*24*2)
        print('mps: ',mps)
        assert mps==results1

    def testMapStatsBarcoded(self):
        '''get mapping stats from pyMongo for run with barcodes'''
        bars=['BC07','BC01','BC02']
        rds_needed=000.1
        res={}
        for bar in bars:
            mp=mpStats(self.sample_name2,score=self.mapq,mdbip=self.ip,mdbport=self.port,thresh=self.cent_score)
            mp.bases=float(rds_needed)
            mp.barcodes=str(bar)
            mps=mp.visTimings(["485"],showChart=False,ut=60*60*24*2)
            res[bar]=mps
            print(bar ,mps)
        assert res==results2

 
    def tearDown(self):
        client = MongoClient(self.ip, self.port)
        client.drop_database(self.db1)
        client.drop_database(self.db2)




