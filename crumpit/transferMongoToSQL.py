#!/usr/bin/env/ python3

from crumpitTools.mongdbConnect import mdbConnect
from crumpitTools.uploadCRuMPIT import pushCrums
from crumpitTools.config_parser import cmptConf
import pymongo
from progress.bar import Bar

class transferToSQL(mdbConnect):

    def __init__(self,**kwargs):
        mdbConnect.__init__(self,**kwargs)
        c=cmptConf('/etc/crumpit.config')
        wfconfig=dict(c.config.items('crumpit'))
        self.wfconfig=wfconfig

    def mainMethod(self):
        pc=pushCrums(database_name='NanoporeMeta',sqlip=self.wfconfig['sqlip'],sqlport=self.wfconfig['sqlport'])
        runs = []

        for entry in self.collection.find():
            runs.append(entry['sample_name'])
        
        grid_runs = {}
        try:
            with pymongo.MongoClient(self.ip, self.port) as client:
                grid_db = client['gridRuns']
                grid_collection = grid_db.gridRuns
                for entry in grid_collection.find():
                    grid_runs[entry['run_name']] = entry['starttime']
        except pymongo.errors.PyMongoError as e:
            print('Cannot connect to gridRuns mongo table, Aborting')
            exit(-1)

        bar = Bar('Processing', max=len(runs))
        mongoErrors = 0
        sqlErrors = 0
        while runs:
            try:
                entry = self.collection.find_one({"sample_name":runs[-1]})
                if 'run_date' not in entry and entry['sample_name'] in grid_runs:
                   entry['run_date'] = grid_runs[entry['sample_name']]
                    
                if pc.pushData(entry):
                    runs.pop()
                    bar.next()
                    mongoErrors = 0
                    sqlErrors = 0
            except pymongo.errors.PyMongoError as e:
                print('Mongo Connection Exception: {}'.format(e))
                mongoErrors += 1
                if mongoErrors < 3:
                    print('Attempting to reconnect to mongoDB')
                    self.resetMongoConnection()
                else:
                    print('Too many failed connections to mongoDB, exiting')
                    break
        
        bar.finish()

if __name__ == "__main__":
    mongoConn = transferToSQL(ip='163.1.213.19', port=27017, table_name='crumpit_summaries')
    mongoConn.mainMethod()
