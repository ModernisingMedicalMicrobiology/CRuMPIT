#!/usr/bin/env python3
from crumpitTools.mongdbConnect import mdbConnect
from crumpitTools.sqlConnect import sqlConnect
from crumpitTools.uploadCRuMPIT import pushCrums
from crumpitTools.config_parser import cmptConf
import pymongo
from progress.bar import Bar

class insertDatesToSQL(mdbConnect):
    '''Class to insert dates in existing SQL enteries with NULLs'''
    def __init__(self, **kwargs):
        mdbConnect.__init__(self,**kwargs)
        c=cmptConf('/etc/crumpit.config')
        wfconfig=dict(c.config.items('crumpit'))
        self.wfconfig=wfconfig
        self.sqlConnection = sqlConnect(sqlip=self.wfconfig['sqlip'], sqlport=self.wfconfig['sqlport'], database='NanoporeMeta')

    def mainMethod(self):
        # Get dict of SQL run names and date 
        sql_runs = self.sqlConnection.getPreRunInfo()
        
        # Get dict of mongo runs and date
        grid_runs = {}
        try:
            with pymongo.MongoClient(self.ip, self.port) as client:
                grid_db = client['gridRuns']
                grid_collection = grid_db.gridRuns
                for entry in grid_collection.find():
                    grid_runs[entry['run_name']] = entry['starttime']
        except pymongo.errors.PyMongoError as e:
            print('Cannot connect to gridRuns mongo table, Aborting')
            exit(-1)
        
        bar = Bar('Processing', max=len(sql_runs))
        # Go through SQL run
        for run, data in sql_runs.items():
            # If date == NULL
            if data['date_started'] == None:
            # If mongoDate
                if run in grid_runs:
                # update SQL
                    self.sqlConnection.updateRun({'sample_name':run, 'date_started':grid_runs[run]})

            bar.next()
        bar.finish()

if __name__ == "__main__":
    mongoConn = insertDatesToSQL(ip='163.1.213.19', port=27017, table_name='crumpit_summaries')
    mongoConn.mainMethod()
