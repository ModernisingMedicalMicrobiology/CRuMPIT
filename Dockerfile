# Use an official Python runtime as a parent image
FROM ubuntu:16.04

# Set the working directory to /app
WORKDIR /soft

# Copy the current directory contents into the container at /app
#ADD soft/ /soft

# Install any needed packages specified in requirements.txt
#RUN pip install -r requirements.txt

# Install software
RUN \
  sed -i 's/# \(.*multiverse$\)/\1/g' /etc/apt/sources.list && \
  apt-get update && \
  apt-get -y upgrade && \
  apt-get install -y build-essential


RUN apt-get -y install git

# centrifuge
RUN git clone https://github.com/infphilo/centrifuge
WORKDIR /soft/centrifuge
RUN pwd
RUN make
RUN make install prefix=/usr/local

# minimap
WORKDIR /soft
RUN git clone https://github.com/lh3/minimap2
WORKDIR /soft/minimap2
RUN apt-get install libz-dev
RUN make
RUN cp minimap2 /usr/local/bin/


# albacore
WORKDIR /soft
RUN apt-get install -y wget
RUN wget https://mirror.oxfordnanoportal.com/software/analysis/python3-ont-albacore_2.1.10-1~xenial_amd64.deb
RUN apt-get update
RUN apt-get install wget
RUN sh -c 'wget -nv -O- https://mirror.oxfordnanoportal.com/apt/ont-repo.pub | apt-key add -'
RUN echo "deb http://mirror.oxfordnanoportal.com/apt trusty-stable non-free" | tee /etc/apt/sources.list.d/nanoporetech.sources.list
RUN apt-get install apt-transport-https
RUN apt-get update
RUN dpkg -i python3-ont-albacore_2.1.10-1~xenial_amd64.deb; exit 0
RUN apt-get -y -f install
RUN apt-get install -y python3-setuptools


# prinseq
WORKDIR /soft
RUN wget https://downloads.sourceforge.net/project/prinseq/standalone/prinseq-lite-0.20.4.tar.gz
#COPY prinseq-lite-0.20.4.tar.gz /soft
RUN tar -xzf prinseq-lite-0.20.4.tar.gz

# script
WORKDIR /
COPY crumpit/lib /soft/lib/
RUN apt-get install -y python-pip python-biopython vim bwa python-dateutil python3-biopython python3-pip
RUN pip install pymongo tqdm ete3 pysam six
RUN pip3 install pymongo tqdm ete3 pysam six scipy matplotlib seaborn

# for centrifuge dbs
RUN mkdir -p /home/nick/dbs

# update ncbi taxids
RUN python3 /soft/lib/updateNCBI.py

# install forked porechop
WORKDIR /soft
RUN git clone https://github.com/nick297/Porechop.git
WORKDIR /soft/Porechop
RUN python3 setup.py install
