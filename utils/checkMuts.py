from Bio import SeqIO
import sys
import json
from argparse import ArgumentParser, SUPPRESS



saur_genes={'gyrA':
        {'chrom':'BX571857.1',
            'start':7005,
            'stop':9668,
            'strand':'pos'},
        'grlA':
        {'chrom':'BX571857.1',
            'start':1386869,
            'stop':1389271,
            'strand':'pos'},
        'grlB':
        {'chrom':'BX571857.1',
            'start':1384878,
            'stop':1386869,
            'strand':'pos'},
        'fusA':
        {'chrom':'BX571857.1',
            'start':577685,
            'stop':579766,
            'strand':'pos'},
        'rpoB':
        {'chrom':'BX571857.1',
            'start':568813,
            'stop':572364,
            'strand':'pos'},
        'dfrB':
        {'chrom':'BX571857.1',
            'start':1464014,
            'stop':1464493,
            'strand':'neg'},
        'blaZ':
        {'chrom':'BX571856.1',
            'start':1913827,
            'stop':1914672,
            'strand':'pos'},
        'mecA': 
        {'chrom':'BX571856.1',
            'start':44919,
            'stop':46925,
            'strand':'neg'},
        'msrA':
        {'chrom':'CP003194.1', 
        'start':54168,
        'stop':55634,
        'strand':'neg'},
        'ermA':
        {'chrom':'BA000018.3',
        'start':56002,
        'stop':56733,
        'strand':'pos'},
        'ermB':
        {'chrom':'AB699882.1', 
        'start':4971,
        'stop':5708,
        'strand':'neg'},
        'ermC': 
        {'chrom':'HE579068.1',
        'start':7858,
        'stop':8592,
        'strand':'pos'},
        'ermT':
        {'chrom':'HF583292.1', 
        'start':11344,
        'stop':12078,
        'strand':'neg'},
        'tetK':
        {'chrom':'FN433596.1', 
        'start':69118,
        'stop':70497,
        'strand':'pos'},
        'tetL':
        {'chrom':'HF583292.1', 
        'start':7713,
        'stop':9089,
        'strand':'pos'},
        'tetM':
        {'chrom':'CP002643.1',
        'start':427033,
        'stop':428952,
        'strand':'neg'},
        'vanA':
        {'chrom':'AE017171.1',
                'start':1,
                'stop':-1,
                'strand':'pos'},
        'fusB': 
        {'chrom':'CP003193.1', 
        'start':1336,
        'stop':1977,
        'strand':'pos'},
        'far':
        {'chrom':'AY373761.1', 
        'start':19072,
        'stop':19713,
        'strand':'neg'},
        'dfrA': 
        {'chrom':'CP002120.1',
        'start':2093303,
        'stop':2093788,
        'strand':'neg'},
        'dfrG':
        {'chrom':'FN433596.1',
        'start':502263,
        'stop':502760,
        'strand':'neg'},
        'aacA-aphD': 
        {'chrom':'FN433596.1', 
        'start':2209531,
        'stop':2210970,
        'strand':'neg'},
        'mupA': 
        {'chrom':'HE579068.1',
        'start':2154,
        'stop':5231,
        'strand':'pos'},
        'mupB':
        {'chrom':'JQ231224.1',
         'start':67,
         'stop':3192,
         'strand':'pos'}
        }

muts={'gyrA': ['S84L', 'E88K', 'G106D', 'S85P', 'E88G', 'E88L'],
        'grlA': ['S80F', 'S80Y', 'E84K', 'E84G', 'E84V', 'D432G', 'Y83N', 'A116E',
            'I45M', 'A48T', 'D79V', 'V41G', 'S108N'],
        'grlB': ['R470D', 'E422D', 'P451S', 'P585S', 'D443E', 'R444S'],
        'fusA': ['A160V', 'A376V', 'A655E', 'A655P', 'A655V', 'A67T', 
        'A70V', 'A71V', 'B434N', 'C473S', 'D189G', 'D189V', 'D373N', 
        'D463G', 'E233Q', 'E444K', 'E444V', 'E449K', 'F441Y','F652S', 
        'G451V', 'G452C', 'G452S', 'G556S', 'G617D', 'G664S', 'H438N', 
        'H457Q', 'H457Y','L430S', 'L456F', 'L461K', 'L461S', 'M161I', 
        'M453I', 'M651I', 'P114H', 'P404L', 'P404Q', 'P406L','P478S', 
        'Q115L', 'R464C', 'R464H', 'R464S', 'R659C', 'R659H', 'R659L', 
        'R659S', 'R76C', 'S416F','T385N', 'T387I', 'T436I', 'T656K', 
        'V607I', 'V90A', 'V90I', 'Y654N'],
        'rpoB': ['A473T', 'A477D', 'A477T', 'A477V', 'D471G', 'D471Y',
            'D550G', 'H481D', 'H481N', 'H481Y','I527F', 'I527L', 'I527M', 
            'ins 475H', 'ins G475', 'L466S', 'M470T', 'N474K', 'Q456K',
            'Q468K', 'Q468L', 'Q468R', 'Q565R', 'R484H', 'S463P', 'S464P', 
            'S486L', 'S529L'],
        'dfrB': ['F99Y', 'F99S', 'F99I', 'H31N', 'L41F', 'H150R', 'L21V', 'N60I']
}

combinations={'fusA':[['L461K','H457Q'],
              ['L461K','C473S'],
              ['H457Y','A67T'],
              ['H457Y','A70V'],
              ['H457Y','R76C'],
              ['L461K','H457Q','A655V','V90I'],
              ['L461F','A376V','A655P','D463G'],
              ['H457Q','L461F'],
              ['V90I','H457Q','L461K'],
              ['L461S','P404L','A71V'],
              ['L461S','V90I'],
              ['H457Y','M161I'],
              ['H457Y','S416F'],
              ['H557Y','D373N'],
              ['L456F','A376V'],
              ['L461F','E444V'],
              ['H457Y','R659L'],
              ['H457Y','G556S'],
              ['L461S','E233Q','V90I'],
              ['A67T','P406L'],
              ['A71V','P404L'],
              ['F652S','Y654N'],
              ['A71V','D189G','P406L'],
              ['A70V','A160V','H457Y'],
              ['T387I','E449K'],
              ['D189V','L430S']],
         'dfrB':[['F99Y','H31N'],
             ['F99Y','H150R'],
             ['F99Y','L21V','N60I']],
          'rpoB':[['D471Y','S486L'],
              ['H481N','A473T','A477T'],
              ['H481N','I527M'],
              ['H481N','Q565R','S529L'],
              ['H481N','L466S'],
              ['H481N','S529L'],
              ['H481N','I527M'],
              ['H481D','Q468K'],
              ['H481D','I527L'],
              ['H481D','S529L'],
              ['H481D','A477T'],
              ['M470T','D471G']],
          'grlA':[['S80F','E84V'],
             ['S80Y','E84G'],
             ['S80F','E84L'],
             ['S80Y','E84G'],
             ['S80Y','E84K'],
             ['S80Y','E84G'],
             ['S80F','S108N'],
             ['S80Y','E84G'],
             ['S80F','S108N'],
             ['S80Y','E84G'],
             ['S80Y','E84K'],
             ['S80F','E84K'],
             ['S80F','A48T'],
             ['S80F','E84K'],
             ['S80F','E84K'],
             ['S80F','E84G'],
             ['S80F','E84V'],
             ['S80F','Y83N'],
             ['S80F','E84K'],
             ['S80F','D432G'],
             ['D79V','S80Y'],
             ['S80F','V41G','I45M'],
             ['S80F','V41G'],
             ['D79V','S80F'],
             ['S80F','I45M']],
          'gyrA':[['S84L','S85P'],
                  ['S84L','S85P'], 
                  ['S84L','E88K'], 
                  ['S84L','S85P'],
                  ['S84L','S85P'],
                  ['S84L','S85P'],
                  ['S84L','E88L'],
                  ['S84L','S85P'],
                  ['S84L','E88K'],
                  ['S84L','E88K'],
                  ['S84L','E88G'],
                  ['S84L','S85P'],
                  ['S84L','S85P'],
                  ['S84L','S85P'],
                  ['S84L','G106D']],
          'grlB':[['D443E','R444S']]}

dicts={'saur_genes':saur_genes,
        'muts':muts, 
        'combinations':combinations}      

#for data in dicts:
#    with open('staph_catalog/{0}.json'.format(data), 'w') as outfile:
#        json.dump(dicts[data], outfile)

class checkMuts:
    def __init__(self,seq,refs,samplename,refname):
        self.chrom={}
        self.savemuts=[]
        self.refname=refname
        self.samplename=samplename
        self.chrom=self.loadSeq(seq)
        self.refs=self.loadSeq(refs)
        self.runGenes()
        self.writeMuts()
        #self.cutGenes()
        #self.saveGenes()
        
    def loadSeq(self,seq):
        seqs=SeqIO.parse(open(seq,'rt'),'fasta')
        chrom={}
        for seq in seqs:
            chrom[seq.id]=seq.seq
        return chrom

    def writeMuts(self):
        with open('{0}_{1}_checkMuts.csv'.format(self.samplename,self.refname),'wt') as outf:
            s='{0},{1},{2},{3},{4},{5},{6}\n'.format('sample name',
                    'gene',
                    'mut type',
                    'AA pos',
                    'WT res',
                    'res',
                    'EXP res',)
            for i in self.savemuts:
                s+='{0},{1},{2},{3},{4},{5},{6}\n'.format(self.samplename,
                        i[0],
                        i[1],
                        i[2],
                        i[3],
                        i[4],
                        i[5])
            outf.write(s)

    def runMuts(self,gene,prot):
        for mut in muts[gene]:
            if mut.split(' ')[0] == 'ins': continue
            wt=mut[0] # wild type residue
            m=mut[-1] # expected mutant residue (EXP)
            aa=int(mut[1:-1]) # AA position in gene 
            r = prot[aa-1] # sample residue
            sub = '{0}{1}{2}'.format(wt,aa,r)
            if r == m: st=[gene,'Mutation',aa, wt, r,m]
            elif r != m and r != wt: 
                if sub not in muts[gene]:
                    st=[gene,'New Mutation',aa, wt, r,m]
            elif r == wt: st=[gene,'Wild type',aa, wt,r,m]
            self.savemuts.append(st)

    def runGenes(self):
        print('gene','type','AA pos','WT','ALT','EXP',sep='\t')
        self.seqs={}
        for gene in saur_genes:
            if saur_genes[gene]['chrom'] not in self.chrom: continue
            chrom=saur_genes[gene]['chrom']
            s=self.chrom[chrom][saur_genes[gene]['start']-1:saur_genes[gene]['stop']]
            self.seqs[gene]=s
            saur_genes[gene]['seq']=s
            if saur_genes[gene]['strand'] == 'pos':
                saur_genes[gene]['prot']=s.translate()
            else:
                saur_genes[gene]['prot']=s.reverse_complement().translate()
            if gene in muts:
                self.runMuts(gene,saur_genes[gene]['prot'])

# added functions for saving the fastas
    def saveGenes(self):
        for gene in saur_genes:
            with open('staph_catalog_test/{0}.fasta'.format(gene),'wt') as outf:
                outf.write('>{0} {1}:{2}-{3}\n{4}\n'.format(gene,
                    saur_genes[gene]['chrom'],
                    saur_genes[gene]['start'],
                    saur_genes[gene]['stop'],
                    saur_genes[gene]['seq']))

            with open('staph_catalog_test/{0}_prot.fasta'.format(gene),'wt') as outf:
                outf.write('>{0} {1}:{2}-{3}\n{4}\n'.format(gene,
                    saur_genes[gene]['chrom'],
                    saur_genes[gene]['start'],
                    saur_genes[gene]['stop'],
                    saur_genes[gene]['prot']))

    def cutGenes(self):
        self.catalog={}
        for gene in saur_genes:
            chrom=saur_genes[gene]['chrom']
            s=self.refs[chrom][saur_genes[gene]['start']-1:saur_genes[gene]['stop']]
            self.seqs[gene]=s
            if saur_genes[gene]['strand'] == 'pos':
                saur_genes[gene]['prot']=s.translate()
                saur_genes[gene]['seq']=s
            else:
                saur_genes[gene]['prot']=s.reverse_complement().translate()
                saur_genes[gene]['seq']=s.reverse_complement()


def runCheckMuts(opts):
    checkMuts(opts.sample,opts.ref,opts.samplename,opts.refname)

def compareGetArgs(parser):
    parser.add_argument('-s','--sample',required=True,
                                             help='aligned conensus file name')
    parser.add_argument('-r','--ref',required=True,
                                             help='reference fasta file')
    parser.add_argument('-sn','--samplename',required=True,
                                             help='sample name')
    parser.add_argument('-rn','--refname',required=True,
                                             help='reference name')
    return parser

if __name__ == '__main__':
    parser = ArgumentParser(description='Check mutations from panel and a reference genome')
    parser = compareGetArgs(parser)

    opts, unknown_args = parser.parse_known_args()
    # run
    runCheckMuts(opts)

