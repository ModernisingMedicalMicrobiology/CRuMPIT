from argparse import ArgumentParser, SUPPRESS
import pandas as pd
from ete3 import NCBITaxa
import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import seaborn as sns; sns.set(color_codes=True)
ncbi = NCBITaxa()


def getData(csv,collapse=False):
    df=pd.read_csv(csv)
    #df['sampleName']=df['Run_name']+'_'+df['Barcode']
    if collapse==True:
        df=df.groupby(['Run_name','Taxid']).sum()
        df.reset_index(inplace=True)
    else:
        df['Run_name']=df['Run_name']+'_'+df['Barcode']
        df=df[['Run_name','Taxid','Bases','Reads']]
        df=df.groupby(['Run_name','Taxid']).sum()
        df.reset_index(inplace=True)
    
    # add names # add names # add names 
    ti=ncbi.get_name_translator(df.Taxid)
    def getTax(r):
        return ti[r][0]
    
    df['t']=df.Taxid.map(getTax)
    b=ncbi.get_descendant_taxa(2,intermediate_nodes=True)

    # filter for bacteria and reads 10 reads
    df=df[df.t.isin(b)]
    df=df[df.Reads > 10]

    # manipulate to pivot table
    df = df.pivot(index='Taxid',columns='Run_name',values='Bases')
    
    df['total']=df.sum(axis=1,numeric_only=True)
    df=df.nlargest(40, 'total')
    df=df.drop('total',axis=1)
    #df.reset_index(inplace=True)
    df.to_csv('matric.csv')
    df.fillna(value=0.0000001,inplace=True)
    return df

def plot(df,name):
    ax = sns.clustermap(df,
                    cmap="Blues",
                    metric='euclidean',
                    method='centroid',
                    z_score=1,
                    xticklabels=True,
                    figsize=(12, 12))

    plt.gcf().subplots_adjust(bottom=0.30,right=0.75,left=0,top=0.98)
    plt.savefig('{0}_clustermap_bases.pdf'.format(name))


def run(opts):
    df=getData(opts.csv,collapse=opts.collapse)
    plot(df,opts.name)


if __name__ == '__main__':
    parser = ArgumentParser(description='cluster map from crumpit csv')
    parser.add_argument('-csv', '--csv', required=True,
                        help='csv file')
    parser.add_argument('-n', '--name', required=True,
                        help='name for output')
    parser.add_argument('-c', '--collapse', required=False,action='store_true',
                        help='collapse barcodes into run and sum all')
    opts, unknown_args = parser.parse_known_args()

    run(opts)


