use NanoporeMeta;
DELETE e FROM Barcode e JOIN Run AS r ON RunID = r.ID WHERE r.sample_name = "Ecoli_1batch_220920_NK";
DELETE e FROM `Mapped Species` e JOIN Run AS r ON RunID = r.ID WHERE r.sample_name = "Ecoli_1batch_220920_NK";
DELETE e FROM Run e WHERE sample_name = "Ecoli_1batch_220920_NK";
