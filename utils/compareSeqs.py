#!/usr/bin/env python3
import numpy as np
import pandas as pd
from Bio import SeqIO
import sys
import re
from argparse import ArgumentParser, SUPPRESS
import logging
import json
import os
logging.basicConfig(filename='example.log',level=logging.DEBUG)

class compareSeqs:
    def __init__(self,seq,ref,samplename,refname):
        self.ref=ref
        self.seq=seq
        self.refname=refname
        self.samplename=samplename

    def loadSeqs(self,seq):
        d={}
        for s in SeqIO.parse(open(seq,'rt'),'fasta'):
            d[s.id]={'SR':s}
        return d


    def percentCovered(self,sq):
        N=sq.count('N')
        L=len(sq)
        PC=100-((N/float(L))*100)
        return PC

    def percentIdentity(self,seq,ref):
        same=0
        diff=0
        N=0
        l=len(seq)
        for s,r in zip(seq,ref):
            if s==r: same+=1
            elif s=='N': N+=1
            elif s!=r and s!='N': diff+=1
        pi=(float(same)/l)*100
        return {'same':same,
                'diff': diff,
                'Ns':N,
                'percentIdentity':pi}

    def SNPs(self,seq,ref):
        sp=seq.translate()
        rp=ref.translate()
        snps=[]
        AA=0
        for s,r in zip(sp,rp):
            if s=='X': 
                AA+=1
                continue
            if s!=r:
                snp='{0}{1}{2}'.format(s,AA,r)
                snps.append(snp)
            AA+=1
        return snps

    def save(self):
        with open('{0}_{1}_comapre.csv'.format(self.samplename,self.refname),'wt') as outf:
            s='{0},{1},{2},{3},{4},{5},{6},{7}\n'.format('sample name',
                        'gene',
                        'percent covered',
                        'nucleotides identical',
                        'nucleotides different',
                        'Ns',
                        'percent identity',
                        'Number of AA substitutions')
            for i in self.seqs:
                s+='{0},{1},{2},{3},{4},{5},{6},{7}\n'.format(self.samplename,
                        self.seqs[i]['SR'].id,
                        str(self.seqs[i]['PC']),
                        str(self.seqs[i]['same']),
                        str(self.seqs[i]['diff']),
                        str(self.seqs[i]['Ns']),
                        str(self.seqs[i]['percentIdentity']),
                        str(self.seqs[i]['NoSNPs']))
                outf.write(s)


    def run(self):
        self.seqs=self.loadSeqs(self.seq)
        self.refs=self.loadSeqs(self.ref)
        for seqid in self.seqs:
            seq=self.seqs[seqid]['SR'].seq
            ref=self.refs[seqid]['SR'].seq
            
            self.seqs[seqid]['PC']=self.percentCovered(seq)
            self.seqs[seqid].update(self.percentIdentity(seq,ref))
            self.seqs[seqid]['snps']=self.SNPs(seq,ref)
            self.seqs[seqid]['NoSNPs']=len(self.seqs[seqid]['snps'])
        self.save()

def runCompare(opts):
    c=compareSeqs(opts.sample,opts.ref,opts.samplename,opts.refname)
    c.run()

def compareGetArgs(parser):
    parser.add_argument('-s','--sample',required=True,
                                             help='aligned conensus file name')
    parser.add_argument('-r','--ref',required=True,
                                             help='reference fasta file')
    parser.add_argument('-sn','--samplename',required=True,
                                             help='sample name')
    parser.add_argument('-rn','--refname',required=True,
                                             help='reference name')
    return parser

if __name__ == '__main__':
    parser = ArgumentParser(description='simple comparison of two fasta files sample and ref')
    parser = compareGetArgs(parser)

    opts, unknown_args = parser.parse_known_args()
    # run
    runCompare(opts)



