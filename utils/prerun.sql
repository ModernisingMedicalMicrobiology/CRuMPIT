-- CREATE DATABASE  IF NOT EXISTS `NanoporeMeta`;
USE `NanoporeMeta`;

--
-- Test data for prerun use
--

-- INSERT INTO Run (ID, sample_name, run_date, porechop, map, basecalling, flow, seq_kit) VALUES (unhex(replace('b2040455-2be1-414e-bcc4-8c7f77a8437b','-','')), 'brctb_SISPA_products_1', '2020-01-15', 'off', 0, 0, 'FLO-MIN107', 'SQK-LSK109');

-- INSERT INTO Run (ID, sample_name, run_date, porechop, map, basecalling, flow, seq_kit) VALUES (unhex(replace('d72df8b6-89ac-4a09-9432-1e40e10c87c5','-','')), 'Hayleah_TimD_GramNeg_1', '2019-11-11', 'guppy', 0,1, 'FLO-MIN107', 'SQK-LSK109');
-- INSERT INTO Run (ID, sample_name, run_date, porechop, map, basecalling, flow, seq_kit) VALUES (unhex(replace('ac8235a1-b72f-4143-b6fa-79dcd6a0bd90','-','')), 'F47676_run1', '2019-11-13', 'guppy', 0, 0, 'FLO-MIN107', 'SQK-LSK109');
-- INSERT INTO Run (ID, sample_name, run_date, porechop, map, basecalling, flow, seq_kit) VALUES (unhex(replace('b171b6e9-59dd-4308-91e2-2cef49a10684','-','')), 'Slip_01', '2019-11-13', 'guppy', 0, 1, 'FLO-MIN107', 'SQK-LSK109');
-- INSERT INTO Run (ID, sample_name, run_date, porechop, map, basecalling, flow, seq_kit) VALUES (unhex(replace('963f627e-bdca-42a4-98a7-92ec8d0c44e3','-','')), 'brctb_AMP_PCR_test6', '2020-01-15', 'off', 0, 0, 'FLO-MIN107', 'SQK-LSK109');
-- INSERT INTO Run (ID, sample_name, run_date, porechop, map, basecalling, flow, seq_kit) VALUES (unhex(replace('94456e4c-1649-4cae-b4c5-eba1d287d973','-','')), 'RHB_APHA_POUL_run6', '2020-01-15', 'normal', 0, 0, 'FLO-MIN107', 'SQK-LSK109');
-- INSERT INTO Run (ID, sample_name, run_date, porechop, map, basecalling, flow, seq_kit) VALUES (unhex(replace('510cc71b-19a8-437e-983c-26b03835297d','-','')), 'brctb_clinical_direct_L99052', '2019-12-06', 'off', 0, 0, 'FLO-MIN107', 'SQK-LSK109');
-- INSERT INTO Run (ID, sample_name, run_date, porechop, map, basecalling, flow, seq_kit) VALUES (unhex(replace('b2040455-2be1-414e-bcc4-8c7f77a8437b','-','')), 'brctb_SISPA_products_1_ORIG', '2020-01-15', 'off', 0, 0, 'FLO-MIN107', 'SQK-LSK109');

-- INSERT INTO Run (ID, sample_name, run_date, porechop, map, basecalling, flow, seq_kit) VALUES (unhex(replace('b2040455-2be1-414e-bcc4-8c7f77a8437b','-','')), 'brctb_SISPA_products_1', '2020-01-15', 'off', 0, 0, 'FLO-MIN107', 'SQK-LSK109');
-- INSERT INTO Run (ID, sample_name, run_date, porechop, map, basecalling, flow, seq_kit) VALUES (unhex(replace('510cc71b-19a8-437e-983c-26b03835297d','-','')), 'brctb_clinical_direct_L99052_SMALL', '2019-12-06', 'off', 0, 0, 'FLO-MIN107', 'SQK-LSK109');

-- INSERT INTO Run (ID, sample_name, run_date, porechop, map, basecalling, flow, seq_kit) VALUES (unhex(replace('ac8235a1-b72f-4143-b6fa-79dcd6a0bd90','-','')), 'F47676_run1', '2019-11-13', 'normal', 0, 0, 'FLO-MIN107', 'SQK-LSK109');

-- INSERT INTO Run (ID, sample_name, run_date, porechop, map, basecalling, flow, seq_kit) VALUES (unhex(replace('b2040455-2be1-414e-bcc4-8c7f77a8437b','-','')), 'brctb_SISPA_products_1', '2020-01-15', 'off', 0, 0, 'FLO-MIN107', 'SQK-LSK109');
-- INSERT INTO Barcode (ID, RunID, name, barcode) VALUES (unhex(replace('7d754785-fd4e-48fa-9674-dde1ff0cd2f6','-','')), unhex(replace('b2040455-2be1-414e-bcc4-8c7f77a8437b','-','')), 'Sample One', 2);
-- INSERT INTO Barcode (ID, RunID, name, barcode) VALUES (unhex(replace('4765a980-6afa-4f22-8b69-99b0d0fcd603','-','')), unhex(replace('b2040455-2be1-414e-bcc4-8c7f77a8437b','-','')), 'Sample Two', 3);

-- INSERT INTO Run (ID, sample_name, run_date, porechop, map, basecalling, flow, seq_kit) VALUES (unhex(replace('510cc71b-19a8-437e-983c-26b03835297d','-','')), 'brctb_clinical_direct_L99052_SMALL', '2019-12-06', 'off', 0, 0, 'FLO-MIN107', 'SQK-LSK109');
-- INSERT INTO Barcode (ID, RunID, name, barcode) VALUES (unhex(replace('5a1465b3-57a3-4770-bf4b-3f22c9130ece','-','')), unhex(replace('510cc71b-19a8-437e-983c-26b03835297d','-','')), 'Sample not multiplexed', 'nomultiplex');

INSERT INTO Run (ID, sample_name, run_date, porechop, map, basecalling, flow, seq_kit) VALUES (unhex(replace('1d4cc9e9-9fbc-4ea7-bd1c-1f1a05a1f67f','-','')), 'RPB004_aorticgraft', '2020-02-05', 'off', 0, 0, 'FLO-MIN107D', 'SQK-LSK109');
INSERT INTO Barcode (ID, RunID, name, barcode) VALUES (unhex(replace('e8d8e4ee-ed2b-40a1-9049-04de9f0457d4','-','')), unhex(replace('1d4cc9e9-9fbc-4ea7-bd1c-1f1a05a1f67f','-','')), 'Sample One', 1);
INSERT INTO Barcode (ID, RunID, name, barcode) VALUES (unhex(replace('1fac8899-0bdf-462a-9515-7cb78caa3554','-','')), unhex(replace('1d4cc9e9-9fbc-4ea7-bd1c-1f1a05a1f67f','-','')), 'Sample Two', 2);
INSERT INTO Barcode (ID, RunID, name, barcode) VALUES (unhex(replace('272e445e-eea3-4544-8458-8b329223ec52','-','')), unhex(replace('1d4cc9e9-9fbc-4ea7-bd1c-1f1a05a1f67f','-','')), 'Sample Three', 3);
INSERT INTO Barcode (ID, RunID, name, barcode) VALUES (unhex(replace('138bf859-9309-497b-b168-b31123e9bf1c','-','')), unhex(replace('1d4cc9e9-9fbc-4ea7-bd1c-1f1a05a1f67f','-','')), 'Sample Four', 4);
INSERT INTO Barcode (ID, RunID, name, barcode) VALUES (unhex(replace('11013252-e5ea-4427-ae63-cbc6c43e4214','-','')), unhex(replace('1d4cc9e9-9fbc-4ea7-bd1c-1f1a05a1f67f','-','')), 'Sample Five', 5);
INSERT INTO Barcode (ID, RunID, name, barcode) VALUES (unhex(replace('2c1de154-5767-4a47-8e98-55badcea1d1d','-','')), unhex(replace('1d4cc9e9-9fbc-4ea7-bd1c-1f1a05a1f67f','-','')), 'Sample Six', 6);
INSERT INTO Barcode (ID, RunID, name, barcode) VALUES (unhex(replace('aaa13609-ac40-4e18-96e8-2c4ed410fbb0','-','')), unhex(replace('1d4cc9e9-9fbc-4ea7-bd1c-1f1a05a1f67f','-','')), 'Sample Seven', 7);
INSERT INTO Barcode (ID, RunID, name, barcode) VALUES (unhex(replace('91072297-f03b-4a95-bb77-4bc26af5110e','-','')), unhex(replace('1d4cc9e9-9fbc-4ea7-bd1c-1f1a05a1f67f','-','')), 'Sample Eight', 8);
INSERT INTO Barcode (ID, RunID, name, barcode) VALUES (unhex(replace('24c6da4f-a1f8-4b48-872c-7b549e9e3c0d','-','')), unhex(replace('1d4cc9e9-9fbc-4ea7-bd1c-1f1a05a1f67f','-','')), 'Sample Nine', 9);
INSERT INTO Barcode (ID, RunID, name, barcode) VALUES (unhex(replace('93597c80-eeb9-43ed-b758-be1186e53f5e','-','')), unhex(replace('1d4cc9e9-9fbc-4ea7-bd1c-1f1a05a1f67f','-','')), 'Sample Ten', 10);
INSERT INTO Barcode (ID, RunID, name, barcode) VALUES (unhex(replace('5db0a3ff-ff84-485d-b3dd-10506daff382','-','')), unhex(replace('1d4cc9e9-9fbc-4ea7-bd1c-1f1a05a1f67f','-','')), 'Sample Eleven', 11);
INSERT INTO Barcode (ID, RunID, name, barcode) VALUES (unhex(replace('cb82f1a1-2e0c-4e81-bb1f-4b67c638351c','-','')), unhex(replace('1d4cc9e9-9fbc-4ea7-bd1c-1f1a05a1f67f','-','')), 'Sample Twelve', 12);