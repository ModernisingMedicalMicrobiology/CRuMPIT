USE NanoporeMeta;

DELETE e FROM `Classified Species` e 
    JOIN Kingdom AS k ON KingdomID = k.ID
    JOIN Barcode AS b ON k.BarcodeID = b.ID
    JOIN Run AS r ON b.RunID = r.ID 
    WHERE r.sample_name = "Ngon_SureSelect_364UB";

DELETE e FROM Kingdom e
    JOIN Barcode AS b ON BarcodeID = b.ID
    JOIN Run AS r ON b.RunID = r.ID 
    WHERE r.sample_name = "Ngon_SureSelect_364UB";

DELETE e FROM `Depth Stats` e
    JOIN Barcode AS b ON BarcodeID = b.ID
    JOIN Run AS r ON RunID = r.ID
    WHERE r.sample_name = "Ngon_SureSelect_364UB";

DELETE e FROM Barcode e 
    JOIN Run AS r ON RunID = r.ID 
    WHERE r.sample_name = "Ngon_SureSelect_364UB";

DELETE e FROM `Mapped Species` e 
    JOIN Run AS r ON RunID = r.ID
    WHERE r.sample_name = "Ngon_SureSelect_364UB";
