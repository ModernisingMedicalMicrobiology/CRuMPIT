-- CREATE DATABASE  IF NOT EXISTS `NanoporeMeta`;
USE `NanoporeMeta`;

--
-- Table structure for table `Run`
--

DROP TABLE IF EXISTS `Depth Stats`;
DROP TABLE IF EXISTS `Classified Species`;
DROP TABLE IF EXISTS `Kingdom`;
DROP TABLE IF EXISTS `Barcode`;
DROP TABLE IF EXISTS `Mapped Species`;
DROP TABLE IF EXISTS `Run`;

CREATE TABLE `Run` (
  `ID` binary(16) NOT NULL,
  `ID_text` varchar(36) generated always as
    (insert(
        insert(
          insert(
            insert(hex(ID),9,0,'-'),
            14,0,'-'),
          19,0,'-'),
        24,0,'-')
    ) virtual,
  `sample_name` varchar(45) DEFAULT NULL,
  `run_date` date DEFAULT NULL,
  `basecalling` tinyint(1) DEFAULT NULL,
  `watch_hours` int(11) DEFAULT NULL,
  `seq_kit` varchar(45) DEFAULT NULL,
  `bar_kit` varchar(45) DEFAULT NULL,
  `flow` varchar(45) DEFAULT NULL,
  `porechop` varchar(45) DEFAULT NULL,
  `map` varchar(45) DEFAULT NULL,
  `cent_bases` int(11) DEFAULT NULL,
  `cent_score` int(11) DEFAULT NULL,
  `batchsize` int(11) DEFAULT NULL,
  `mapq` int(11) DEFAULT NULL,
  `minimum_map_bases` int(11) DEFAULT NULL,
  `grid_files` int(11) DEFAULT NULL,
  `insfx` varchar(45) DEFAULT NULL,
  `subparser` varchar(45) DEFAULT NULL,
  `ref_base_dir` varchar(255) DEFAULT NULL,
  `workflow` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `fast5s_dir` varchar(255) DEFAULT NULL,
  `crumpit_dir` varchar(255) DEFAULT NULL,
  `base_dir` varchar(255) DEFAULT NULL,
  `centrifuge_db_dir` varchar(255) DEFAULT NULL,
  `git_id` varchar(36) DEFAULT NULL,
  `chart` tinyint(1) DEFAULT NULL,
  `batch_numbers` int(11) DEFAULT NULL,
  `experiment_type` varchar(45) DEFAULT NULL,
  `flowcell_type` varchar(45) DEFAULT NULL,
  `local_bc_temp_model` varchar(45) DEFAULT NULL,
  `sequencing_kit` varchar(45) DEFAULT NULL,
  `device_type` varchar(45) DEFAULT NULL,
  `flow_cell_id` varchar(45) DEFAULT NULL,
  `device_id` varchar(45) DEFAULT NULL,
  `protocols_version` varchar(45) DEFAULT NULL,
  `hostname` varchar(45) DEFAULT NULL,
  `wash_number` int(3) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `Barcode`
--

CREATE TABLE `Barcode` (
  `ID` binary(16) NOT NULL,
  `ID_text` varchar(36) generated always as
    (insert(
        insert(
          insert(
            insert(hex(ID),9,0,'-'),
            14,0,'-'),
          19,0,'-'),
        24,0,'-')
    ) virtual,
  `RunID` binary(16) NOT NULL,
  `RunID_text` varchar(36) generated always as
    (insert(
        insert(
          insert(
            insert(hex(RunID),9,0,'-'),
            14,0,'-'),
          19,0,'-'),
        24,0,'-')
    ) virtual,
  `name` varchar(45) DEFAULT NULL,
  `barcode` varchar(20) DEFAULT NULL,
  `total_bases` bigint(11) UNSIGNED DEFAULT NULL,
  `total_reads` int(11) DEFAULT NULL,
  `unclassified_bases` bigint(11) UNSIGNED DEFAULT NULL,
  `unclassified_reads` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `runID_idx` (`RunID`),
  CONSTRAINT `fk_Barcode_1` FOREIGN KEY (`RunID`) REFERENCES `Run` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `Kingdom`
--

CREATE TABLE `Kingdom` (
  `ID` binary(16) NOT NULL,
  `ID_text` varchar(36) generated always as
    (insert(
        insert(
          insert(
            insert(hex(ID),9,0,'-'),
            14,0,'-'),
          19,0,'-'),
        24,0,'-')
    ) virtual,
  `BarcodeID` binary(16) DEFAULT NULL,
  `BarcodeID_text` varchar(36) generated always as
    (insert(
        insert(
          insert(
            insert(hex(BarcodeID),9,0,'-'),
            14,0,'-'),
          19,0,'-'),
        24,0,'-')
    ) virtual,
  `name` varchar(45) DEFAULT NULL,
  `bases` bigint(11) UNSIGNED DEFAULT NULL,
  `sequenceReads` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Barcode_ID_idx` (`BarcodeID`),
  CONSTRAINT `fk_Kingdom_1` FOREIGN KEY (`BarcodeID`) REFERENCES `Barcode` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `Classified Species`
--

CREATE TABLE `Classified Species` (
  `ID` binary(16) NOT NULL,
  `ID_text` varchar(36) generated always as
    (insert(
        insert(
          insert(
            insert(hex(ID),9,0,'-'),
            14,0,'-'),
          19,0,'-'),
        24,0,'-')
    ) virtual,
  `KingdomID` binary(16) DEFAULT NULL,
  `KingdomID_text` varchar(36) generated always as
    (insert(
        insert(
          insert(
            insert(hex(KingdomID),9,0,'-'),
            14,0,'-'),
          19,0,'-'),
        24,0,'-')
    ) virtual,
  `TaxID` varchar(45) DEFAULT NULL,
  `bases` bigint(11) UNSIGNED DEFAULT NULL,
  `sequenceReads` int(11) DEFAULT NULL,
  `filtered` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `KingdomID_idx` (`KingdomID`),
  CONSTRAINT `fk_Classified_1` FOREIGN KEY (`KingdomID`) REFERENCES `Kingdom` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `Mapped Species`
--

CREATE TABLE `Mapped Species` (
  `ID` binary(16) NOT NULL,
  `ID_text` varchar(36) generated always as
    (insert(
        insert(
          insert(
            insert(hex(ID),9,0,'-'),
            14,0,'-'),
          19,0,'-'),
        24,0,'-')
    ) virtual,
  `RunID` binary(16) NOT NULL,
  `RunID_text` varchar(36) generated always as
    (insert(
        insert(
          insert(
            insert(hex(RunID),9,0,'-'),
            14,0,'-'),
          19,0,'-'),
        24,0,'-')
    ) virtual,
  `TaxID` varchar(45) DEFAULT NULL,
  `Reference_Path` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `RunID_idx` (`RunID`),
  CONSTRAINT `fk_Mapped_1` FOREIGN KEY (`RunID`) REFERENCES `Run` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `Depth Stats`
--

CREATE TABLE `Depth Stats` (
  `ID` binary(16) NOT NULL,
  `ID_text` varchar(36) generated always as
    (insert(
        insert(
          insert(
            insert(hex(ID),9,0,'-'),
            14,0,'-'),
          19,0,'-'),
        24,0,'-')
    ) virtual,
  `BarcodeID` binary(16) DEFAULT NULL,
  `BarcodeID_text` varchar(36) generated always as
    (insert(
        insert(
          insert(
            insert(hex(BarcodeID),9,0,'-'),
            14,0,'-'),
          19,0,'-'),
        24,0,'-')
    ) virtual,
  `taxid` varchar(45) DEFAULT NULL,
  `chrom` varchar(45) DEFAULT NULL,
  `cov_avg` decimal(10,7) DEFAULT NULL,
  `cov_stdv` decimal(10,7) DEFAULT NULL,
  `len` int(11) UNSIGNED DEFAULT NULL,
  `x1` int(11) UNSIGNED DEFAULT NULL,
  `x5` int(11) UNSIGNED DEFAULT NULL,
  `x10` int(11) UNSIGNED DEFAULT NULL,
  `x20` int(11) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Barcode_ID_idx` (`BarcodeID`),
  CONSTRAINT `fk_Depth_1` FOREIGN KEY (`BarcodeID`) REFERENCES `Barcode` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;