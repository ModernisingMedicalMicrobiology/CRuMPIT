import sys
from pymongo import MongoClient
from argparse import ArgumentParser
import pandas as pd
from crumpitTools.sqlConnect import sqlConnect
from ete3 import NCBITaxa
ncbi = NCBITaxa()

def txname(taxids):
        taxid2name = ncbi.get_taxid_translator(taxids)
        return taxid2name


class runTable:
    def __init__(self,sample,sqlip,sqlport,mapout):
        self.sample=sample
        self.sqlip=sqlip
        self.sqlport=sqlport
        self.sqlConnection = sqlConnect(sqlip=sqlip, sqlport=sqlport, database='NanoporeMeta')
        self.mapout=mapout

    def getMaps(self):
        # get pointer from mongodb and make pandas df
        ds = self.sqlConnection.getDepthStats(sample_name=self.sample)
        df = pd.DataFrame(list(ds.values()))
        # get list of taxids for taxid -> species name dict and add species name column
        taxids=list(df.taxid.unique())
        cleaned_taxids = []
        missed_taxids = []
        for taxid in taxids:
            try:
                cleaned_taxids.append(int(taxid))
            except ValueError:
                missed_taxids.append(taxid)
        
        taxid2name=txname(cleaned_taxids)
        strtaxids = { str(taxid) : species for (taxid, species) in taxid2name.items()}
        for extra in missed_taxids:
            strtaxids[extra] = extra
        df['taxid']=df.taxid.map(str)
        df['Species name']=df.taxid.map(strtaxids)
        # reduce to columns of interest and sort then save to csv
        df=df[['ID_text','barcode','Species name','chrom','len','cov_avg','x1','x5','x10','x20']]
        df=df.sort_values(by=['ID_text','barcode','Species name'])
        df.to_csv(self.mapout,index=False)



def getTableArgs(parser):
    parser.add_argument('-s', '--sample_name', required=True,
                         help='name of run or sample in mongodb')
    parser.add_argument('-sqlip', '--sqlip', required=False, default='127.0.0.1',
                         help='IP address for mongoDB database')
    parser.add_argument('-sqlp', '--sqlport', required=False, default=3306,type=int,
                         help='port address for mongoDB database')
    parser.add_argument('-o', '--map_out_csv', required=False, default='map.csv',
                         help='Output csv file for mapping stats')
    return parser


def runTables(opts):
    t=runTable(opts.sample_name,
            opts.sqlip,
            opts.sqlport,
            opts.map_out_csv)
    t.getMaps()

if __name__ == "__main__":
    # args
    parser = ArgumentParser(description='get details from crumpitdb make tables')
    parser = getTableArgs(parser)
    opts, unknown_args = parser.parse_known_args()
    # run
    runTables(opts)
