import pandas as pd
from pymongo import MongoClient
import sys

run=str(sys.argv[1])

client = MongoClient(sys.argv[2], 27017)
db = client['gridRuns']
collection = db.gridRuns
myquery = { "run_name": run }
collection.delete_one(myquery)

client.drop_database(run)
