bam=$1
ref=$( samtools view -H $bam | grep 'map-ont' | awk '{print $9}' | sort | uniq )
ref=${ref%".mmi"}
cp $bam ./
bam=$(basename -- $bam)
samtools index $bam
tablet $bam $ref
