#!/bin/bash

base=$1
sample=$2
refbase=$3
sqlip=$4
mo=$5
rf=$6
bam=$7

echo ${bam} ${base}

echo 'variables:'
bamname="$(basename $bam .bam)"
parentdir="$(dirname "$bam")"
bar="$(basename $parentdir)"

if [ $bar == '.' ]; then
        bar=nomultiplex
fi

taxid=(${bamname//_/ })
echo $bar ${bamname} ${taxid}

if [ ! -f ${base}/merged/$bar/$taxid.sorted.bam ]; then
    echo 'file not exits, copying from sorted'
    cp $bam ${base}/merged/$bar/$taxid.sorted.bam
else
    echo 'file exists, merging'
    samtools merge ${base}/merged/$bar/$taxid.merged.bam $bam ${base}/merged/$bar/$taxid.sorted.bam
    rm ${base}/merged/$bar/$taxid.sorted.bam
    mv ${base}/merged/$bar/$taxid.merged.bam ${base}/merged/$bar/$taxid.sorted.bam
fi

echo 'mpileup'
#samtools mpileup ${base}/merged/$bar/$taxid.sorted.bam > ${base}/mpileup/$bar/$taxid.mpileup
samtools depth ${base}/merged/$bar/$taxid.sorted.bam > ${base}/mpileup/$bar/$taxid.depth
# crumpit depthStats 
crumpit depthStats \
	-s $sample \
	-b $bar \
	-pf ${base}/mpileup/$bar/$taxid.depth \
	-t $taxid \
	-r $refbase \
	-sqlip $sqlip \
	-mo $mo \
	-rf $rf


