
Channel
    .fromPath( "${params.ref}/*.fasta" )
    .map { file -> tuple(file.baseName, file) }
    .view()
    .set{ refinputs }

Channel
    .fromPath( "${params.infastq}/*" )
    .map { file -> tuple(file.baseName, file) }
    .view()
    .set{ fastqinputs }

 
process map {
	tag { sample + r }

        publishDir 'bams', mode: 'link', overwrite: true, pattern: '*.sorted.bam'

	input:
	set val(r), file(ref),val(fqname), file(fastq) from refinputs.combine( fastqinputs )

	output:
        set val(sample), val(r), file(ref),file("${sample}.sorted.bam") into aligned

	script:
	sample=params.sample + fqname +r 
	"""
	minimap2 -t ${task.cpus} -ax map-ont $ref $fastq |\
	samtools view -F4 -bS - | samtools sort -o ${sample}.sorted.bam 
	"""

}

process pileup {
	tag { sample }

	input:
	//file ref from refinputs
	//file bam from baminputs
        set val(sample), val(r), file(ref), file("${sample}.sorted.bam") from aligned

	output:
	set val(sample), val(r), file(ref), file('mpileup') into bcf_out 

	script:
	"""
	samtools mpileup -aaB -Q2 ${sample}.sorted.bam  > mpileup 
	"""
} 

process consensus {
	tag { sample }

	publishDir 'consensusSeqs', mode: 'copy', overwrite: true

	input:
	set val(sample), val(r), file(ref), file('mpileup') from bcf_out

	output:
	set val(sample), val(r), file(ref), file("${sample}.fasta") into cons_out,cons_out1

	script:
	"""
        python3 ~/soft/branches/CRuMPIT_develop/utils/callMpileup.py \
		-r $ref \
		-m mpileup \
		-d 4 \
		-F 60 \
		-f ${sample}.fasta
	"""
}


process compareGene {
        tag { sample }

        publishDir 'compared', mode: 'copy', overwrite: true

        input:
        set val(sample), val(r), file(ref), file("${sample}.fasta") from cons_out

        output:
        file("*.csv") into comparedGenes

        script:
        """

        python3 ~/soft/branches/CRuMPIT_develop/utils/compareSeqs.py \
            -s "${sample}.fasta" \
            -r $ref \
            -sn ${sample} \
            -rn ${r}

        """
}

process checkMuts {
        tag { sample }

        publishDir 'checkMuts', mode: 'copy', overwrite: true

        input:
        set val(sample), val(r), file(ref), file("${sample}.fasta") from cons_out1

        output:
        file("*.csv") into checkMuts 

        script:
        """

        python3 ~/soft/branches/CRuMPIT_develop/utils/checkMuts.py \
            -s "${sample}.fasta" \
            -r $ref \
            -sn ${sample} \
            -rn ${r}

        """
}
//process blastn {
//	tag { sample }
//
//	publishDir 'blasts', mode: 'copy', overwrite: true
//	
//	input:
//	set val(sample), file('fasta') from cons_out
//
//	output:
//	set val(sample), file("*.O6.blast.txt"),file("*.aln.blast.txt") into blastOut
//
//	script:
//        db=params.blastdb
//	"""
//	blastn -query fasta -db $db -outfmt 6 -max_target_seqs 10000 > ${sample}.O6.blast.txt
//	blastn -query fasta -db $db -max_target_seqs 10000 > ${sample}.aln.blast.txt
//	"""
//}
//
//process parseBlast6 {
//	tag { sample }
//
//	publishDir 'ngstar', mode: 'copy', overwrite: true
//	
//	input:
//	set val(sample), file('O6.blast.txt'),file('aln.blast.txt') from blastOut
//
//	output:
//	set val(sample), file("${sample}.csv") into pb6_out
//
//	script:
//	meta=params.ngstarMeta
//	"""
//	python3 /home/nick/Dropbox/MMM/ana/Ngon/consensus/pb6.py -b O6.blast.txt -m $meta -o ${sample}.csv
//	"""
//}
