
Channel
        .fromPath( "${params.ref}" )
        .map{ file -> tuple(file.baseName, file) }
        .view()
        .into{ refinputs; refinputs2 }

Channel
        .fromPath( "${params.infastq}/*" )
        .map{ file -> tuple(file.baseName, file) }
        .view()
        .into{ fastqinputs; fastqinputs2 }


process map {
        tag { sample }

        input:
        set val(refName),file('ref'),val(sample),file(fastq) from refinputs.combine(fastqinputs)

	output:
        set val(sample), file("${sample}.sorted.bam") into aligned

	script:
	"""
	minimap2 -ax map-ont $ref $fastq |\
	samtools view -bS - | samtools sort -o ${sample}.sorted.bam 
	"""

}

process pileup {
	tag { sample }
        publishDir 'mpileup', mode: 'copy', overwrite: true

	input:
        set val(sample), file("${sample}.sorted.bam") from aligned

	output:
	set val(sample), file("${sample}.mpileup") into bcf_out 

	script:
	"""
	samtools mpileup -aaB -Q2 ${sample}.sorted.bam  > ${sample}.mpileup 
	"""
} 

process consensus {
	tag { sample }

	publishDir 'consensusSeqs', mode: 'copy', overwrite: true

	input:
        file ref from refinputs
	set val(sample), file('mpileup') from bcf_out

	output:
	set val(sample), file("${sample}.fasta") into cons_out,cons_out1

	script:
	"""
        python3 ~/soft/branches/CRuMPIT_develop/utils/callMpileup.py \
		-r $ref \
		-m mpileup \
		-d 4 \
		-F 60 \
		-f ${sample}.fasta
	"""
}

process blastn {
	tag { sample }

	publishDir 'blasts', mode: 'copy', overwrite: true
	
	input:
	set val(sample), file('fasta') from cons_out

	output:
	set val(sample), file("*.O6.blast.txt"),file("*.aln.blast.txt") into blastOut

	script:
        db=params.blastdb
	"""
	blastn -query fasta -db $db -outfmt 6 -max_target_seqs 10000 > ${sample}.O6.blast.txt
	blastn -query fasta -db $db -max_target_seqs 10000 > ${sample}.aln.blast.txt
	"""
}

process parseBlast6 {
	tag { sample }

	publishDir 'ngstar', mode: 'copy', overwrite: true
	
	input:
	set val(sample), file('O6.blast.txt'),file('aln.blast.txt') from blastOut

	output:
	set val(sample), file("${sample}.csv") into pb6_out

	script:
	meta=params.ngstarMeta
	"""
	python3 /home/nick/Dropbox/MMM/ana/Ngon/consensus/pb6.py -b O6.blast.txt -m $meta -o ${sample}.csv
	"""
}
