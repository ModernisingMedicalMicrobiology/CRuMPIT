tarInputs = file(params.tarInputs)

params.map=false
params.mvf=0.7

Channel
	.fromPath( "${params.ref}" )
        .map{ file -> tuple(file.baseName, file) }
	.view()
	.into{ refinputs; refinputs2 } 

Channel
	.fromPath( "${params.infastq}/*" )
        .map{ file -> tuple(file.baseName, file) }
	.view()
	.into{ fastqinputs; fastqinputs2 }


process map {
	tag { sample }

	input:
	set val(refName),file('ref'),val(sample),file(fastq) from refinputs.combine(fastqinputs)

	output:
        set val(sample),file('ref'), file(fastq),file("${sample}.sorted.bam") into aligned,aligned2

	script:
	"""
	minimap2 -ax map-ont 'ref' $fastq |\
	samtools view -q 50 -bS - | samtools sort -o ${sample}.sorted.bam 
	"""

}

process samtools {
     tag { sample }
     publishDir 'remaps',  mode: 'copy', overwrite: true
     input:
     set val(sample),file('ref'),file('nano'),file("${sample}.sorted.bam") from aligned
     output:
     set val(sample),file( "${sample}.stats.txt" ),file( "*.png" ),file("${sample}.sorted.bam"),file('ref') into samtooled, samtooled2,samtooled3
     script:
     """
     samtools index ${sample}.sorted.bam
     python3 /home/nick/dtc/plotBam.py ${sample}.sorted.bam > ${sample}.stats.txt
     """
}


process pileup {
	tag { sample }

	input:
	//file ref from refinputs
	//file bam from baminputs
	set val(sample),file('ref'),file('nano'),file("${sample}.sorted.bam") from aligned2

	output:
	set val(sample), file('mpileup') into bcf_out 

	script:
	mvf=params.mvf
	"""
	samtools mpileup -aaB -Q0 ${sample}.sorted.bam  > mpileup 
	"""
} 

process consensus {
	tag { sample }

	publishDir 'consensusSeqs', mode: 'copy', overwrite: true

	input:
	set val(sample), file('mpileup'),val(refName),file('ref.gz') from bcf_out.combine(refinputs2)
	//set val(refName),file('ref.gz') from refinputs2

	output:
	set val(sample), file("${sample}.fasta") into cons_out,cons_out1

	script:
	"""
        python3 ~/soft/branches/CRuMPIT_conscall/utils/callMpileup.py \
		-r ref.gz \
		-m mpileup \
		-d 10 \
		-F 70 \
		-f ${sample}.fasta

	"""
}

// processes for nanopolish prior to polishing

process contentsAll {
     input:
     file tars from tarInputs
     output:
     file tar_contents into tarContents
     script:
     """
     mkdir tar_contents
     tars=\$(ls $tars)
     for tar in \${tars}
     do
	echo \${tar}
	n=\$(basename \${tar})
	tar -tf $tars/\${tar} > tar_contents/\${n}.txt
     done
     """
}

process ripF5s {
    tag { sample }
    
    input:
    set val(sample),file('nano') from fastqinputs2
    file tar_contents from tarContents
    output:
    set val(sample),file('nano'),file('f5s'),file('sequencing_summary.txt') into rippedF5s
    script:
    tars=params.tarInputs
    """
    mkdir outfiles f5s
    python3 ~/soft/branches/CRuMPIT_conscall/utils/ripTarF5s.py -s PHE_208_209 \
        -ssum ${params.seqsum} \
	-t $tars \
	-tc tar_contents \
	-f $nano
    """
}

process nanoindex {
    tag { sample }
    input:
    set val(sample),file('nano'),file('f5s'),file('sequencing_summary.txt') from rippedF5s
    output:
    set val(sample),file('nano'),file('f5s'),file('fqs') into indexed, indexed2
    script:
    """
    mkdir fqs/
    cp $nano fqs/fastq
    ~/soft/nanopolish/nanopolish index -s sequencing_summary.txt -d f5s/ fqs/fastq
    """
}

// polish full genome


process makeRange {
    tag { sample }

    input:
    set val(sample),file( "${sample}.stats.txt" ),file( "*.png" ),file("${sample}.sorted.bam"),file("ctg.lay.fa") from samtooled2

    output:
    set val(sample),file('windows/window*') into rangesOut

    """
    mkdir windows
    zcat ctg.lay.fa > ref.fa
    python ~/soft/nanopolish/scripts/nanopolish_makerange.py ref.fa > ranges.txt
    split -l1 -d ranges.txt windows/window
    """
}

rangesOutT = rangesOut
                .map { sample,file -> tuple(sample,file.baseName, file) }
                .transpose()

polishAllIn = indexed2
                .combine(samtooled3, by: 0)
                .combine(rangesOutT, by: 0)



process polishAll {
    tag { sample + ' ' + winN }
    cpus 2
    publishDir 'polished', mode: 'copy', overwrite: true, pattern: "*_polished.fa"
    publishDir 'VCFs', mode: 'copy', overwrite: true, pattern: "*.vcf"


    input:
    set val(sample),file('nano'),file('f5s'),file('fqs'),file( "${sample}.stats.txt" ),file( "*.png" ),file("${sample}.sorted.bam"),file("ctg.lay.fa"), val(winN), file("window.txt") from polishAllIn

    output:
    set val(sample), val(winN), file("${sample}_${winN}.polished.vcf") into polishedFull

    script:
    """
    zcat "ctg.lay.fa" > ref.fa
    samtools index "${sample}.sorted.bam"
    win=\$(head -n1  window.txt)
    
    echo \$win $winN

    ~/soft/nanopolish/nanopolish variants --consensus \
        -o ${sample}_${winN}.polished.vcf \
        -w \$win \
        -r fqs/fastq \
        -b "${sample}.sorted.bam" \
        -g ref.fa -t ${task.cpus} \
        -q dcm,dam \
        --min-candidate-frequency 0.1
    """
}


// find regions of interest from ref/consensus/assembly

process blastn {
	tag { sample }

	publishDir 'blasts', mode: 'copy', overwrite: true
	
	input:
	set val(sample), file('fasta') from cons_out

	output:
	set val(sample), file("*.O6.blast.txt") into blastOut

	script:
        db=params.blastdb
	"""
	blastn -query fasta -db $db -outfmt 6 -max_target_seqs 10000 > ${sample}.O6.blast.txt
	"""
}

process parseBlast6 {
	tag { sample }

	publishDir 'ngstar', mode: 'copy', overwrite: true
	
	input:
	set val(sample), file('O6.blast.txt') from blastOut

	output:
	set val(sample), file("${sample}.csv"),file('windows') into pb6_out1
 	set val(sample),file('windows/*.txt') into pb6_out

	script:
	meta=params.ngstarMeta
	"""
	python3 /home/nick/soft/branches/CRuMPIT_conscall/utils/pb6.py -b O6.blast.txt -m $meta -o ${sample}.csv
        mkdir windows
	awk -F, '{print \$2":"\$8"-"\$9  > "windows/window_"\$3"_"\$8".txt" }' ${sample}.csv
	rm windows/window_sseqid*
	"""
}

pb6_outT = pb6_out
		.map { sample,file -> tuple(sample,file.baseName, file) }
		.transpose()

polishIn = indexed
		.combine(samtooled, by: 0)
		.combine(pb6_outT, by: 0)


process polish {
    tag { sample + ' ' + winN }
    cpus 2
    publishDir 'polished', mode: 'copy', overwrite: true, pattern: "*_polished.fa"
    publishDir 'VCFs', mode: 'copy', overwrite: true, pattern: "*.vcf"


    input:
    set val(sample),file('nano'),file('f5s'),file('fqs'),file( "${sample}.stats.txt" ),file( "*.png" ),file("${sample}.sorted.bam"),file("ctg.lay.fa"), val(winN), file("window.txt") from polishIn

    output:
    //set val(sample), file("*_polished.fa"),file("*.vcf") into polished
    set val(sample), val(winN), file("${sample}_${winN}_polished.fa") into polished

    script:
    """
    zcat "ctg.lay.fa" > ref.fa
    samtools index "${sample}.sorted.bam"
    win=\$(head -n1  window.txt)
    
    echo \$win $winN

    ~/soft/nanopolish/nanopolish variants --consensus \
        -o ${sample}_${winN}.polished.vcf \
        -w \$win \
        -r fqs/fastq \
        -b "${sample}.sorted.bam" \
        -g ref.fa -t ${task.cpus} \
        -q dcm,dam \
        --min-candidate-frequency 0.1
        ~/soft/nanopolish/nanopolish vcf2fasta --skip-checks -g ref.fa ${sample}_${winN}.polished.vcf > polished_genome.fa
        samtools faidx polished_genome.fa \$win > ${sample}_${winN}_polished.fa
    """
}

process blastnpolished {
        tag { sample + ' ' + winN }

        publishDir 'blasts', mode: 'copy', overwrite: true

        input:
	set val(sample), val(winN), file("polished.fa") from polished

        output:
        set val(sample), val(winN), file("*.O6.blast.txt") into blastPolishOut

        script:
        db=params.blastdb
        """
        blastn -query  polished.fa -db $db -outfmt 6 -max_target_seqs 10000 > ${sample}_${winN}.O6.blast.txt
        """
}

process parseBlast6 {
        tag { sample + ' ' + winN }

        publishDir 'ngstar', mode: 'copy', overwrite: true

        input:
        set val(sample), val(winN), file('O6.blast.txt') from blastPolishOut

        output:
        set val(sample), file("${sample}_${winN}_polished.csv") into pb6Polish_out

        script:
        meta=params.ngstarMeta
        """
        python3 /home/nick/soft/branches/CRuMPIT_conscall/utils/pb6.py -b O6.blast.txt -m $meta -o ${sample}_${winN}_polished.csv
        """
}



