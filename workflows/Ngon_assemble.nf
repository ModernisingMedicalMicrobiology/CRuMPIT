params.inCsv=file('files.csv')
params.ngstarMeta='~/Dropbox/MMM/ana/Ngon/genes/'
tarInputs = file(params.tarInputs)
glist = file(params.inCsv)
Channel
        .from( glist )
        .splitCsv()
        .map { row -> tuple(row[0], row[1]) }
        .view()
        .into{ ginputs1; ginputs2 }


process assemble {
     tag { sample }
     cpus 4

     input:
     set val(sample),val(nano) from ginputs1

     output:
     set val(sample),file('contigs.ctg.lay'),val(nano) into assembled

     script:
     """
     zcat ${nano} | head -n 800000 > nano.fastq
     ~/soft/wtdbg2/wtdbg2 -t ${task.cpus} -i nano.fastq -fo contigs -L 5000
     """
}

process consensus {
     tag { sample }
     cpus 4

     publishDir 'assemblies',  mode: 'copy'

     input:
     set val(sample),file('contigs.ctg.lay'),val(nano) from assembled

     output:
     set val(sample),file( "${sample}.ctg.lay.fa" ),val(nano) into conseqs,conseqs2,conseqs3

     script:
     """
     ~/soft/wtdbg2/wtpoa-cns -t ${task.cpus} -i contigs.ctg.lay -fo ${sample}.ctg.lay.fa
     """
}

process blastn {
     tag { sample }

     publishDir 'blastns',  mode: 'copy'

     input:
     set val(sample),file( "${sample}.ctg.lay.fa" ),val(nano) from conseqs3
     output:
     set val(sample),file( "${sample}.blast.txt" ) into blast


     script:
     """
     blastn -query ${sample}.ctg.lay.fa -db ~/Dropbox/MMM/ana/Ngon/genes/ngstar \
                -outfmt 6 -max_target_seqs 10000 > ${sample}.blast.txt
     """
}


process map {
     tag { sample }
     cpus 4

     input:
     set val(sample),file( "ctg.lay.fa" ),val(nano) from conseqs2

     output:
     set val(sample),file("ctg.lay.fa"),val(nano),file('out.sam') into mapped, mapped2 

     script:
     """
     minimap2 -t ${task.cpus} -ax map-ont  ctg.lay.fa ${nano} > out.sam
     """
}


process samtools {
     tag { sample }

     publishDir 'remaps',  mode: 'copy'

     input:
     set val(sample),file( "ctg.lay.fa" ),val(nano),file('out.sam') from mapped2

     output:
     set val(sample),file( "${sample}.stats.txt" ),file( "*.png" ),file("${sample}.sorted.bam"),file("ctg.lay.fa") into samtooled

     script:
     """
     samtools view -Sb out.sam -o out.bam
     samtools sort -o ${sample}.sorted.bam out.bam
     samtools index ${sample}.sorted.bam
     python3 /home/nick/dtc/plotBam.py ${sample}.sorted.bam > ${sample}.stats.txt
     """
}

//process racon {
//     tag { sample }
//     cpus 4
//
//     publishDir 'assemblies',  mode: 'copy'
//
//     input:
//     set val(sample),file( "ctg.lay.fa" ),val(nano),file('out.sam') from mapped
//
//     output:
//     set val(sample),file( "${sample}.ctg.lay.racon.fa" ) into racons0,racons1
//
//     script:
//     """
//     racon ${nano} out.sam ctg.lay.fa > ${sample}.ctg.lay.racon.fa
//     """
//}
//
//process annotate {
//     tag { sample }
//     cpus 4
//
//     publishDir 'annotation',  mode: 'copy'
//
//     input:
//     set val(sample),file( "ctg.lay.fa" ) from racons0
//
//     output:
//     set val(sample),file( "*_prokka" ) into annotations
//
//     script:
//     """
//     $HOME/prokka/bin/prokka --outdir ${sample}_prokka --prefix ${sample} ctg.lay.fa
//     """
//}
//
//process blastnRacon {
//     tag { sample }
//
//     publishDir 'blastns',  mode: 'copy'
//
//     input:
//     set val(sample),file( "${sample}.ctg.lay.racon.fa" ) from racons1
//     output:
//     set val(sample),file( "${sample}.racon.blast.txt" ) into blastRacon
//
//
//     script:
//     """
//     blastn -query ${sample}.ctg.lay.racon.fa -db ~/Dropbox/MMM/ana/Ngon/genes/ngstar \
//		-outfmt 6 -max_target_seqs 10000 > ${sample}.racon.blast.txt
//     """
//}

process parseBlast6 {
     tag { sample }

     publishDir 'ngstar', mode: 'copy', overwrite: true, pattern: "*csv"

     input:
     set val(sample), file('O6.blast.txt') from blast

     output:
     set val(sample), file("${sample}.csv"),file('windows') into pb6_out

     script:
     meta=params.ngstarMeta
     """
     python3 /home/nick/Dropbox/MMM/ana/Ngon/consensus/pb6.py -b O6.blast.txt -m $meta -o ${sample}.csv
     mkdir windows
     awk -F, '{print \$2":"\$8"-"\$9  > "windows/window_"\$3"_"\$8".txt" }' ${sample}.csv
     rm windows/window_sseqid*
     """
}

process contentsAll {
     cache 'deep' 

     input:
     file tars from tarInputs

     output:
     file tar_contents into tarContents

     script:
     """
     mkdir tar_contents
     tars=\$(ls $tars)

     for tar in \${tars}
     do
	echo \${tar}
	n=\$(basename \${tar})
	tar -tf $tars/\${tar} > tar_contents/\${n}.txt
     done
     """
}

process ripF5s {
    tag { sample }
    
    input:
    set val(sample),val(nano) from ginputs2
    file tar_contents from tarContents

    output:
    set val(sample),file('fqs'),file('f5s'),file('sequencing_summary.txt') into rippedF5s

    script:
    tars=params.tarInputs
    """
    mkdir outfiles f5s fqs
    zcat ${nano} | head -n 800000 > fqs/fastq
    python3 ~/soft/branches/CRuMPIT_conscall/utils/ripTarF5s.py -s PHE_208_209 \
	-ip 163.1.213.195 \
	-t $tars \
	-tc tar_contents \
	-f fqs/fastq

    """
}

process nanoindex {
    tag { sample }

    input:
    set val(sample),file('fqs'),file('f5s'),file('sequencing_summary.txt') from rippedF5s

    output:
    set val(sample),file('f5s'),file('fqs') into indexed

    script:
    """
    ~/soft/nanopolish/nanopolish index -s sequencing_summary.txt -d f5s/ fqs/fastq
    """

}

process polish {
    tag { sample }
    cpus 4

    publishDir 'polished', mode: 'copy', overwrite: true

    input:
    set val(sample),val(nano),file('f5s'),file('fqs'),file( "${sample}.stats.txt" ),file( "*.png" ),file("${sample}.sorted.bam"),file("ctg.lay.fa"),file("${sample}.csv"),file('windows') from indexed.combine(samtooled, by: 0).combine(pb6_out, by: 0)

    output:
    file("*_polished.fa") into polished

    script:
    """
    samtools index "${sample}.sorted.bam"
    windows=\$(ls windows/*.txt)
    for window in \${windows}
    do
    win=\$(head -n1  \$window)
    winN=\$(basename \$window .txt)
    echo \$win \$winN
        ~/soft/nanopolish/nanopolish variants --consensus \
        --methylation-aware dcm,dam \
        -o \$winN.polished.vcf \
        -w \$win \
        -r fqs/fastq \
        -b "${sample}.sorted.bam" \
        -g "ctg.lay.fa" -t ${task.cpus} \
        --min-candidate-frequency 0.1

        ~/soft/nanopolish/nanopolish vcf2fasta --skip-checks -g "ctg.lay.fa" \$winN.polished.vcf > polished_genome.fa
        samtools faidx polished_genome.fa \$win > ${sample}_\${winN}_polished.fa
    ~/soft/nanopolish/nanopolish vcf2fasta --skip-checks -g "ctg.lay.fa" *polished.vcf > ${sample}_polished.fa
    done
    """
}



