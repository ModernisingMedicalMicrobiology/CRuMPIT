#!/usr/bin/env nextflow

///////////// params ////////////////

base=params.base
bdir=params.bdir
params.sample='noname'
params.ip='127.0.0.1'
params.sqlip='127.0.0.1'
params.seq_kit='SQK-RPK004'
params.seqfol=''
params.watch=true
params.porechop='normal'
params.insfx='batch'
params.basecalling=true
params.cudadevice='auto'
params.map='off'
params.flowcell='FLO-MIN106'
params.bar_kit='SQK-RBK004'

if (!params.bar_kit || (params.bar_kit == '') || (params.bar_kit == 'None')) {	
	params.bar_kit2=params.seq_kit
} else {
	params.bar_kit2=params.bar_kit
}

if (params.watch == true){
	pf5s=Channel
		.fromPath( "$bdir/*${params.insfx}" )
		.splitCsv()
		.map { row -> tuple(row[0], row[1], row[2], row[3], row[4]) }

	wf5s=Channel
		.watchPath( "$bdir/*${params.insfx}" )
		.splitCsv()
		.map { row -> tuple(row[0], row[1], row[2], row[3], row[4]) }

		f5s=pf5s.mix(wf5s)
} else if (params.watch == false){
	f5s=Channel
		.fromPath( "$bdir/*${params.insfx}" )
		.splitCsv()
		.map { row -> tuple(row[0], row[1], row[2], row[3], row[4]) }
}

////// basecalling //////////////

if (params.basecalling == true){
/*
	///// input channels ///////////
	if (params.watch == true){
		pf5s=Channel
			.fromPath( "$bdir/*${params.insfx}" )
			.map { file -> tuple(file.baseName, file) }

		wf5s=Channel
			.watchPath( "$bdir/*${params.insfx}" )
			.map { file -> tuple(file.baseName, file) }

		f5s=pf5s.mix(wf5s)
	} else if (params.watch == false){
		f5s=Channel
			.fromPath( "$bdir/*${params.insfx}" )
			.map { file -> tuple(file.baseName, file) }
	}
*/
	process runGuppy {
		tag {params.sample + '_' + name }
		maxForks 5
		errorStrategy 'ignore'
//		scratch true
		label 'gpu'

		input:
		//set val(name), file(f5) from f5s
		set val(lane), val(name), file(f5), val(fqf), val(sum) from f5s

		output:
		set val(name),file("${name}_fastqs") into fastqout2
		set val(name),file("lnFiles") into fast5out

		script:
		if  (params.insfx == 'batch')
			"""
			mkdir lnFiles
			crumpit f5Linker -i ${f5} -o lnFiles

			guppy_basecaller \
			-x ${params.cudadevice} \
			--flowcell ${params.flowcell} \
			--kit ${params.seq_kit} \
			-i lnFiles \
			-s ${name}_fastqs \
			--recursive \
			-q 8000 
			cat ${name}_fastqs/*.fastq > ${name}.fastq
			rm ${name}_fastqs/*.fastq
			mv ${name}.fastq ${name}_fastqs/

			crumpit pushAlbaSum -s ${params.sample} \
			-ip ${params.ip} -sum ${name}_fastqs/sequencing_summary.txt \
			-fastq ${name}_fastqs/${name}.fastq

			crumpit f5info -r ${params.sample} -b ${f5} \
			-ip ${params.ip}

			"""
		else if (params.insfx in ['tar','tar.gz'])
			"""
			mkdir lnFiles
			tar -xvf ${f5} -C lnFiles

			${gupath}/bin/guppy_basecaller \
			-x ${params.cudadevice} \
			-c ${gupath}/data/dna_r9.4.1_450bps_prom.cfg \
			-i lnFiles \
			-s ${name}_fastqs \
			--recursive \
			-m ${gupath}/data/template_r9.4.1_450bps_5mer_raw_prom.jsn

			find  lnFiles -name '*.fast5' > ${name}_fastqs/${name}.batch

			crumpit pushAlbaSum -s ${params.sample} \
			-ip ${params.ip} -sum ${name}_fastqs/sequencing_summary.txt

			crumpit f5info -r ${params.sample} \
			-b ${name}_fastqs/${name}.batch \
			-ip ${params.ip}

			"""
	}
}
else if (params.grid == true) {
/*
	///// input channels ///////////
	if (params.watch == true){
		pf5s=Channel
			.fromPath( "$bdir/*${params.insfx}" )
			.splitCsv()
			.map { row -> tuple(row[0], row[1], row[2], row[3], row[4]) }

		wf5s=Channel
			.watchPath( "$bdir/*${params.insfx}" )
			.splitCsv()
			.map { row -> tuple(row[0], row[1], row[2], row[3], row[4]) }

			f5s=pf5s.mix(wf5s)
	} else if (params.watch == false){
		f5s=Channel
			.fromPath( "$bdir/*${params.insfx}" )
			.splitCsv()
			.map { row -> tuple(row[0], row[1], row[2], row[3], row[4]) }
	}
*/
	process processFiles {
			tag {params.sample + '_' + lane + '_' + name }
			errorStrategy 'ignore'
			scratch true
			//maxForks 4 

			input:
			set val(lane), val(name), val(reads), val(fqf), val(sum) from f5s

			output:
			set val(name),file("${name}_fastqs") into fastqout2
			set val(name),file('lnFiles') into fast5out

			script:
			seqfol=params.seqfol

			"""
			echo 'copy files'
			date
				mkdir ${name}'_fastqs/' 
			zcat -f ${fqf} > ${name}'_fastqs/'${lane}'_'${name}'.fastq'
			mkdir lnFiles
			cp ${reads} lnFiles/reads.fast5

			echo 'pushAlba'
			date
	

			touch ${name}'_fastqs/'sequencing_summary.txt

			echo 'f5info'
			date

			crumpit f5info -r ${params.sample} \
			-f5 lnFiles/reads.fast5 \
			-ip ${params.ip}

			"""
	}
}

if (params.porechop != 'off'){
	process demultiplex {
		errorStrategy 'ignore'
		tag {params.sample + '_' + name }
		scratch true

		input:
		set val(name),file("fastqs") from fastqout2

		//publishDir "fastqs", mode: 'symlink', overwrite: false

		output:
		set val(name),file("fastqs") into deMultOut

		script:
		if (params.porechop == 'normal')
			"""
			porechop -i fastqs \
			--untrimmed \
			-b porechopped -t ${task.cpus} \
			> deBarcode.log
					rm fastqs/*fastq
					cat porechopped/*.fastq > fastqs/${name}.fastq
			"""
		else if (params.porechop == 'strict')
			"""
			porechop -i fastqs \
			--untrimmed \
			-b porechopped \
			-t ${task.cpus} \
			--barcode_threshold 60 \
			--discard_middle \
			--require_two_barcodes \
			> deBarcode.log
					rm fastqs/*fastq
					cat porechopped/*.fastq > fastqs/${name}.fastq
			"""
		else if (params.porechop == 'guppy_strict')
			"""
			guppy_barcoder \
				-i fastqs \
				-s guppy_demuxed \
				--barcode_kits ${params.bar_kit2} \
				--require_barcodes_both_ends \
				-t 1 -v -r
			rm fastqs/*fastq
			cat guppy_demuxed/*/*.fastq > fastqs/${name}.fastq
			"""
		else if (params.porechop == 'guppy')
			"""
			guppy_barcoder \
				-i fastqs \
				-s guppy_demuxed \
				--barcode_kits ${params.bar_kit2} \
				-t 1 -v -r
			rm fastqs/*.fastq
			cat guppy_demuxed/*/*.fastq > fastqs/${name}.fastq
			"""
	}
}
else {
	//deMultOut=fastqout2

	process demultiplexOff {
		errorStrategy 'ignore'
		tag {params.sample + '_' + name }
		scratch true

		input:
		set val(name),file("fastqs") from fastqout2

		//publishDir "fastqs", mode: 'symlink', overwrite: false

		output:
		set val(name),file("fastqs") into deMultOut

		script:
		"""
		crumpit processNoMultiplex \
			-fol fastqs
		rm fastqs/*.fastq
		cat processed_*.fastq > fastqs/${name}.fastq
		"""
	}
}

////////////////////////// PIPELINE START ////////////////////////////

params.soft="/soft"

soft=file(params.soft)
cdb=file(params.cdb)

process remove_Low_Complexity {
	tag {params.sample + '_' + name }
	errorStrategy 'ignore'
	scratch true

	input:
	set val(name),file("fastqs") from deMultOut

	output:
	set val(name),file("fastqs"),file('high_complex.fastq') into complexity, complexity2

	script:
	sample=params.sample
	"""
	touch high_complex.fastq low_complex.fastq
	cat fastqs/*.fastq | sed  '/^\$/d' | prinseq-lite.pl -lc_method dust -lc_threshold 7 -fastq stdin -out_good high_complex -out_bad low_complex
	"""
}

process runCentrifuge {
	tag {params.sample + '_' + name }
	errorStrategy 'ignore'
	scratch true

	input:
	set val(name),file("fastqs"),file('high_complex.fastq') from complexity

	output:
	set val(name),file("fastqs"), file('fuge.txt.gz'),file('high_complex.fastq') into centout, centout2, centout3

	script:
	ip=params.ip
	sample=params.sample

	"""
	centrifuge -f -x $cdb \
	-q --mm -U 'high_complex.fastq' \
	-S 'fuge.txt' --min-hitlen 16 -k 1
	tail -n +2 'fuge.txt' | gzip > 'fuge.txt.gz'
	crumpit pushCentMdb \
	-cf 'fuge.txt.gz' -s ${sample} -fq 'high_complex.fastq' -ip ${ip}
	"""
}


if (params.map != 'off') {
	process map {
		tag {params.sample + '_' + name }
		errorStrategy 'ignore'
//		scratch true
		maxForks 4
		//publishDir "$base/", mode: 'copy', overwrite: false
	
		input:
		set val(name),file("fastqs"), file('fuge.txt.gz'),file('high_complex.fastq') from centout

		output:
		set val(name),file("${name}_sorted") into maps 
		file("*_sorted/**/*.bam") into allMaps
	
		script:
		ip=params.ip
		sample=params.sample
		refbase=params.refbase
		taxids=params.map
		custom_refs=params.custom_refs
		"""
		crumpit preMap \
		-cf 'fuge.txt.gz' -s ${sample} \
		-fq 'high_complex.fastq' \
		-ip ${ip} -base ${refbase} \
		-n ${name} \
		-mo ${taxids} \
		-ref ${custom_refs}

		rsync -avP sorted ${base}/ || :
		mv sorted ${name}_sorted
		"""
	
	}
	
	process mpileup {
		tag {params.sample + '_' + name }
		errorStrategy 'ignore'
		scratch true
		maxForks 1

		//publishDir "$base/", overwrite: true

		input:
		set val(name),file('sorted') from maps

		when:
		params.watch == true

		//output:
		//set val(name),file('mpileup') into mpileups
		//set file('plots'), file('mapCsvs') into merges

		script:
		sqlip=params.sqlip
		sample=params.sample
		refbase=params.refbase
		base=params.base
		taxids=params.map
		custom_refs=params.custom_refs

		""" 
		bams="\$(ls sorted/*/*.bam)"
		bars="\$(ls -d sorted/*)"

		mkdir -p ${base}/mpileup ${base}/merged
		for bar in \${bars}
		do
			barN="\$(basename \${bar})"
			mkdir -p  ${base}/mpileup/\${barN}
			mkdir -p ${base}/merged/\${barN}
		done

		for bam in \${bams}
		do 
			merge_mpileup.bash ${base} $sample $refbase ${sqlip} '${taxids}' ${custom_refs} \${bam} 
		done
		rsync -avP plots mapCsvs ${base}/ || :
		"""
	}

	allMaps
		.collect()
		.flatten()
		.map { file -> tuple(file.getParent().getName(),file.getBaseName().split('_')[0], file) }
		.groupTuple(by:[0,1])
		.view()
		.set{allBams}
	
	process mergeAll {
		tag {params.sample + ' ' + bar + ' ' + taxid}
		errorStrategy 'ignore'
		scratch true

		input:
		set val(bar),val(taxid), file("in?.sorted.bam") from allBams

		params.watch == false

		script:
		ip=params.ip                                                    
		sample=params.sample                                            
		refbase=params.refbase                                          
		base=params.base
		taxids=params.map
		custom_refs=params.custom_refs
		"""
		if [ ! -f ${base}/merged/${bar}/${taxid}.sorted.bam ]; then
			echo 'file not exits'
			mkdir -p ${base}/merged/$bar/
			samtools merge ${base}/merged/$bar/${taxid}.sorted.bam \
				*.sorted.bam 
		else
			echo 'file exists, merging'
			samtools merge ${base}/merged/$bar/${taxid}.merged.bam \
				*.sorted.bam \
				${base}/merged/${bar}/${taxid}.sorted.bam
			mv ${base}/merged/$bar/${taxid}.merged.bam ${base}/merged/$bar/${taxid}.sorted.bam
		fi
		mkdir -p ${base}/mpileup/$bar/
		samtools depth ${base}/merged/$bar/${taxid}.sorted.bam > ${base}/mpileup/$bar/${taxid}.depth
		crumpit depthStats \
		-s $sample \
		-b $bar \
		-pf ${base}/mpileup/$bar/${taxid}.depth \
		-t $taxid \
		-r $refbase \
		-sqlip $sqlip \
		-mo ${taxids} \
		-rf ${custom_refs}

		rsync -avP plots mapCsvs ${base}/ || :
		"""
	}
}

process compileFQs {
	tag {params.sample + '_' + name }
	errorStrategy 'ignore'
	scratch true
	maxForks 5
        stageInMode 'copy'
	publishDir "$base/", mode: 'move'

	input:
	set val(name),file("fastqs"), file('fuge.txt.gz'),file('high_complex.fastq'),file('lnFiles') from centout3.combine(fast5out,by:0)

	output:
	file("basecalled_fastq/*") into fastqs_out
	file("f5s/*") into f5s_out

	script:
	"""
	crumpit processFQs \
	-fol fastqs \
	-c fuge.txt.gz

	rebinner.py -f5s lnFiles/*.fast5 -fq barcodes/* -o f5s/
	mv barcodes basecalled_fastq
	gzip basecalled_fastq/*
	"""
}

//process backUpF5s {
//	tag {params.sample + '_' + name }
//        errorStrategy 'ignore'
//	//publishDir "$base/", mode: 'move', overwrite: true
//	//scratch true
//
//	input:
//	set val(name),file("fastqs"), file('fuge.txt.gz'),file('high_complex.fastq'),file('reads.fast5') from centout2
//
//	output:
//	file("f5s/*.fast5.gz")
//
//	script:
//	"""
//	mv fastqs ${name}_fastqs
//	crumpit backUpf5s \
//	-sum ${name}_fastqs/sequencing_summary.txt \
//	-b reads.fast5 \
//	-c fuge.txt.gz
//        mkdir f5s/
//        """
//        ///mv reads.fast5 f5s/${name}.fast5
//        ///gzip --force f5s/${name}.fast5
//	///"""
//}

